// SPDX-License-Identifier: GPL-3.0-or-later
#include <lib/tinystd/streams.h>

#include "../loader/efi_boot_iface.h"

STD_INTERFACE
void pipe_stream_write(uint8_t stream __attribute__((unused)), const char *data) {
    boot_print_screen(data);
}

