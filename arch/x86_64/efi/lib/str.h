// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stddef.h>

size_t efi_strlen(const char* string);
size_t efi_strchoccur(const char* str, const char c, size_t len);
int efi_strpos(const char* str, const char c, int len);
void efi_memcpy(void* dest, const void* src, size_t len);
