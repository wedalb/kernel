// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stddef.h>

void char_to_wchar(const char* str, wchar_t* wstr, size_t len);