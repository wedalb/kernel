// SPDX-License-Identifier: GPL-3.0-or-later

#include <stddef.h>

#include "../lib/str.h"

void char_to_wchar(const char* str, wchar_t* wstr, size_t len) {
    size_t i;
    for (i = 0; i < len; i++) {
        wstr[i] = (wchar_t) str[i];
    }
    wstr[i + 1] = 0;
}