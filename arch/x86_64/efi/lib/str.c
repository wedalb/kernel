// SPDX-License-Identifier: GPL-3.0-or-later

#include <stddef.h>

size_t efi_strlen(const char* str) {
    size_t len = 0;
    while (str[len]) {
        len++;
    }
    return len;
}

size_t efi_strchoccur(const char* str, const char c, size_t len) {
    size_t occur = 0;
    for (size_t i = 0; i < len; i++) {
        if (str[i] == c) {
            occur++;
        }
    }
    return occur;
}

int efi_strpos(const char* str, const char c, int len) {
    int pos = -1;
    for (int i = 0; i < len; i++) {
        if (str[i] == c) {
            pos = i;
            break;
        }
    }
    return pos;
}

void efi_memcpy(void* dest, const void* src, size_t len) {
    for (size_t i = 0; i < len; i++) {
        ((char*) dest)[i] = ((char*) src)[i];
    }
}
