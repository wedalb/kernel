// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <external.uefi.uefi-headers/protocol/file.h>
#include <external.uefi.uefi-headers/protocol/loaded-image.h>
#include <external.uefi.uefi-headers/protocol/simple-file-system.h>


efi_file_protocol* efi_open_file(const char* path);
