// SPDX-License-Identifier: GPL-3.0-or-later

#include <external.uefi.uefi-headers/protocol/file.h>
#include <external.uefi.uefi-headers/protocol/loaded-image.h>
#include <external.uefi.uefi-headers/protocol/simple-file-system.h>

#include "../lib/str.h"
#include "../lib/chars.h"
#include "../loader/efi_runtime_vars.h"
#include "../loader/efi_boot_iface.h"

efi_file_protocol* efi_open_file(const char* path) {
    if (*path != '/') {
        boot_print_screen("efi_open_file: only absolute paths allowed\n");
        return NULL;
    }

    efi_status status;

    efi_guid loaded_image_protocol = EFI_LOADED_IMAGE_PROTOCOL_GUID;
    efi_loaded_image_protocol* loaded_image;
    status = boot_efi_system_table->BootServices->HandleProtocol(
        efi_handle_instance,
        &loaded_image_protocol,
        (void**) &loaded_image
    );
    if (status) {
        boot_print_screen("Unable to locate protocol for loaded image\n");
        return NULL;
    }

    efi_guid simple_file_system_protocol = EFI_SIMPLE_FILE_SYSTEM_PROTOCOL_GUID;
    efi_simple_file_system_protocol* filesystem;
    status = boot_efi_system_table->BootServices->HandleProtocol(
        loaded_image->DeviceHandle,
        &simple_file_system_protocol,
        (void**) &filesystem
    );
    if (status) {
        boot_print_screen("Unable to locate protocol for simple file system\n");
        return NULL;
    }

    efi_file_protocol* root_dir;
    if (filesystem->OpenVolume(filesystem, &root_dir)) {
        boot_print_screen("Could not open volume\n");
        return NULL;
    }

    efi_file_protocol* cur_dirent = root_dir;

    size_t path_str_len = efi_strlen(path);
    int absolute_slash_strpos = efi_strpos(path, '/', path_str_len);
    for (
        int slash_strpos = absolute_slash_strpos;
        slash_strpos != -1;
        slash_strpos = efi_strpos(&path[absolute_slash_strpos + 1], '/', path_str_len - absolute_slash_strpos - 1),
        absolute_slash_strpos += slash_strpos + 1
    ) {
        // Get the current component of the path. The component is the string between the last slash and the next slash.
        // The component should be copied into a new buffer because the path string is not null-terminated.
        // The component should also be null-terminated.
        int component_len = efi_strpos(&path[absolute_slash_strpos + 1], '/', path_str_len - absolute_slash_strpos - 1);
        if (component_len == -1) {
            component_len = path_str_len - absolute_slash_strpos - 1;
        }
        char component[component_len + 1];
        efi_memcpy(component, &path[absolute_slash_strpos + 1], component_len);
        component[component_len] = 0;
        // Convert the component to a wide string.
        wchar_t component_w[component_len + 1];
        char_to_wchar(component, component_w, component_len + 1);

        boot_print_screen("Opening ");
        boot_print_screen(component);
        boot_print_screen("\n");

        efi_file_protocol* dirent;
        status = cur_dirent->Open(
            cur_dirent,
            &dirent,
            component_w,
            EFI_FILE_MODE_READ,
            EFI_FILE_READ_ONLY
        );
        if (status) {
            boot_print_screen("could not open ");
            char temp_component[component_len + 1];
            for (int i = 0; i < component_len; i++) {
                temp_component[i] = component[i];
            }
            temp_component[component_len] = 0;
            boot_print_screen(temp_component);
            boot_print_screen(" (path ");
            boot_print_screen(path);
            boot_print_screen(")\n");
            return NULL;
        }

        cur_dirent = dirent;
    }

    return cur_dirent;
}

