// SPDX-License-Identifier: GPL-3.0-or-later

#include <external.uefi.uefi-headers/protocol/file.h>
#include <external.uefi.uefi-headers/protocol/loaded-image.h>
#include <external.uefi.uefi-headers/protocol/simple-file-system.h>
#include <external.uefi.uefi-headers/boot-services.h>
#include <external.uefi.uefi-headers/types.h>

#include <lib/tinystd/mm.h>
#include <lib/tinystd/printf.h>

#include <include/spec/bin/elf.h>
#include <include/spec/hw/acpi/acpi.h>

#include <kernel/boot_iface.h>

#include <arch/x86_64/mm/paging.h>

#include "../io/fs.h"
#include "common.h"
#include "drivers/video/fb/efifb.h"
#include "efi_boot_iface.h"
#include "efi_runtime_vars.h"
#include "kernel/mm/mem_info.h"

// Reference: https://github.com/Absurdponcho/PonchoOS/blob/main/gnu-efi/bootloader/main.c

const char* efi_kernel_file_path = "/EFI/os/kernel.elf";
const char* efi_kernel_file_path_alternative = "/efi/os/kernel.elf";
void* kernel_image;
size_t kernel_image_aligned_size;

static inline void* stack_pointer() {
    void* stackptr = NULL;
    __asm__ volatile ("mov %%rsp, %0" : "=r"(stackptr));
    return stackptr;
}

static inline bool compare_efi_guids(efi_guid *a, efi_guid *b) {
  return memncmp(a, b, sizeof(efi_guid));
}

static void* acpi_get_rsdp() {
  size_t num_of_entries = boot_efi_system_table->NumberOfTableEntries;
  efi_guid acpi_20_guid = ACPI_20_TABLE_GUID;

  for (size_t i = 0; i < num_of_entries; i++) {
    efi_configuration_table *table =
        &boot_efi_system_table->ConfigurationTable[i];
    if (compare_efi_guids(&table->VendorGuid, &acpi_20_guid)) {
      return (acpi_rsdp_descriptor_20 *)table->VendorTable;
    }
  }

  return NULL;
}

efi_file_protocol* efi_open_kernel_image() {
    printf("Opening kernel image %s...\n", efi_kernel_file_path);
    efi_file_protocol* kernel_file = efi_open_file(efi_kernel_file_path);
    if (!kernel_file) {
        kernel_file = efi_open_file(efi_kernel_file_path_alternative);
        if (!kernel_file) {
            boot_print_screen("Failed to open kernel image\n");
            boot_full_halt();
        }
    }
    return kernel_file;
}

efi_file_info* efi_get_file_info(efi_file_protocol* kernel_file) {
    efi_status status;
    efi_guid file_info_guid = EFI_FILE_INFO_GUID;
    size_t file_info_size;
    status = kernel_file->GetInfo(kernel_file, &file_info_guid, &file_info_size, NULL);
    if (status != EFI_BUFFER_TOO_SMALL && status != EFI_SUCCESS) {
        boot_print_screen("Could not prepare to get kernel image info\n");
        boot_full_halt();
    }

    efi_file_info *file_info;
    status = boot_efi_system_table->BootServices->AllocatePool(
        EfiLoaderData, file_info_size, (void **)&file_info);
    if (status) {
        printf("Could not allocate pool of size %zu\n", file_info_size);
        boot_full_halt();
    }
    printf("Allocated pool of size %zu, addr %p\n", file_info_size, file_info);

    status = kernel_file->GetInfo(kernel_file, &file_info_guid, &file_info_size,
                                    (void *)file_info);
    if (status) {
        boot_print_screen("Could not get kernel image info\n");
        printf("%d %d\n", file_info_size, status);
        boot_full_halt();
    }

    return file_info;
}

static void read_kernel_image_elf_header(efi_file_protocol* kernel_file, elf_header* header) {
    size_t header_size = sizeof(elf_header);
    printf("ELF header size: %zu\n", header_size);
    efi_status status = kernel_file->Read(kernel_file, &header_size, header);
    if (status) {
        printf("Could not read kernel image header, size %zu\n", header_size);
        boot_full_halt();
    }

    if (!elf_header_magic_matches(header->magic)) {
        boot_print_screen("Kernel image ELF header magic mismatch\n");
        boot_full_halt();
    }

    if (header->type != ELF_TYPE_EXEC) {
        printf("Wrong ELF type: %d\n", header->type);
        boot_full_halt();
    }
}

static void get_elf_program_headers(efi_file_protocol* kernel_file, elf_header* header, elf_program_header* pheaders, size_t pheaders_size) {
    printf("Program header offset: %zu\n", header->phoff);
    // Now we'll go to the program header and read that in
    if (kernel_file->SetPosition(kernel_file, header->phoff)) {
        printf("Could not seek to program header at %x\n", header->phoff);
        boot_full_halt();
    }
    if (kernel_file->Read(kernel_file, &pheaders_size, pheaders)) {
        boot_print_screen("Could not read program header\n");
        boot_full_halt();
    }
    printf("pheader entsize: %d, phnum: %d\n", header->phentsize, header->phnum);
    printf("pheader struct size: %d, pheader flags size: %d\n",
           sizeof(elf_program_header), sizeof(elf_program_header__flags));
}

static void allocte_kernel_data(efi_file_info* file_info, elf_program_header* pheaders) {
    efi_status status;

    // Allocate memory for the kernel data
    kernel_image = (void *)pheaders[0].paddr; // use the address of the first segment
    size_t kimg_pages_required = (file_info->FileSize + 0x1000 - 1) / 0x1000;
    status = boot_efi_system_table->BootServices->AllocatePages(
        AllocateAddress, EfiLoaderData, kimg_pages_required,
        (efi_physical_addr *)&kernel_image);
    if (status) {
        printf(
            "Could not allocate %zu kernel image pages at %p of size %lu: %d\n",
            kimg_pages_required, kernel_image, file_info->FileSize, status);
        boot_full_halt();
    }
    kernel_image_aligned_size = kimg_pages_required * 0x1000;
    // If we hard reset, the memory might still include old data so zero all of
    // it first
    memzero(kernel_image, kernel_image_aligned_size);

    kernel_image_aligned_size = kimg_pages_required * 0x1000;
}

static void load_kernel_data_from_elf(
    efi_file_protocol* kernel_file,
    elf_header* header,
    elf_program_header* pheaders,
    size_t pheaders_size,
    char* pheaders_buf
) {
    efi_status status;

    // Let's iterate through the program headers and
    // load the relevant entries
    for (elf_program_header *pheader = pheaders;
         (char *)pheader < &pheaders_buf[pheaders_size];
         pheader = (elf_program_header *)&((char *)pheader)[header->phentsize]) {
        switch (pheader->type) {
        case ELF_PH_TYPE_LOAD: {
            printf("pheader\n\tfilesz: %zu (%x), memsz: %zu (%x)\n\toffs: %x, "
                   "paddr: %x, vaddr: %x\n\tflags: %x (%c%c%c), align: %x\n",
                   pheader->filesz, pheader->filesz, pheader->memsz,
                   pheader->memsz, pheader->offset, pheader->paddr,
                   pheader->vaddr, pheader->flags.n,
                   pheader->flags.flags.r ? 'r' : '-',
                   pheader->flags.flags.w ? 'w' : '-',
                   pheader->flags.flags.x ? 'x' : '-', pheader->align);

            elf_program_header__physical_addr segment = pheader->paddr;

            status = kernel_file->SetPosition(kernel_file, pheader->offset);
            if (status) {
                boot_print_screen("Could not seek to program\n");
                boot_full_halt();
            }
            size_t program_filesz = pheader->filesz;
            status = kernel_file->Read(kernel_file, &program_filesz,
                                       (void *)(uintptr_t)segment);
            if (status) {
                boot_print_screen("Could not read kernel program\n");
                boot_full_halt();
            }
            break;
        }
        default:
            printf("Unsupported program header type %d, skipping\n",
                   pheader->type);
            break;
        }
    }

    printf("Kernel image successfully loaded\n");
}

void efi_deallocate_pool(void* ptr) {
    efi_status status = boot_efi_system_table->BootServices->FreePool(ptr);
    if (status) {
        printf("Could not free pool at %p: %d\n", ptr, efi_error_to_string(status));
        boot_full_halt();
    }
}

void efi_deallocate_pages(void* ptr, size_t pages) {
    efi_status status = boot_efi_system_table->BootServices->FreePages((efi_physical_addr) ptr, pages);
    if (status) {
        printf("Could not free pages at %p: %d\n", ptr, efi_error_to_string(status));
        boot_full_halt();
    }
}

typedef __attribute__((sysv_abi)) void (*kernel_main)(boot_interface *bi);

/**
 * Loads the kernel and returns the entrypoint
 */
static kernel_main load_kernel() {
    efi_file_protocol *kernel_file = efi_open_kernel_image();
    efi_file_info *file_info = efi_get_file_info(kernel_file);

    printf("Kernel image size: %lu\n", file_info->FileSize);

    elf_header header;
    read_kernel_image_elf_header(kernel_file, &header);

    size_t pheaders_size = header.phentsize * header.phnum;
    char pheaders_buf[pheaders_size];
    elf_program_header *pheaders = (elf_program_header *)pheaders_buf;
    get_elf_program_headers(kernel_file, &header, pheaders, pheaders_size);

    allocte_kernel_data(file_info, pheaders);
    load_kernel_data_from_elf(kernel_file, &header, pheaders, pheaders_size,
                              pheaders_buf);

    kernel_file->Close(kernel_file);

    printf("Deallocating pool for file info\n");
    // Deallocate the memory used for loading the kernel
    efi_deallocate_pool(file_info);

    printf("Kernel entry point: %p\n", header.entry);
    return (void*) header.entry;
}

void efi_boot_kernel() {
    kernel_main kernel_entry = load_kernel();

    void* acpi_rsdp = acpi_get_rsdp();

    printf("Initializing GOP\n");
    driver_efifb_handoff efi_fb_handoff = {0};
    efi_initialize_gop(&efi_fb_handoff);

    efi_memory_descriptor* memory_map = NULL;
    size_t memory_map_size = 0;
    size_t memory_map_descriptor_size = 0;
    uint32_t memory_map_descriptor_version = 0;
    exit_boot_services(&memory_map, &memory_map_size, &memory_map_descriptor_size, &memory_map_descriptor_version);

    kernel_meminfo meminfo = {0};
    efi_populate_meminfo(
        memory_map,
        memory_map_size,
        memory_map_descriptor_size,
        memory_map_descriptor_version,
        &meminfo
    );

    // Call the kernel passing all the information it needs
    // We don't want the kernel calling into the bootloader
    // because this makes it hard to separate things.
    kernel_entry(&(boot_interface) {
        .stack_pointer_before_kernel = stack_pointer(),
        .acpi_rsdp = acpi_rsdp,
        .meminfo = &meminfo,
        .efifb_handoff = &efi_fb_handoff,
    });

    boot_full_halt();
}