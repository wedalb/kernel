// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <external.uefi.uefi-headers/boot-services.h>
#include <external.uefi.uefi-headers/types.h>

void exit_boot_services(efi_memory_descriptor **memory_map,
                        size_t *memory_map_size_ptr,
                        size_t *memory_map_descriptor_size_ptr,
                        uint32_t *memory_map_descriptor_version_ptr);
void boot_full_halt() __attribute__((noreturn));
void boot_asm_hlt();
const char *efi_error_to_string(efi_status status);
