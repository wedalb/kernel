// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stddef.h>

extern void* kernel_image;
extern size_t kernel_image_aligned_size;

void efi_boot_kernel();
