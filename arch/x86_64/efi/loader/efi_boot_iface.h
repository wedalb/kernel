// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <external.uefi.uefi-headers/boot-services.h>

#include <drivers/video/fb/efifb.h>

#include "kernel/mm/mem_info.h"
#include "kernel/mm/mem_iface.h"

void boot_print_screen(const char* string);
void efi_populate_meminfo(
    efi_memory_descriptor* memory_map,
    size_t memory_map_size,
    size_t memory_map_descriptor_size,
    uint32_t memory_map_descriptor_version,
    kernel_meminfo* meminfo
);
void efi_initialize_gop(driver_efifb_handoff *efifb_handoff);
