// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <external.uefi.uefi-headers/system-table.h>

extern efi_system_table* boot_efi_system_table;
extern efi_handle* efi_handle_instance;
