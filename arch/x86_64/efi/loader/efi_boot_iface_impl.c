// SPDX-License-Identifier: GPL-3.0-or-later
#include <external.uefi.uefi-headers/boot-services.h>
#include <external.uefi.uefi-headers/protocol/graphics-output.h>
#include <external.uefi.uefi-headers/types.h>

#include <lib/tinystd/mm.h>
#include <lib/tinystd/printf.h>
#include <lib/tinystd/str.h>

#include <include/internal/gfx/colors/format.h>

#include <drivers/video/fb/efifb.h>
#include <drivers/video/fb/console.h>

#include <kernel/boot_iface.h>
#include <kernel/mm/mem_info.h>

#include <arch/x86_64/interrupts/pic.h>

#include "../../io/ports.h"
#include "../../io/serial.h"
#include "../../mm/paging.h"
#include "common.h"
#include "efi_runtime_vars.h"
#include "kernel_loader.h"

#define TAB_SIZE 4

// for now we limit the maximum to 256
#define MEM_MAX_AREAS 256
kernel_meminfo_area efi_usable_mem_areas[MEM_MAX_AREAS] = {};
int efi_usable_mem_area_count = 0;
kernel_meminfo_area efi_all_mem_areas[MEM_MAX_AREAS] = {};
int efi_all_mem_area_count = 0;


#define EFI_MEMORY_TYPE_COUNT 14
// https://stackoverflow.com/a/65134765/4479004
const char* efi_memory_types[EFI_MEMORY_TYPE_COUNT] = {
    "EfiReservedMemoryType",
    "EfiLoaderCode",
    "EfiLoaderData",
    "EfiBootServicesCode",
    "EfiBootServicesData",
    "EfiRuntimeServicesCode",
    "EfiRuntimeServicesData",
    "EfiConventionalMemory",
    "EfiUnusableMemory",
    "EfiACPIReclaimMemory",
    "EfiACPIMemoryNVS",
    "EfiMemoryMappedIO",
    "EfiMemoryMappedIOPortSpace",
    "EfiPalCode",
};

bool use_efifb = false;

void boot_efi_print_console(const char* string) {
    size_t str_len = strlen(string);
    size_t line_feed_occurrences = strchoccur(string, '\n', str_len);
    size_t tab_occurences = strchoccur(string, '\t', str_len);
    size_t long_string_len = str_len + line_feed_occurrences + tab_occurences * TAB_SIZE + 1;
    uint16_t long_string[long_string_len];
    size_t dest_i;
    size_t last_nl = 0;
    int offs = 0;
    for (int i = 0; i < str_len; i++) {
        char cur_c = string[i];
        dest_i = i + offs;
        switch (cur_c) {
            case '\n':
                long_string[dest_i] = '\r';
                offs++;
                dest_i++;
                last_nl = dest_i;
                break;
            case '\t': {
                uint8_t spaces_to_add = TAB_SIZE - ((dest_i - last_nl) % TAB_SIZE);
                if (spaces_to_add == 0) {
                    spaces_to_add = TAB_SIZE;
                }
                for (uint8_t si = 0; si < spaces_to_add; si++) {
                    long_string[dest_i + si] = (uint16_t) ' ';
                }
                offs += spaces_to_add - 1;
                continue;
            }
        }
        long_string[dest_i] = (uint16_t) cur_c;
    }
    long_string[dest_i + 1] = 0;

    boot_efi_system_table->ConOut->OutputString(boot_efi_system_table->ConOut, long_string);
}

void boot_print_screen(const char* string) {
    boot_efi_print_console(string);
}

void efi_initialize_gop(driver_efifb_handoff* efifb_handoff) {
    efi_status status;

	efi_guid gop_guid = EFI_GRAPHICS_OUTPUT_PROTOCOL_GUID;
	efi_graphics_output_protocol* gop;
	status = boot_efi_system_table->BootServices->LocateProtocol(&gop_guid, NULL, (void**) &gop);
	if (status) {
        printf("Failed to locate protocol for EFI graphics output\n");
        boot_full_halt();
    }

	efi_graphics_output_mode_information* info;
	uint32_t mode_num;
	size_t info_size = 0;
	status = gop->QueryMode(gop, gop->Mode == NULL ? 0 : gop->Mode->Mode, &info_size, &info);
	if (status == EFI_NOT_STARTED) {
		status = gop->SetMode(gop, 0);
	}
	if (status) {
        printf("Failed to set EFI graphics output mode\n");
        boot_full_halt();
    }
	mode_num = gop->Mode->MaxMode;

    // Find largest resolution
    uint32_t largest_res_mode = 0;
    uint32_t max_w = 0;
	for (int i = 0; i < mode_num; i++) {
		status = gop->QueryMode(gop, i, &info_size, &info);
        if (status) {
            boot_full_halt();
        }
		if (info->HorizontalResolution > max_w) {
            largest_res_mode = i;
            max_w = info->HorizontalResolution;
        }
	}

    // Found the largest one, use it!
    status = gop->SetMode(gop, largest_res_mode);
    if (status) {
        boot_full_halt();
    }

    gfx_pixel_color_format pixel_format = -1;
    switch (gop->Mode->Info->PixelFormat) {
        case PixelRedGreenBlueReserved8BitPerColor:
            pixel_format = RGBR_32;
            break;
        case PixelBlueGreenRedReserved8BitPerColor:
            pixel_format = BGRR_32;
            break;
        default:
            boot_full_halt();
    }

    efifb_handoff->framebuffer = (fbcon_framebuffer){
        .fb = (void*) gop->Mode->FrameBufferBase,
        .size = gop->Mode->FrameBufferSize,
        .width = gop->Mode->Info->HorizontalResolution,
        .height = gop->Mode->Info->VerticalResolution,
        .pps = gop->Mode->Info->PixelsPerScanLine,
        .format = pixel_format,
    };

    use_efifb = true;
}

// info for usable memory: https://hypervsir.blogspot.com/2014/09/approach-to-retrieving-bios-memory-map.html
void efi_populate_meminfo(
    efi_memory_descriptor* memory_map,
    size_t memory_map_size,
    size_t memory_map_descriptor_size,
    uint32_t memory_map_descriptor_version,
    kernel_meminfo* meminfo
) {
    for (int i = 0; i < memory_map_size / memory_map_descriptor_size; i++) {
        if (efi_all_mem_area_count == MEM_MAX_AREAS) {
            boot_full_halt();
        }

        efi_memory_descriptor* mem_desc = (efi_memory_descriptor*) &((char*) memory_map)[i * memory_map_descriptor_size];
        if (mem_desc->Type >= EFI_MEMORY_TYPE_COUNT) {
            boot_full_halt();
        }
        size_t mem_size = mem_desc->NumberOfPages * PAGE_SIZE;
        // We do this here because it's going to be relevant soon
        mem_desc->VirtualStart = mem_desc->PhysicalStart;
        // These are the three that we can use.
        // Boot services have been exited by now so we can reuse the memory.
        if (mem_desc->Type == EfiConventionalMemory ||
            mem_desc->Type == EfiBootServicesCode ||
            mem_desc->Type == EfiBootServicesData ||
            mem_desc->Type == EfiLoaderCode ||
            mem_desc->Type == EfiLoaderData) {

            kernel_meminfo_area* usable_area = &efi_usable_mem_areas[efi_usable_mem_area_count++];
            usable_area->start = (void*) mem_desc->PhysicalStart;
            usable_area->size_bytes = mem_size;
        }

        kernel_meminfo_area* area = &efi_all_mem_areas[efi_all_mem_area_count++];
        area->start = (void*) mem_desc->PhysicalStart;
        area->size_bytes = mem_size;
    }

    meminfo->mem_areas = efi_usable_mem_areas;
    meminfo->mem_area_count = efi_usable_mem_area_count;
    meminfo->all_mem_areas = efi_all_mem_areas;
    meminfo->all_mem_area_count = efi_all_mem_area_count;
    meminfo->page_size = PAGE_SIZE;
}
