// SPDX-License-Identifier: GPL-3.0-or-later

#include <external.uefi.uefi-headers/boot-services.h>
#include <external.uefi.uefi-headers/types.h>

#include <lib/tinystd/printf.h>

#include "common.h"
#include "efi_runtime_vars.h"

// EFI error code strings
const char efi_error_strings[][32] = {
    "EFI_SUCCESS",
    "EFI_LOAD_ERROR",
    "EFI_INVALID_PARAMETER",
    "EFI_UNSUPPORTED",
    "EFI_BAD_BUFFER_SIZE",
    "EFI_BUFFER_TOO_SMALL",
    "EFI_NOT_READY",
    "EFI_DEVICE_ERROR",
    "EFI_WRITE_PROTECTED",
    "EFI_OUT_OF_RESOURCES",
    "EFI_VOLUME_CORRUPTED",
    "EFI_VOLUME_FULL",
    "EFI_NO_MEDIA",
    "EFI_MEDIA_CHANGED",
    "EFI_NOT_FOUND",
    "EFI_ACCESS_DENIED",
    "EFI_NO_RESPONSE",
    "EFI_NO_MAPPING",
    "EFI_TIMEOUT",
    "EFI_NOT_STARTED",
    "EFI_ALREADY_STARTED",
    "EFI_ABORTED",
    "EFI_ICMP_ERROR",
    "EFI_TFTP_ERROR",
    "EFI_PROTOCOL_ERROR",
    "EFI_INCOMPATIBLE_VERSION",
    "EFI_SECURITY_VIOLATION",
    "EFI_CRC_ERROR",
    "EFI_END_OF_MEDIA",
    "EFI_END_OF_FILE",
    "EFI_INVALID_LANGUAGE",
    "EFI_COMPROMISED_DATA",
    "EFI_IP_ADDRESS_CONFLICT",
    "EFI_HTTP_ERROR"
};

const char* efi_error_to_string(efi_status status) {
    if (status < 0 || status > EFI_HTTP_ERROR) {
        return "Unknown error";
    }

    return efi_error_strings[status];
}

size_t efi_get_memory_map(
    efi_memory_descriptor** memory_map,
    size_t* memory_map_size_ptr,
    size_t* memory_map_descriptor_size_ptr,
    uint32_t* memory_map_descriptor_version_ptr
) {
    efi_status status;
    size_t memory_map_key;
    size_t memory_map_size = 0;
    size_t memory_map_descriptor_size = 0;
    uint32_t memory_map_descriptor_version;

    // Get the required memory pool size for the memory map.
    // This will fail with EFI_BUFFER_TOO_SMALL because we're purposefully
    // passing a non-existent buffer to get the required size.
    status = boot_efi_system_table->BootServices->GetMemoryMap(
        &memory_map_size,
        NULL,
        NULL,
        &memory_map_descriptor_size,
        NULL
    );

    if (status != EFI_BUFFER_TOO_SMALL) {
        printf("Expected buffer to be too small but somehow it wasn't.\n");
        boot_full_halt();
    }

    // 2 extra descriptor sizes because the memory map may have grown
    memory_map_size += 2 * memory_map_descriptor_size;

    // We'll retry allocation until we found the sweet spot
    do {
        status = boot_efi_system_table->BootServices->AllocatePool(
            EfiLoaderData,
            memory_map_size,
            (void**) memory_map
        );

        if (status == EFI_BUFFER_TOO_SMALL) {
            printf("Map size too small: %zu, retrying\n", memory_map_size);
            memory_map_size += memory_map_descriptor_size;
        }
    } while (status == EFI_BUFFER_TOO_SMALL);

    if (status != EFI_SUCCESS) {
        printf("An error occured during memory pool allocation: %d\n", status);
        boot_full_halt();
    }

    status = boot_efi_system_table->BootServices->GetMemoryMap(
        &memory_map_size,
        *memory_map,
        &memory_map_key,
        &memory_map_descriptor_size,
        &memory_map_descriptor_version
    );

    if (status != EFI_SUCCESS) {
        printf("Failed to get memory map: %d\n", status);
        boot_full_halt();
    }

    // Apply all memory_map_* variables to parameters
    if (memory_map_size_ptr) {
        *memory_map_size_ptr = memory_map_size;
    }
    if (memory_map_descriptor_size_ptr) {
        *memory_map_descriptor_size_ptr = memory_map_descriptor_size;
    }
    if (memory_map_descriptor_version_ptr) {
        *memory_map_descriptor_version_ptr = memory_map_descriptor_version;
    }

    return memory_map_key;
}

void efi_set_virtual_memory_map(
    size_t memory_map_key,
    efi_memory_descriptor* memory_map,
    size_t memory_map_size,
    size_t memory_map_descriptor_size,
    uint32_t memory_map_descriptor_version
) {
    efi_status status = boot_efi_system_table->RuntimeServices->SetVirtualAddressMap(
        memory_map_size, memory_map_descriptor_size, memory_map_descriptor_version, memory_map);
    if (status != EFI_SUCCESS) {
        boot_full_halt();
    }
}

void exit_boot_services(
    efi_memory_descriptor** memory_map,
    size_t* memory_map_size_ptr,
    size_t* memory_map_descriptor_size_ptr,
    uint32_t* memory_map_descriptor_version_ptr
) {
    printf("Exiting boot services (no more output will be produced by the OS loader)\n");
    size_t memory_map_key = efi_get_memory_map(
        memory_map, memory_map_size_ptr, memory_map_descriptor_size_ptr, memory_map_descriptor_version_ptr);

    // We need silence here. After getting the memory map, we should not print anything
    // because the memory map may have changed and then we can't exit boot services anymore.

    efi_status status = boot_efi_system_table->BootServices->ExitBootServices(efi_handle_instance, memory_map_key);
    if (status) {
        printf("Failed to exit EFI boot services, status: %s", efi_error_to_string(status));
        boot_full_halt();
    }

    efi_set_virtual_memory_map(
        memory_map_key, *memory_map, *memory_map_size_ptr, *memory_map_descriptor_size_ptr, *memory_map_descriptor_version_ptr);
}

void boot_full_halt() __attribute__((noreturn)) {
    // Disable interrupts
    __asm__("cli");
    // Halt CPU (no interrupts = stops forever)
    __asm__("hlt");
    __builtin_unreachable();
}

void boot_asm_hlt() {
    __asm__("hlt");
}

