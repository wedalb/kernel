// SPDX-License-Identifier: GPL-3.0-or-later
#include <stdbool.h>

#include <external.uefi.uefi-headers/boot-services.h>
#include <external.uefi.uefi-headers/runtime-services.h>
#include <external.uefi.uefi-headers/system-table.h>
#include <external.uefi.uefi-headers/types.h>

#include "efi_runtime_vars.h"
#include "../../io/serial.h"
#include "kernel_loader.h"
#include "efi_boot_iface.h"

#define ERR(x) if(EFI_ERROR((x))) return (x)

efi_system_table* boot_efi_system_table;
efi_handle* efi_handle_instance;

efi_status prepare_boot(efi_system_table *st) {
	efi_status status;

	// reset the watchdog timer
	status = st->BootServices->SetWatchdogTimer(0, 0, 0, NULL);
	ERR(status);

	// clear the screen
	status = st->ConOut->ClearScreen(st->ConOut);
	ERR(status);

	// disable all interrupts
	__asm__ ("cli");

	serial_init();

	return EFI_SUCCESS;
}

efi_status efi_main(efi_handle handle, efi_system_table *st) {
	efi_status status;

	efi_handle_instance = handle;
	boot_efi_system_table = st;

	status = prepare_boot(st);
	ERR(status);

	boot_print_screen("OS Kernel Image Loader\n\n");

	// Load and jump into the kernel.
	// We pass our boot interface so that the kernel
	// can call back to us. Bye, bye UEFI.
	efi_boot_kernel();

	return EFI_SUCCESS;
}

// This is not used but we include it so that we can link

void __chkstk() {

}

int _fltused = 0;