// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

void serial_init();
void serial_output(const char* output);
