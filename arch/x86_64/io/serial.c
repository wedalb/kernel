// SPDX-License-Identifier: GPL-3.0-or-later
#include <stdbool.h>

#include "lib/tinystd/sync/spinlock.h"

#include "ports.h"

// Parts of this copied from https://wiki.osdev.org/Serial_Ports

#define PORT 0x3f8          // COM1

bool boot_serial_enabled = false;
static spinlock serial_lock;
static spinlock serial_output_lock;

void serial_init() {
   spinlock_acquire(&serial_lock);

   outb(PORT + 1, 0x00);    // Disable all interrupts
   outb(PORT + 3, 0x80);    // Enable DLAB (set baud rate divisor)
   outb(PORT + 0, 0x03);    // Set divisor to 3 (lo byte) 38400 baud
   outb(PORT + 1, 0x00);    //                  (hi byte)
   outb(PORT + 3, 0x03);    // 8 bits, no parity, one stop bit
   outb(PORT + 2, 0xC7);    // Enable FIFO, clear them, with 14-byte threshold
   outb(PORT + 4, 0x0B);    // IRQs enabled, RTS/DSR set
   outb(PORT + 4, 0x1E);    // Set in loopback mode, test the serial chip
   outb(PORT + 0, 0xAE);    // Test serial chip (send byte 0xAE and check if serial returns same byte)

   // Check if serial is faulty (i.e: not same byte as sent)
   if (inb(PORT + 0) != 0xAE) {
      return;
   }

   // If serial is not faulty set it in normal operation mode
   // (not-loopback with IRQs enabled and OUT#1 and OUT#2 bits enabled)
   outb(PORT + 4, 0x0F);
   boot_serial_enabled = true;

   spinlock_release(&serial_lock);
}

static int serial_received() {
   return inb(PORT + 5) & 1;
}

char serial_read() {
   spinlock_guard(serial_lock,
      while (serial_received() == 0);
   )

   return inb(PORT);
}

static int serial_is_transmit_empty() {
   return inb(PORT + 5) & 0x20;
}

static void serial_write(const char c) {
   spinlock_guard(serial_lock,
      while (serial_is_transmit_empty() == 0);
      outb(PORT, c);
   )
}

void serial_output(const char* output) {
   if (!boot_serial_enabled) {
      return;
   }
   char c;
   spinlock_guard(serial_output_lock, {
      for (int i = 0; (c = output[i]) != 0; i++) {
         serial_write(c);
      }
   })
}
