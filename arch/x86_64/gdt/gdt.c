// SPDX-License-Identifier: GPL-3.0-or-later
#include "include/internal/rings/levels.h"

#include "lib/tinystd/printf.h"
#include "lib/tinystd/mm.h"

#include "gdt.h"

// Reference: https://github.com/Absurdponcho/PonchoOS/tree/Episode-10-GDT/kernel/src/gdt
// Reference: https://blog.llandsmeer.com/tech/2019/07/21/uefi-x64-userland.html

/*
 * The GDT by itself is useless to us.
 * We will be using paging and not segmenting so
 * to pave our way, we'll simply define overlapping
 * regions that are both kernel and user code and data.
 */

__attribute__((aligned(0x1000)))
global_descriptor_table default_gdt = {
    {0, 0, 0, 0x00, 0x00, 0},   // null
    {0, 0, 0, 0x9a, 0xa0, 0},   // kernel code
    {0, 0, 0, 0x92, 0xa0, 0},   // kernel data
    {0, 0, 0, 0x00, 0x00, 0},   // null
    {0, 0, 0, 0x9a, 0xa0, 0},   // user code
    {0, 0, 0, 0x92, 0xa0, 0},   // user data
    {0, 0, 0, 0x9a, 0xa0, 0},   // ovmf code
    {0, 0, 0, 0x92, 0xa0, 0},   // ovmf data
    {0, 0, 0, 0x89, 0xa0, 0},   // tss low
    {0, 0, 0, 0x00, 0x00, 0},   // tss high
};


gdt_descriptor default_gdt_descriptor = {
    .size = sizeof(global_descriptor_table) - 1,
    .offset = (uint64_t) &default_gdt,
};

gdt_tss tss;

static void setup_tss() {
    memset(&tss, 0, sizeof(gdt_tss));
    uint64_t tss_base = ((uint64_t)&tss);
    default_gdt.tss_low.base_low = tss_base & 0xffff;
    default_gdt.tss_low.base_mid = (tss_base >> 16) & 0xff;
    default_gdt.tss_low.base_high = (tss_base >> 24) & 0xff;
    default_gdt.tss_low.limit_low = sizeof(tss);
    default_gdt.tss_high.limit_and_flags = (tss_base >> 32) & 0xffff;
    default_gdt.tss_high.base_mid = (tss_base >> 48) & 0xffff;
}

void gdt_load_table(register gdt_descriptor* gdt)
__attribute__((disable_tail_calls))
__attribute__((noinline))
{
    // The compiler saves the rbp and moves it to rsp
    // before the GDT is loaded.

    // Load the GDT
    __asm__ volatile ("lgdt %0" : : "m"(*gdt));

    // Load TSS
    __asm__ volatile (
        "movw $0x40, %ax\n"
        "ltr %ax"
    );
    __asm__ volatile (
        "movw $0x10, %ax\n"
        "movw %ax, %ds\n"
        "movw %ax, %es\n"
        "movw %ax, %fs\n"
        "movw %ax, %gs\n"
        "movw %ax, %ss\n"
    );

    // Pop the saved rbp and return address off the stack
    __asm__ volatile ("pop %rbp");
    __asm__ volatile ("pop %r10");

    // Push the code segment and return address
    __asm__ volatile ("movw $0x08, %ax");
    __asm__ volatile ("push %rax");
    __asm__ volatile ("push %r10");

    // Do a far return which will load the code segment and jump to the return address
    __asm__ volatile ("lretq");
}

void gdt_load() __attribute__((disable_tail_calls)) {
    setup_tss();

    gdt_descriptor* gdt_descriptor = &default_gdt_descriptor;

    printf("Loading GDT table %p using descriptor at %p, size: %zu\n",
           &default_gdt, gdt_descriptor, default_gdt_descriptor.size);
    gdt_load_table(gdt_descriptor);
    printf("GDT table loaded.\n");
}
