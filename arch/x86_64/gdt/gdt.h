// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

// https://wiki.osdev.org/Global_Descriptor_Table

#define GDT_DESCRIPTOR_TYPE_SYSTEM 0
#define GDT_DESCRIPTOR_TYPE_CODE_DATA 1

#define GDT_DIRECTION_UP 0
#define GDT_DIRECTION_DOWN 1

/** Code in this segment can only be executed from the ring set in privl */
#define GDT_CONFORM_ONLY_SPECIFIED_RING 0
/**
 * Code in this segment can be executed from an equal
 * or lower privilege level. For example, code in ring 3 can
 * far-jump to conforming code in a ring 2 segment. The privl-bits
 * represent the highest privilege level that is allowed to execute
 * the segment. For example, code in ring 0 cannot far-jump to a
 * conforming code segment with privl==0x2, while code in ring 2 and 3
 * can.
 *
 * Note that the privilege level remains the same, ie. a far-jump from
 * ring 3 to a privl==2-segment remains in ring 3 after the jump.
 */
#define GDT_CONFORM_SPECIFIED_OR_LOWER_RING 1

#define GDT_RW_ONLY_CODE_EXECUTION 0
#define GDT_RW_ONLY_DATA_READ 0
#define GDT_RW_ALLOW_CODE_READ 1
#define GDT_RW_ALLOW_DATA_WRITE 1

#define LIMIT_MED(b) (b << 4 >> 8)

/**
 * The GDT descriptor is not part of the GDT itself.
 * It is used by the LGDT instruction to tell where to
 * look for the actual GDT. Thus, we need to specify
 * where it is and how big it is (minus one).
 */
typedef struct {
    /**
     * Size of the GDT - 1
     */
    uint16_t size;
    /**
     * Offset of the GDT table
     */
    uint64_t offset;
} __attribute__((packed)) gdt_descriptor;

/**
 * These are the GDT entry access flags.
 * They are used to specify what a segment is (i. e. its properties)
 * and who is allowed to access it.
 */
typedef struct {
    /**
     * Present bit
     *
     * Whether this segment is present.
     * Has to be set to true for all valid selectors.
     */
    bool present : 1;
    /**
     * Privilege (2 bits: 0, 1, 2, 3)
     *
     * The ring level allowed to access this segment.
     */
    uint8_t ring_privilege : 2;
    /**
     * Descriptor type
     *
     * Settings this to 1 will make it a code/data segment.
     * 0 is used for system segments.
     */
    uint8_t descriptor_type : 1;
    /**
     * Executable bit
     *
     * Whether this segment is executable.
     * Setting this to true will cause it to be
     * a code segment instead of a data segment.
     * This will affect the meaning of the other flags
     * and how they behave.
     */
    bool executable : 1;
    /**
     * Direction bit/Conforming bit
     *
     * This one is a bit weird.
     *
     * Data selectors (direction bit):
     *   if set to 0, the segment grows up
     *   if set to 1, the segment grows down: offset must be > limit
     * Code selectors (confirming bit):
     *   if set to 0, code in this segment can only be
     *                executed from the ring set in the
     *                ring_privilege field
     *   if set to 1, code in this segment can be executed
     *                from a privilege level <= ring_privilege.
     *                ring_privilege would represent the highest
     *                level that is allowed to execute this segment.
     *                For example, code in ring 0 cannot far-jump
     *                to a conforming code segment with
     *                ring_privilege = 0x2 (ring 2) while code in
     *                ring 2 (0x2) and ring 3 (0x3) can. The privilege
     *                level remains the same, so jumping into this segment
     *                will preserve the ring level.
     */
    uint8_t direction_or_access : 1;
    /**
     * Readable bit (for code sections) or
     * Writable bit (for data sections)
     *
     * Code sections:
     *   if set to 1, allows reading code
     *   if set to 0, only allows execution
     * Data sections:
     *   if set to 1, allows writing data
     *   if set to 0, only allows reading data
     */
    uint8_t rw : 1;
    /**
     * Accessed bit
     *
     * The cpu sets this field to true when
     * it is being accessed. We don't have to do
     * anything here and can just leave it at 0.
     */
    bool is_being_accessed : 1;
} __attribute__((packed)) gdt_access_flags;

typedef struct {
    // byte granularity = 1 B blocks (0)
    // page granularity = 4 KiB blocks (1)
    /** Granularity bit */
    bool use_page_granularity : 1;
    /**
     * Size bit
     *
     * Specifies whether this is a 32 bit protected mode
     * selector. Setting this to false will make it a 16 bit one.
     * You probably want to always set this to true.
     */
    bool is_32bit : 1;

    //// x86_64 territory

    /**
     * L bit
     *
     * Indicates a x86-64 code descriptor.
     * For data segments, this bit is reserved.
     */
    bool is_64bit_code : 1;

    /**
     * Sz bit (but for 64 bit)
     *
     * Must be 0 if L is set.
     * Sz = 1, L = 1 is reserved for future use (throws exception if used)
     */
    uint8_t sz_bit_64 : 1;
} __attribute__((packed)) gdt_flags;

typedef struct {
    uint8_t limit_mid : 4;
    gdt_flags flags;
} __attribute__((packed)) gdt_limit_and_flags;

typedef struct {
    // The limit is essentially a 32 bit unsigned integer
    // It's split up into multiple fields.
    // The base is also split up.
    // We also have access flags and some other flags in here.
    uint16_t limit_low;
    uint16_t base_low;
    uint8_t base_mid;
    /*gdt_access_flags */ uint8_t access_flags;
    /*gdt_limit_and_flags*/ uint8_t limit_and_flags;
    uint8_t base_high;
} __attribute__((packed)) gdt_entry;

typedef struct {
    uint32_t reserved0;
    uint64_t rsp0;
    uint64_t rsp1;
    uint64_t rsp2;
    uint64_t reserved1;
    uint64_t ist1;
    uint64_t ist2;
    uint64_t ist3;
    uint64_t ist4;
    uint64_t ist5;
    uint64_t ist6;
    uint64_t ist7;
    uint64_t reserved2;
    uint16_t reserved3;
    uint16_t iopb_offset;
}
__attribute__((packed))
gdt_tss;

/**
 * This is our GDT.
 * Every GDT needs a null entry at the beginning.
 * (That's just how it is).
 * Then we specify kernel segments as well as userspace segments.
 * The kernel segments are obviously only for kernelspace and can't
 * be accessed by userspace.
 * The user segments also include a null entry.
 */
typedef struct {
    gdt_entry null;
    gdt_entry kernel_code;
    gdt_entry kernel_data;
    gdt_entry user_null;
    gdt_entry user_code;
    gdt_entry user_data;
    gdt_entry ovmf_code;
    gdt_entry ovmf_data;
    gdt_entry tss_low;
    gdt_entry tss_high;
}
__attribute__((packed))
__attribute__((aligned(0x1000)))
global_descriptor_table;

/**
 * This is the default GDT that we're going to be using.
 */
extern global_descriptor_table default_gdt;

void gdt_load();
