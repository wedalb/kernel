// SPDX-License-Identifier: GPL-3.0-or-later
#include "kernel/entry.h"
#include <drivers/video/fb/console.h>

#include <kernel/boot_iface.h>
#include <kernel/drivers.h>
#include <kernel/streams.h>

void boot_print(const char* str) {
    serial_output(str);
    fbcon_write_text(str);
}

void boot_arch_prepare_display() {
    kernel_driver_require(KERNEL_DRIVER_EFIFB, boot_iface->efifb_handoff);
    pipe_stream_set_default_out_write_text(boot_print);
}