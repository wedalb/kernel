// SPDX-License-Identifier: GPL-3.0-or-later

#include <lib/tinystd/mm.h>
#include <lib/tinystd/str.h>
#include <lib/tinystd/printf.h>

#include "../interrupts/idt.h"
#include "../interrupts/handlers/keyboard.h"
#include "../interrupts/pic.h"
#include "../io/serial.h"
#include "../io/ports.h"

void boot_arch_prepare_interrupts() {
    // Erase the IDT
    memset(idt_entries, 0, sizeof(idt_entry) * IDT_ENTRY_COUNT);

    idt_desc.offset = (uintptr_t) idt_entries;
    printf("IDT entries at %p, size: %d\n", idt_entries, idt_desc.size);

    // Register a keyboard handler
    idt_register_typical_interrupt(0x21, interrupt_handler_keyboard);

    // Load the IDT
    printf("Loading IDT, size: %d, offset: %p\n", idt_desc.size, idt_desc.offset);
    __asm__ volatile ("lidt %0" : : "m" (idt_desc));

    // Remap the PIC
    pic_remap();

    // Enable the PIC
    pic_enable();

    printf("Enabling interrupts\n");
    __asm__ volatile ("sti");

    printf("Interrupts have been enabled\n");
}
