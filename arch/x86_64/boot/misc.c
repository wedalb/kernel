// SPDX-License-Identifier: GPL-3.0-or-later

void boot_arch_asm_hlt() {
    __asm__ ("hlt");
}

void boot_arch_full_halt() __attribute__((noreturn)) {
    __asm__ ("cli");
    boot_arch_asm_hlt();
    __builtin_unreachable();
}
