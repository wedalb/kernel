// SPDX-License-Identifier: GPL-3.0-or-later

#include <lib/tinystd/math.h>
#include <lib/tinystd/mm.h>
#include <lib/tinystd/printf.h>
#include <lib/tinystd/stddef.h>

#include <include/internal/mm/stack.h>
#include <include/internal/mm/paging.h>

#include <kernel/boot_iface.h>
#include <kernel/entry.h>
#include <kernel/image.h>
#include <kernel/mm/mem_iface.h>
#include <kernel/mm/mem_info.h>
#include <kernel/mm/paging.h>

#include <arch/x86_64/gdt/gdt.h>
#include <arch/x86_64/mm/paging.h>
#include <arch/x86_64/mm/e820.h>

int kernel_image_skip_range;

void boot_arch_prepare_memory() {
    gdt_load();
}

static inline size_t kernel_image_aligned_size() {
    size_t size = kernel_image_size();
    size_t rem = size % PAGE_SIZE;
    if (rem == 0) {
        return size;
    } else {
        return size + PAGE_SIZE - rem;
    }
}

void boot_arch_memory_management_fill_info(kernel_meminfo* info) {
    memcpy(info, boot_iface->meminfo, sizeof(kernel_meminfo));
}

void arch_memory_management_initialize(kernel_meminfo* meminfo) {
    if (meminfo->mem_area_count == 0) {
        printf("No memory areas found, this should not happen!\n");
        boot_arch_full_halt();
    }

    // Essentially marks this region as allocated
    kernel_image_skip_range = paging_add_skip_range(&kernel_image_start, &kernel_image_end);

    memzero(&pml4, sizeof(page_mapping_table));
    for (int i = 0; i < meminfo->all_mem_area_count; i++) {
        kernel_meminfo_area area = meminfo->all_mem_areas[i];
        paging_map_range(area.start, area.start, area.size_bytes);
    }

    void* kernel_image_phys_addr = &kernel_image_start;
    paging_map_range(kernel_image_phys_addr, &kernel_image_start, kernel_image_aligned_size());

    paging_load_page_table(&pml4);
}

void* memory_higher_half_base() {
    return (void*) 0xffffffff80000000;
}

#define set_stack_pointer(p) \
    __asm__ volatile ("mov %0, %%rsp" : : "r"(p));


void __attribute__((noinline)) boot_arch_remap_stack() __attribute__((disable_tail_calls)) {
    void* new_stackptr;
    void* first_stackptr = boot_iface->stack_pointer_before_kernel;
    size_t real_stack_size;
    void* stackptr;

    size_t stack_size = align(4 * 1024 * 1024, PAGE_SIZE);
    void* virt_stack_hint = (void*) (UINTPTR_MAX - stack_size);
    void* new_stack = kernel_mm_alloc_at(stack_size, virt_stack_hint);
    memzero(new_stack, stack_size);

    stackptr = stack_pointer();
    real_stack_size = first_stackptr - stackptr;
    new_stackptr = new_stack + stack_size - real_stack_size;
    memcpy(new_stackptr, stackptr, real_stack_size);

    // We have to call it inside this function because otherwise
    // we will be modifying the stack before we're using the new one.
    // Also, aligning the stack pointer to 16 bytes is required by the ABI.
    set_stack_pointer(new_stackptr + 0x10);
}
