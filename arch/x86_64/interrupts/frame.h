// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stdint.h>

typedef struct interrupt_frame_st {
    uint16_t ip;
    uint16_t cs;
    uint16_t flags;
    uint16_t sp;
    uint16_t ss;
}
__attribute__((packed))
interrupt_frame;

