// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include "../frame.h"

__attribute__ ((interrupt))
__attribute__ ((sysv_abi))
void interrupt_handler_keyboard(interrupt_frame* frame);

