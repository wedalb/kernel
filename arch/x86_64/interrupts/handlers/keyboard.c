// SPDX-License-Identifier: GPL-3.0-or-later

#include "drivers/keyboard/keyboard.h"

#include "lib/tinystd/printf.h"

#include "../pic.h"
#include "../../io/ports.h"
#include "../frame.h"

__attribute__((interrupt))
__attribute__((sysv_abi))
void interrupt_handler_keyboard(interrupt_frame* frame) {
    // read the keycode
    uint8_t keycode = inb(0x60);
    driver_keyboard_event event = {
        .keycode = keycode,
        .is_down = !(keycode & 0x80)
    };
    driver_keyboard_on_event(&event);
    pic_send_eoi(1);
}
