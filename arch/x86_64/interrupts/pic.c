// SPDX-License-Identifier: GPL-3.0-or-later
#include <stdint.h>

#include "pic.h"
#include "../io/ports.h"

void pic_end_master(){
    outb(PIC1_COMMAND, PIC_EOI);
}

void pic_end_slave(){
    outb(PIC2_COMMAND, PIC_EOI);
    outb(PIC1_COMMAND, PIC_EOI);
}

void pic_remap() {
    // remap the PIC
    // save the masks
    uint8_t mask1 = inb(PIC1_DATA);
    uint8_t mask2 = inb(PIC2_DATA);

    // start the initialization sequence
    outb(PIC1_COMMAND, PIC_INIT);
    io_wait();
    outb(PIC2_COMMAND, PIC_INIT);

    // set the offsets
    outb(PIC1_DATA, PIC1_OFFSET);
    io_wait();
    outb(PIC2_DATA, PIC2_OFFSET);
    io_wait();

    // tell the PICs about each other
    outb(PIC1_DATA, 4);
    io_wait();
    outb(PIC2_DATA, 2);
    io_wait();

    // set the mode
    outb(PIC1_DATA, PIC_8086);
    io_wait();

    outb(PIC2_DATA, PIC_8086);
    io_wait();

    // restore the masks
    outb(PIC1_DATA, mask1);
    outb(PIC2_DATA, mask2);
}

void pic_disable() {
    pic_mask_primary(0xff);
    pic_mask_secondary(0xff);
}

void pic_enable() {
    // Enable the keyboard interrupt
    pic_mask_primary(0xfd);
    pic_mask_secondary(0xff);
}

void pic_mask_primary(uint8_t mask) {
    outb(PIC1_DATA, mask);
    io_wait();
}

void pic_mask_secondary(uint8_t mask) {
    outb(PIC2_DATA, mask);
    io_wait();
}

__attribute__ ((no_caller_saved_registers))
void pic_send_eoi(uint8_t irq) {
    if (irq >= 8) {
        outb(PIC2_COMMAND, PIC_EOI);
    }
    outb(PIC1_COMMAND, PIC_EOI);
}
