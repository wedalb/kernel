// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stdint.h>

// https://github.com/Absurdponcho/PonchoOS/blob/Episode-12-More-IDT/kernel/src/interrupts/interrupts.h

#define PIC1_COMMAND 0x20
#define PIC1_DATA 0x21
#define PIC2_COMMAND 0xA0
#define PIC2_DATA 0xA1
#define PIC_EOI 0x20

#define PIC_INIT 0x10
#define PIC_ICW4 0x01
#define PIC_8086 0x01

#define PIC1_OFFSET 0x20
#define PIC2_OFFSET 0x28

void pic_end_master();
void pic_end_slave();
void pic_remap();

void pic_mask_primary(uint8_t mask);
void pic_mask_secondary(uint8_t mask);
void pic_enable();
void pic_disable();
__attribute__((no_caller_saved_registers)) void pic_send_eoi(uint8_t irq);
