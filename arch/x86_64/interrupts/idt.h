// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stdint.h>

#include "frame.h"
#include "itr_func.h"

// https://wiki.osdev.org/Interrupt_Descriptor_Table#Structure_AMD64
#define IDT_TA_INTERRUPT_GATE   0b10001110
#define IDT_TA_CALL_GATE        0b10001100
#define IDT_TA_TRAP_GATE        0b10001111

#define IDT_ENTRY_COUNT 256

typedef struct {
    uint16_t offset_low;
    uint16_t selector;
    uint8_t int_stack_table_offs;
    uint8_t type_attr;
    uint16_t offset_mid;
    uint32_t offset_high;
    uint32_t _;
} __attribute__((packed,aligned(8))) idt_entry;

typedef struct {
    uint16_t size;
    uint64_t offset;
} __attribute__((packed,aligned(8))) idt_descriptor;

void idt_entry_set_offset(idt_entry* entry, uint64_t offset);
uint64_t idt_entry_get_offset(idt_entry* entry);
void idt_register_interrupt(
    int offs,
    uint8_t type_attr,
    uint16_t selector,
    INTERRUPT_FUNC(itr_func)
);
void idt_register_typical_interrupt(
    int offs,
    INTERRUPT_FUNC(itr_func)
);

extern idt_descriptor idt_desc;
extern idt_entry idt_entries[];