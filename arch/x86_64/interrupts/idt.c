// SPDX-License-Identifier: GPL-3.0-or-later
#include <stdint.h>

#include "lib/tinystd/printf.h"

#include "idt.h"
#include "frame.h"
#include "itr_func.h"

idt_descriptor idt_desc = {
    .size = sizeof(idt_entry) * IDT_ENTRY_COUNT,
};

idt_entry idt_entries[IDT_ENTRY_COUNT] = {};

void idt_entry_set_offset(idt_entry* entry, uint64_t offset) {
    entry->offset_low = offset & 0xFFFF;
    entry->offset_mid = (offset >> 16) & 0xFFFF;
    entry->offset_high = (offset >> 32) & 0xFFFFFFFF;
}

uint64_t idt_entry_get_offset(idt_entry* entry) {
    return entry->offset_low | (entry->offset_mid << 16) | ((uint64_t) entry->offset_high << 32);
}

void idt_register_interrupt(
    int offs,
    uint8_t type_attr,
    uint16_t selector,
    INTERRUPT_FUNC(itr_func)
) {
    printf("Registering interrupt %d, type_attr %x, sel %x, func at %p\n", offs, type_attr, selector, itr_func);
    idt_entry* entry = &idt_entries[offs];
    idt_entry_set_offset(entry, (uint64_t) itr_func);
    entry->selector = selector;
    entry->type_attr = type_attr;
}

void idt_register_typical_interrupt(
    int offs,
    INTERRUPT_FUNC(itr_func)
) {
    idt_register_interrupt(offs, IDT_TA_INTERRUPT_GATE, 0x08, itr_func);
}