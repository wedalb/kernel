// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#define INTERRUPT_FUNC(name) __attribute__((interrupt)) __attribute__((sysv_abi)) void (*name)(interrupt_frame* frame)
