// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <lib/tinystd/stddef.h>

typedef struct {
    bool should_ignore : 1;
    bool is_non_volatile : 1;
    uint32_t _ : 30;
}
__attribute__((packed))
e820_acpi_30_extended_attrs;

typedef struct {
    uint64_t base_addr;
    uint64_t length;
    uint32_t type;
    e820_acpi_30_extended_attrs acpi_30_extended_attrs;
}
__attribute__ ((packed))
e820_entry;

size_t e820_get_entry_count();
