// SPDX-License-Identifier: GPL-3.0-or-later
#include <stdint.h>

#include "kernel/boot_iface.h"
#include "kernel/drivers.h"
#include "kernel/mm/alloc.h"
#include "kernel/mm/mem_info.h"
#include "kernel/entry.h"
#include "kernel/mm/paging.h"
#include "lib/tinystd/endianness/convert.h"
#include "lib/tinystd/math.h"
#include "lib/tinystd/mm.h"
#include "lib/tinystd/printf.h"
#include "lib/tinystd/sync/spinlock.h"

#include "paging.h"

// Reference: https://blog.llandsmeer.com/tech/2019/07/21/uefi-x64-userland.html

#define STACK_SIZE (4 * 1024 * 1024)
#define STACK_ADDR ((void*) (UINTPTR_MAX - STACK_SIZE))

#define AS_VIRT_ADDR(addr) (*(paging_virtual_address*) &addr)
#define AS_ADDR(virt_addr) (*(void**) &virt_addr)

#define LV1_PAGE_SIZE PAGE_SIZE
#define LV2_PAGE_SIZE (PAGE_SIZE * sizeof(page_mapping_table))
#define LV3_PAGE_SIZE (PAGE_SIZE * sizeof(page_mapping_table[sizeof(page_mapping_table)]))

__attribute__((aligned(PAGE_SIZE)))
page_mapping_table pml4 = {
    .entries = {},
};

typedef struct {
    uintptr_t start;
    uintptr_t exclusive_end;
} addr_range;

#define SKIP_RANGES_LEN 4
static uintptr_t last_page = 0;
static int skip_range_ix = 0;
static addr_range skip_ranges[SKIP_RANGES_LEN] = {};
bool paging_alloc_is_frozen = false;

static spinlock page_map_lock;
static spinlock page_map_range_lock;

static page_mapping paging_get_mapping(void* virt_addr);

static void* find_closest_available_page_to(uintptr_t target_page) {
    if (kernel_mem_info.mem_area_count == 0) {
        printf("No memory areas, cannot continue!\n");
        boot_arch_full_halt();
    }
    if (paging_alloc_is_frozen) {
        printf(
            "This basic alloc method cannot be used anymore as the kernel has "
            "already taken control of it."
        );
        boot_arch_full_halt();
    }

    bool is_in_skips = false;
    do {
        is_in_skips = false;
        for (int i = 0; i < SKIP_RANGES_LEN; i++) {
            if (target_page >= skip_ranges[i].start && target_page < skip_ranges[i].exclusive_end) {
                is_in_skips = true;
                target_page = skip_ranges[i].exclusive_end;
                break;
            }
        }
    } while (is_in_skips);
    uintptr_t closest_page = 0;
    uintptr_t last_area_start = 0;
    size_t last_area_size = 0;

    if (last_page == 0 && kernel_mem_info.mem_areas[0].size_bytes >= PAGE_SIZE) {
        // Very first page, just take the beginning
        closest_page = (uintptr_t) kernel_mem_info.mem_areas[0].start;
        if (closest_page == 0) {
            if (kernel_mem_info.mem_areas[0].size_bytes >= PAGE_SIZE * 2) {
                // We don't want address 0x0, take the next page
                closest_page += PAGE_SIZE;
                goto end;
            }
            printf(
                "basic_alloc: first area is unusable because its address is 0 and only contains one page\n"
            );
            // In case we can't use the first area, just do a normal walk
        } else {
            goto end;
        }
    }

    for (int i = 0; kernel_mem_info.mem_area_count; i++) {
        kernel_meminfo_area area = kernel_mem_info.mem_areas[i];
        if (target_page < (uintptr_t) area.start) {
            // So now we've surpassed the area that COULD hold our
            // target page. Let's check whether it's actually in bounds.
            // If not, we'll use this one.
            if (last_area_start == 0) {
                // There isn't any last area. So we have to use this one.
                // We'll take up the very beginning of this area.
                // The next alloc won't run into this.
                closest_page = (uintptr_t) area.start;
                // Align the page to page boundary
                closest_page = align(closest_page, PAGE_SIZE);
                // Check if the page is in bounds
                if (closest_page + PAGE_SIZE <= (uintptr_t) area.start + area.size_bytes) {
                    // We are in bounds, so proceed
                    break;
                }
            }
            if (target_page >= (uintptr_t) last_area_start && target_page + PAGE_SIZE < last_area_start + last_area_size) {
                // Yes! We can use this as-is.
                closest_page = target_page;
                // Align the page to page boundary
                closest_page = align(closest_page, PAGE_SIZE);
                // Check if the page is in bounds
                if (closest_page + PAGE_SIZE <= (uintptr_t) area.start + area.size_bytes) {
                    // We are in bounds, so proceed
                    break;
                }
            } else {
                // Oh no! It doesn't fit. Let's use the new one then.
                closest_page = (uintptr_t) area.start;
                // Align the page to page boundary
                closest_page = align(closest_page, PAGE_SIZE);
                // Check if the page is in bounds
                if (closest_page + PAGE_SIZE <= (uintptr_t) area.start + area.size_bytes) {
                    // We are in bounds, so proceed
                    break;
                }
            }
        }

        last_area_start = (uintptr_t) area.start;
        last_area_size = area.size_bytes;
    }

end:
    if (closest_page == 0) {
        printf("FATAL: memory allocation failure, could not find page after last page %p\n",
                last_page);
        boot_arch_full_halt();
    }

    return (void*) closest_page;
}

void* basic_alloc_page() {
    if (paging_alloc_is_frozen) {
        return kernel_mm_alloc_page_phys();
    }
    page_mapping mapping;
    void* new_page;
    do {
        new_page = find_closest_available_page_to(last_page + PAGE_SIZE);
        last_page = (uintptr_t) new_page;
        mapping = paging_get_mapping(new_page);
    } while (mapping.phys_addr != new_page);
    if ((uintptr_t) new_page % PAGE_SIZE != 0) {
        printf("Allocated page at %p (%d) is not page-aligned!\n", new_page, new_page);
        boot_arch_full_halt();
    }
    return new_page;
}

void* basic_alloc_at(void* addr_hint, size_t size) {
    size_t pages = (size + PAGE_SIZE - 1) / PAGE_SIZE;
    void* alloc_addr = find_closest_available_page_to((uintptr_t) addr_hint);
    for (size_t p = 1; p < pages; p++) {
        void* next_addr = find_closest_available_page_to((uintptr_t) alloc_addr + PAGE_SIZE * p);
        if (next_addr - alloc_addr != p * PAGE_SIZE) {
            printf("Cannot allocate contiguous block of memory of size %zu\n", size);
            boot_arch_full_halt();
        }
    }

    paging_add_skip_range(alloc_addr, alloc_addr + pages * PAGE_SIZE);

    return alloc_addr;
}

static void sign_extend(paging_virtual_address* virt_addr) {
    virt_addr->sign_extension = *(uint64_t*) virt_addr >> 47 & 1 ? 0xffff : 0x0000;
}

static uintptr_t to_pt_addr(void* addr) {
    return (uintptr_t) addr >> PAGE_ADDR_SHIFT;
}

static void* from_pt_addr(uintptr_t pt_addr) {
    return (void*) (pt_addr << PAGE_ADDR_SHIFT);
}

static void paging_reload_page_table() {
    // Reloading is done by rewriting cr3
    page_mapping_table* table = paging_get_current_pml4();
    if (table == &pml4) {
        __asm__ volatile ("mov %0, %%cr3" : : "r"(table));
    }
}

void paging_map_page_direct(void* phys_addr, void* virt_addr) {
    paging_virtual_address virt_addr_s = *(paging_virtual_address*) &virt_addr;
    sign_extend(&virt_addr_s);
    bool need_reload = false;

    if (!pml4.entries[virt_addr_s.pml4_index].present) {
        void* pdpt_alloc = basic_alloc_page();
        memzero(pdpt_alloc, PAGE_SIZE);
        pml4.entries[virt_addr_s.pml4_index] = (page_table_entry){
            .present = true,
            .writable = true,
            .phys_addr = to_pt_addr(pdpt_alloc),
        };
        need_reload = true;
    }

    page_mapping_table* pdpt = (page_mapping_table*) from_pt_addr(pml4.entries[virt_addr_s.pml4_index].phys_addr);
    /* and repeat the same process for the PDPT */
    if (!pdpt->entries[virt_addr_s.pdpt_index].present) {
        void* pdt_alloc = basic_alloc_page();
        memzero(pdt_alloc, PAGE_SIZE);
        pdpt->entries[virt_addr_s.pdpt_index] = (page_table_entry){
            .present = true,
            .writable = true,
            .phys_addr = to_pt_addr(pdt_alloc),
        };
    }

    page_mapping_table* pdt = (page_mapping_table*) from_pt_addr(pdpt->entries[virt_addr_s.pdpt_index].phys_addr);
    if (!pdt->entries[virt_addr_s.page_directory_index].present) {
        void* pt_alloc = basic_alloc_page();
        memzero(pt_alloc, PAGE_SIZE);
        pdt->entries[virt_addr_s.page_directory_index] = (page_table_entry){
            .present = true,
            .writable = true,
            .phys_addr = to_pt_addr(pt_alloc),
        };
    }

    page_mapping_table* pt = (page_mapping_table*) from_pt_addr(pdt->entries[virt_addr_s.page_directory_index].phys_addr);
    pt->entries[virt_addr_s.page_table_index] = (page_table_entry){
        .present = true,
        .writable = true,
        .phys_addr = to_pt_addr(phys_addr),
    };

    if (need_reload) {
        paging_reload_page_table();
    }

}

void paging_map_page(void* phys_addr, void* virt_addr) {
    spinlock_guard(page_map_lock, paging_map_page_direct(phys_addr, virt_addr))
}

void paging_map_identity_page(void *phys_addr) {
    paging_map_page(phys_addr, phys_addr);
}

void paging_map_identity_page_direct(void *phys_addr) {
    paging_map_page_direct(phys_addr, phys_addr);
}

void paging_map_addr_range(void* phys_start, void* phys_exclusive_end, void* virt_start) {
    if ((uintptr_t) phys_start % PAGE_SIZE != 0) {
        printf("page map addr range: start addr is not page-aligned (%d)\n", PAGE_SIZE);
        boot_arch_full_halt();
    }
    if ((uintptr_t) phys_exclusive_end % PAGE_SIZE != 0) {
        printf("page map addr range: exclusive end addr is not page-aligned (%d)\n", PAGE_SIZE);
        boot_arch_full_halt();
    }

    uintptr_t delta = 0;
    for (void* addr = phys_start; addr < phys_exclusive_end; addr += PAGE_SIZE, delta += PAGE_SIZE) {
        void* virt_addr = virt_start + delta;
        paging_map_page(addr, virt_addr);
    }
}

void paging_map_range_direct(void* phys_start, void* virt_start, size_t len) {
    paging_map_addr_range(phys_start, phys_start + len, virt_start);
}

void paging_map_range(void* phys_start, void* virt_start, size_t len) {
    spinlock_guard(page_map_range_lock, paging_map_range_direct(phys_start, virt_start, len))
}

void paging_map_identity_range(void* phys_start, size_t len) {
    paging_map_addr_range(phys_start, phys_start + len, phys_start);
}

page_mapping_table* paging_get_current_pml4() {
    uintptr_t table;
    __asm__ ("mov %%cr3, %0" : "=r"(table));
    return (page_mapping_table*) table;
}

void paging_load_page_table(page_mapping_table* pml4) {
    __asm__ volatile ("mov %0, %%cr3" : : "r"(pml4));
}

int paging_add_skip_range(void* start, void* exclusive_end) {
    if (skip_range_ix == SKIP_RANGES_LEN) {
        printf("Cannot add more skip ranges, reached max of %d. Increase SKIP_RANGES_LEN to fix.\n",
                skip_range_ix, SKIP_RANGES_LEN);
        boot_arch_full_halt();
    }

    skip_ranges[skip_range_ix] = (addr_range){
        .start = (uintptr_t) start,
        .exclusive_end = (uintptr_t) exclusive_end
    };

    return skip_range_ix++;
}

void paging_remove_skip_range(int index) {
    if (index >= SKIP_RANGES_LEN) {
        printf("Invalid skip range index %d (len: %d)\n", index, SKIP_RANGES_LEN);
        boot_arch_full_halt();
    }

    if (index == SKIP_RANGES_LEN - 1) {
        skip_ranges[index] = (addr_range){0, 0};
    } else {
        memmove(&skip_ranges[index], &skip_ranges[index + 1], sizeof(addr_range) * (SKIP_RANGES_LEN - index - 1));
    }
    skip_range_ix--;
}

uintptr_t paging_physical_addr_from_virt(void* addr) {
    paging_virtual_address virt = *(paging_virtual_address*) &addr;
    page_mapping_table* pml4 = paging_get_current_pml4();
    page_table_entry* pml4_entry = &pml4->entries[virt.pml4_index];
    if (!pml4_entry->present) {
        printf("Invalid virtual address %p, cannot find PML4 entry at index %d\n", addr, virt.pml4_index);
pml4_print:
        printf("PML4: %d %d %d %p\n",
                pml4_entry->writable, pml4_entry->global, pml4_entry->accessed,
                from_pt_addr(pml4_entry->phys_addr));
        boot_arch_full_halt();
    }
    if (!pml4_entry->phys_addr) {
        printf("PML4 at %p has phys addr of 0!\n");
        goto pml4_print;
    }


    page_mapping_table* pdpt = from_pt_addr(pml4_entry->phys_addr);
    page_table_entry* pdpt_entry = &pdpt->entries[virt.pdpt_index];
    if (!pdpt_entry->present) {
        printf("Invalid virtual address %p, cannot find PDPT entry at index %d\n", addr, virt.pdpt_index);
pdpt_print:
        printf("PDPT: %d %d %d %d %p\n",
                pdpt_entry->writable, pdpt_entry->global, pdpt_entry->accessed, pdpt_entry->huge_page,
                from_pt_addr(pdpt_entry->phys_addr));
        goto pml4_print;
    }
    if (!pdpt_entry->phys_addr) {
        printf("PDPT at %p has phys addr of 0!\n", pdpt_entry);
        goto pdpt_print;
    }
    if (pdpt_entry->huge_page) {
        // This is a huge page, we have to return right now because this already is the page
        return (uintptr_t) from_pt_addr(pdpt_entry->phys_addr) + virt.page_offset;
    }

    page_mapping_table* pdt = from_pt_addr(pdpt_entry->phys_addr);
    page_table_entry* pdt_entry = &pdt->entries[virt.page_directory_index];
    if (!pdt_entry->present) {
        printf("Invalid virtual address %p, cannot find PDT entry at index %d\n", addr, virt.page_directory_index);
pdt_print:
        printf("PDT: %d %d %d %d %p\n",
                pdt_entry->writable, pdt_entry->global, pdt_entry->accessed, pdt_entry->huge_page,
                from_pt_addr(pdt_entry->phys_addr));
        goto pdpt_print;
    }
    if (!pdt_entry->phys_addr) {
        printf("PDT at %p has phys addr of 0!\n", pdt_entry);
        goto pdt_print;
    }
    if (pdt_entry->huge_page) {
        // This is a huge page, we have to return right now because this already is the page
        return (uintptr_t) from_pt_addr(pdt_entry->phys_addr) + virt.page_offset;
    }


    page_mapping_table* pt = from_pt_addr(pdt_entry->phys_addr);
    page_table_entry* pt_entry = &pt->entries[virt.page_table_index];
    if (!pt_entry->present) {
        printf("Invalid virtual address %p, cannot find PT entry at index %d\n", addr, virt.page_table_index);
pt_print:
        printf("PT: %d %d %d %p\n",
                pt_entry->writable, pt_entry->global, pt_entry->accessed,
                from_pt_addr(pt_entry->phys_addr));
        goto pdt_print;
    }
    if (!pt_entry->phys_addr) {
        printf("PT at %p has phys addr of 0!\n", pt_entry);
        goto pt_print;
    }

    return (uintptr_t) from_pt_addr(pt_entry->phys_addr) + virt.page_offset;
}

void* arch_memory_alloc_base() {
    return find_closest_available_page_to(last_page + PAGE_SIZE);
}

void arch_memory_freeze_setup_paging() {
    paging_alloc_is_frozen = true;
}

static page_mapping paging_get_mapping(void* virt_addr) {
    paging_virtual_address virt_addr_s = AS_VIRT_ADDR(virt_addr);
    page_mapping mapping = {};
    mapping.pml4 = paging_get_current_pml4();

    mapping.pml4_entry = &mapping.pml4->entries[virt_addr_s.pml4_index];
    if (!mapping.pml4_entry->present) {
        return mapping;
    }
    mapping.pdpt = from_pt_addr(mapping.pml4_entry->phys_addr);

    mapping.pdpt_entry = &mapping.pdpt->entries[virt_addr_s.pdpt_index];
    if (!mapping.pdpt_entry->present) {
        return mapping;
    }
    if (mapping.pdpt_entry->huge_page) {
        mapping.addr_entry = mapping.pdpt_entry;
        mapping.phys_addr = (void*) ((uintptr_t) from_pt_addr(mapping.pdpt_entry->phys_addr) | ((uintptr_t) virt_addr & 0xfffffffff));
        mapping.page_size = LV3_PAGE_SIZE;
        return mapping;
    }
    mapping.pdt = from_pt_addr(mapping.pdpt_entry->phys_addr);

    mapping.pdt_entry = &mapping.pdt->entries[virt_addr_s.page_directory_index];
    if (!mapping.pdt_entry->present) {
        return mapping;
    }
    if (mapping.pdt_entry->huge_page) {
        mapping.addr_entry = mapping.pdt_entry;
        mapping.phys_addr = (void*) ((uintptr_t) from_pt_addr(mapping.pdt_entry->phys_addr) | ((uintptr_t) virt_addr & 0xffffff));
        mapping.page_size = LV2_PAGE_SIZE;
        return mapping;
    }
    mapping.pt = from_pt_addr(mapping.pdt_entry->phys_addr);

    mapping.pt_entry = &mapping.pt->entries[virt_addr_s.page_table_index];
    if (!mapping.pt_entry->present) {
        return mapping;
    }
    mapping.addr_entry = mapping.pt_entry;
    mapping.phys_addr = from_pt_addr(mapping.pt_entry->phys_addr) + virt_addr_s.page_offset;
    mapping.page_size = LV1_PAGE_SIZE;

    return mapping;
}

void paging_unmap_page(void* virt_addr) {
    if (virt_addr && (uintptr_t) virt_addr <= last_page) {
        // Skip already allocated pages
        return;
    }
    page_mapping mapping = paging_get_mapping(virt_addr);
    if (!mapping.addr_entry) {
        printf("Warning: virt addr %p not mapped, skipping page unmap\n", virt_addr);
        return;
    }
    for (int i = 0; i < SKIP_RANGES_LEN; i++) {
        if ((uintptr_t) mapping.phys_addr >= skip_ranges[i].start && (uintptr_t) mapping.phys_addr < skip_ranges[i].exclusive_end) {
            return;
        }
    }
    memzero(mapping.addr_entry, sizeof(page_table_entry));
}

void paging_unmap_addr_range(void* virt_start, void* virt_exclusive_end) {
    if ((uintptr_t) virt_start % PAGE_SIZE != 0) {
        printf("page map addr range: start addr is not page-aligned (%d)\n", PAGE_SIZE);
        boot_arch_full_halt();
    }
    if ((uintptr_t) virt_exclusive_end % PAGE_SIZE != 0) {
        printf("page map addr range: exclusive end addr is not page-aligned (%d)\n", PAGE_SIZE);
        boot_arch_full_halt();
    }

    spinlock_acquire(&page_map_lock);
    for (void* addr = virt_start; addr < virt_exclusive_end;) {
        page_mapping mapping = paging_get_mapping(addr);
        if (addr && (uintptr_t) addr <= last_page) {
            // Skip already allocated pages
            addr += mapping.page_size;
            continue;
        }
        if (!mapping.addr_entry) {
            printf("Warning: virt addr %p not mapped, skipping range unmap (start: %p)\n",
                    addr, virt_start);
            addr += PAGE_SIZE;
            continue;
        }
        bool is_in_skips = false;
        for (int i = 0; i < SKIP_RANGES_LEN; i++) {
            if ((uintptr_t) mapping.phys_addr >= skip_ranges[i].start &&
                (uintptr_t) mapping.phys_addr < skip_ranges[i].exclusive_end) {
                is_in_skips = true;
                break;
            }
        }
        addr += mapping.page_size;
        if (is_in_skips) {
            // This address is in skips and is special – do not unmap it.
            // If you want it unmapped, remove it from the skips.
            continue;
        }
        memzero(mapping.addr_entry, sizeof(page_table_entry));
    }

    paging_reload_page_table();

    spinlock_release(&page_map_lock);
}

void paging_unmap_range(void* virt_start, size_t len) {
    paging_unmap_addr_range(virt_start, virt_start + len);
}

void paging_set_page_allocation_border(void* border) {
    last_page = (uintptr_t) border;
}
