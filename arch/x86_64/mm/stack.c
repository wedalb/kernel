// SPDX-License-Identifier: GPL-3.0-or-later
#include <stddef.h>

void* stack_pointer() {
    void* stackptr = NULL;
    __asm__ volatile ("mov %%rsp, %0" : "=r"(stackptr));
    return stackptr;
}

void stack_set_pointer(void* addr) {
    __asm__ volatile ("mov %0, %%rsp" : : "r"(addr));
}
