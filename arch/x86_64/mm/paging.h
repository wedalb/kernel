// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#include "kernel/mm/alloc.h"
#include "kernel/mm/paging.h"

// https://os.phil-opp.com/paging-introduction/#page-table-format

#define PAGING_MAPPING_TABLE_ENTRY_COUNT 512
#define PAGE_SIZE 4096
#define PAGE_ADDR_SHIFT 12

typedef struct {
    bool present : 1;
    bool writable : 1;
    bool user_accessible : 1;
    bool write_through_caching : 1;
    bool disable_cache : 1;
    bool accessed : 1;
    bool dirty : 1;
    bool huge_page : 1;
    bool global : 1;
    uint8_t : 3;
    uintptr_t phys_addr : 40;
    uint16_t : 11;
    bool no_execute : 1;
}
__attribute__((packed))
page_table_entry;

typedef struct {
    page_table_entry entries[PAGING_MAPPING_TABLE_ENTRY_COUNT];
}
__attribute__((packed))
page_mapping_table;

typedef struct {
    /**
     * Contains the offset inside the page that we have retrieved.
     * This is the final part of the virtual address.
     *
     * physical_addr = page.phys_addr + page_offset
     */
    uint16_t page_offset : 12;
    /**
     * Page Table Index
     * Contains the index to the page that we're looking for
     *
     * page = (*page_table)[page_table_index]
     */
    uint16_t page_table_index : 9;
    /**
     * Page Directory Index
     * Contains the index to the page table
     *
     * page_table = (*page_directory)[page_directory_index]
     */
    uint16_t page_directory_index : 9;
    /**
     * Page Directory Pointer Table index
     * Contains the index to the pointer pointing to
     * a page directory
     *
     * page_directory = (*pdpt)[pdpt_index]
     */
    uint16_t pdpt_index : 9;
    /**
     * Page Mapping Table Level 4 index
     * Contains the index to the pdpt entry
     *
     * pdpt = pml4[pml4_index]
     */
    uint16_t pml4_index : 9;

    // Unused bits. We could extend this to support 5-level paging
    // but at this point we shouldn't even begin doing that (also, older
    // CPUs don't support it)
    uint16_t sign_extension : 16;
}
__attribute__((packed))
paging_virtual_address;

typedef struct {
    page_mapping_table* pml4;
    page_table_entry* pml4_entry;
    page_mapping_table* pdpt;
    page_table_entry* pdpt_entry;
    page_mapping_table* pdt;
    page_table_entry* pdt_entry;
    page_mapping_table* pt;
    page_table_entry* pt_entry;
    page_table_entry* addr_entry;
    void* phys_addr;
    size_t page_size;
} page_mapping;

__attribute__((aligned(PAGE_SIZE)))
extern page_mapping_table pml4;

void paging_load_page_table(page_mapping_table* pml4);
page_mapping_table* paging_get_current_pml4();

void* basic_alloc_at(void* addr_hint, size_t size);

extern bool paging_alloc_is_frozen;
