// SPDX-License-Identifier: GPL-3.0-or-later

#include <lib/tinystd/stddef.h>
#include <lib/tinystd/printf.h>

#include "e820.h"

size_t e820_get_entry_count() {
    uint32_t cont_id = 0, signature = 0, bytes = 0;
    size_t entries = 0;

    do {
        e820_entry entry = {};
        __asm__ volatile (
            "int $0x15"
            : "=a"(signature), "=c"(bytes), "=b"(cont_id)
            : "a"(0xe820), "b"(cont_id), "c"(sizeof(entry)), "d"(0x534d4150), "D"(&entry)
        );

        if (signature != 0x534d4150) {
            printf("E820: Invalid signature: %x\n", signature);
            return 0;
        }

        if (bytes != sizeof(entry) && bytes != 20) {
            printf("E820: Invalid entry size: %d\n", bytes);
            return 0;
        } else if (!entry.acpi_30_extended_attrs.should_ignore) {
            entries++;
        }
    } while (cont_id != 0);

    printf("E820: Found %d entries\n", entries);

    return entries;
}
