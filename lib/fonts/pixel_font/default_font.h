// SPDX-License-Identifier: GPL-3.0-or-later
#include "font.h"

extern pixel_font pixel_font_default;

#define GLYPH(c, w, px...) PIXEL_FONT_GLYPH(default_font, c, w, PIXEL_FONT_ALIGN_CENTER, px)
#define GLYPH_LEFT(c, w, px...) PIXEL_FONT_GLYPH(default_font, c, w, PIXEL_FONT_ALIGN_LEFT, px)
#define GLYPH_RIGHT(c, w, px...) PIXEL_FONT_GLYPH(default_font, c, w, PIXEL_FONT_ALIGN_RIGHT, px)
#define GLYPH_LOWER(c, w, px...) PIXEL_FONT_GLYPH(default_font, c, w, PIXEL_FONT_ALIGN_CENTER, EMPTY_ROW_##w EMPTY_ROW_##w EMPTY_ROW_##w EMPTY_ROW_##w px)
#define GLYPH_LOWER_LEFT(c, w, px...) PIXEL_FONT_GLYPH(default_font, c, w, PIXEL_FONT_ALIGN_LEFT, EMPTY_ROW_##w EMPTY_ROW_##w EMPTY_ROW_##w EMPTY_ROW_##w px)
#define GLYPH_LOWER_RIGHT(c, w, px...) PIXEL_FONT_GLYPH(default_font, c, w, PIXEL_FONT_ALIGN_RIGHT, EMPTY_ROW_##w EMPTY_ROW_##w EMPTY_ROW_##w EMPTY_ROW_##w px)
