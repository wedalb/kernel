// SPDX-License-Identifier: GPL-3.0-or-later
#include "lib/tinystd/mm.h"

#define PIXEL_FONT_NAME default_font

#include "font.h"
#include "default_font.h"
#include "font_helpers.h"


// Glyphs
GLYPH(empty, 0)
GLYPH(space, 1, ZERO_COLUMN)
GLYPH(exclamation_mark, 1,
    O       // 1
    O       // 2
    O       // 3
    O       // 4
    O       // 5
    O       // 6
    O       // 7
    O       // 8
    _       // 9
    O       // 10
    O       // 11
)
GLYPH(double_quotes, 4,
    O _ _ O
    O _ _ O
    O _ _ O
    O _ _ O
)
GLYPH(pound, 8,
    _ _ O _ _ _ _ O // 1
    _ _ O _ _ _ _ O // 2
    _ _ O _ _ _ _ O // 3
    _ _ O _ _ _ _ O // 4
    O O O O O O O O // 5
    _ O _ _ _ _ O _ // 6
    _ O _ _ _ _ O _ // 7
    _ O _ _ _ _ O _ // 8
    O O O O O O O O // 9
    O _ _ _ _ O _ _ // 10
    O _ _ _ _ O _ _ // 11
    O _ _ _ _ O _ _ // 12
)
GLYPH(dollar, 7,
    _ _ _ O _ _ _   // 1
    _ _ O O O _ _   // 2
    _ O _ _ _ O _   // 3
    _ O _ _ _ _ _   // 4
    _ _ O O _ _ _   // 5
    _ _ o O o _ _   // 6
    _ _ _ O O _ _   // 7
    _ _ _ _ _ O _   // 8
    _ O _ _ _ O _   // 9
    _ _ O O O _ _   // 10
    _ _ _ O _ _ _   // 11
)
GLYPH(percent, 7,
    _ _ _ _ _ _ _
    _ O O _ _ _ _
    O _ _ O _ _ O
    O _ _ O _ O _
    _ O O _ O _ _
    _ _ _ O _ _ _
    _ _ O _ O O _
    _ O _ O _ _ O
    O _ _ O _ _ O
    _ _ _ _ O O _
    _ _ _ _ _ _ _
)
GLYPH(ampersand, 8, // &
    _ _ O O O _ _ _
    _ O _ _ _ O _ _
    _ O _ _ _ O _ _
    _ O _ _ O _ _ _
    _ _ O O _ _ _ _
    _ O O O _ _ _ O
    _ O _ _ O _ O _
    O _ _ _ _ O _ _
    O _ _ _ O _ O _
    O _ _ O _ _ _ O
    _ O O _ _ _ _ _
)
GLYPH(apostrophe, 1,
    O
    O
    O
    O
)
GLYPH(opening_paran, 5,
    _ _ O _ _
    _ _ O _ _
    _ O _ _ _
    _ O _ _ _
    O _ _ _ _
    O _ _ _ _
    O _ _ _ _
    _ O _ _ _
    _ O _ _ _
    _ _ O _ _
    _ _ O _ _
)
GLYPH(closing_paran, 5,
     _ _ O _ _
     _ _ O _ _
     _ _ _ O _
     _ _ _ O _
     _ _ _ _ O
     _ _ _ _ O
     _ _ _ _ O
     _ _ _ O _
     _ _ _ O _
     _ _ O _ _
     _ _ O _ _
)
GLYPH(asterisk, 5,
    _ _ _ _ _
    _ _ O _ _
    _ _ O _ _
    O O O O O
    _ O _ O _
    O _ _ _ O
)
GLYPH(plus, 5,
    _ _ _ _ _
    _ _ _ _ _
    _ _ _ _ _
    _ _ O _ _
    _ _ O _ _
    O O O O O
    _ _ O _ _
    _ _ O _ _
)
GLYPH_LOWER(comma, 2,
    _ _     // 5
    _ _     // 6
    _ _     // 7
    _ _     // 8
    _ _     // 9
    O O     // 10
    O O     // 11
    _ O
    O _
)
GLYPH_LOWER(minus, 5,
    _ _ _ _ _
    _ _ _ _ _
    O O O O O
)
GLYPH_LOWER(dot, 2,
    _ _     // 5
    _ _     // 6
    _ _     // 7
    _ _     // 8
    _ _     // 9
    O O     // 10
    O O     // 11
)
GLYPH(slash, 5,
    _ _ _ _ u
    _ _ _ _ O
    _ _ _ _ O
    _ _ _ O _
    _ _ _ O _
    _ _ O _ _
    _ _ O _ _
    _ O _ _ _
    _ O _ _ _
    O _ _ _ _
    O _ _ _ _
)

GLYPH(0, 7,
    _ _ O O O _ _
    _ O _ _ _ O _
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ O _ _ O
    O _ _ O _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    _ O _ _ _ O _
    _ _ O O O _ _
)
GLYPH(1, 7,
    _ O O O _ _ _   // 1
    O u _ O _ _ _   // 2
    _ _ _ O _ _ _   // 3
    _ _ _ O _ _ _   // 4
    _ _ _ O _ _ _   // 5
    _ _ _ O _ _ _   // 6
    _ _ _ O _ _ _   // 7
    _ _ _ O _ _ _   // 8
    _ _ _ O _ _ _   // 9
    _ _ _ O _ _ _   // 10
    O O O O O O O   // 11
)
GLYPH(2, 7,
    _ O O O O O _   // 1
    O u _ _ _ u O   // 2
    O _ _ _ _ _ O   // 3
    _ _ _ _ _ _ O   // 4
    _ _ _ _ _ O _   // 5
    _ _ _ O O _ _   // 6
    _ _ O _ _ _ _   // 7
    _ O _ _ _ _ _   // 8
    O _ _ _ _ _ _   // 9
    O u _ _ _ _ _   // 10
    O O O O O O O   // 11
)
GLYPH(3, 7,
    _ O O O O O _   // 1
    O o _ _ _ u O   // 2
    O _ _ _ _ _ O   // 3
    O _ _ _ _ _ O   // 4
    _ _ _ _ _ u O   // 5
    _ _ _ O O O _   // 6
    _ _ _ _ _ u O   // 7
    O _ _ _ _ _ O   // 8
    O _ _ _ _ _ O   // 9
    O o _ _ _ u O   // 10
    _ O O O O O _   // 11
)
GLYPH(4, 7,
    _ _ _ _ _ O _   // 1
    _ _ _ _ O O _
    _ _ _ O _ O _
    _ _ O _ _ O _
    _ O _ _ _ O _
    O u _ _ _ O _
    O O O O O O O
    _ _ _ _ _ O _
    _ _ _ _ _ O _
    _ _ _ _ _ O _
    _ _ _ _ _ O _   // 11
)
GLYPH(5, 7,
    O O O O O O O   // 1
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ O O O O _
    O O _ _ _ _ O
    O _ _ _ _ _ O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    O _ _ _ _ _ O
    _ O O O O O _   // 11
)
GLYPH(6, 7,
    _ _ o O O O _   // 1
    _ o O _ _ _ _
    o O _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O O O O O O _
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    _ O O O O O _   // 11
)
GLYPH(7, 7,
    O O O O O O O
    _ _ _ _ · O o
    _ _ _ _ u O _
    _ _ _ · O u _
    _ _ _ u O · _
    _ _ _ o O _ _
    _ _ _ O u _ _
    _ _ _ O · _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
)
GLYPH(8, 7,
    _ O O O O O _
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    _ O O O O O _
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    _ O O O O O _
)
GLYPH(9, 7,
    _ O O O O O _
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    _ O O O O O O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    O _ _ _ _ _ O
    O · _ _ _ · O
    _ O O O O O _
)

GLYPH_LOWER(colon, 2,
    O O
    O O
    _ _
    _ _
    _ _
    O O
    O O
)
GLYPH_LOWER(semicolon, 2,
    O O
    O O
    _ _
    _ _
    _ _
    O O
    O O
    _ O
    O
)
GLYPH_LOWER(lt, 7,
    _ _ _ _ o O O
    _ _ o O O _ _
    _ O O _ _ _ _
    O o _ _ _ _ _
    _ O O _ _ _ _
    _ _ o O O _ _
    _ _ _ _ o O O
)
GLYPH_LOWER(equals, 7,
    _ _ _ _ _ _ _
    _ _ _ _ _ _ _
    O O O O O O O
    _ _ _ _ _ _ _
    O O O O O O O
)
GLYPH_LOWER(gt, 7,
    O O o _ _ _ _
    _ _ O O o _ _
    _ _ _ _ O O _
    _ _ _ _ _ o O
    _ _ _ _ O O _
    _ _ O O o _ _
    O O o _ _ _ _
)
GLYPH(question_mark, 7,
    _ _ O O O _ _
    _ O _ _ _ O _
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    _ _ _ _ _ O _
    _ _ _ _ O _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ _ _ _ _
    _ _ _ O _ _ _
)
GLYPH(at, 7,
    _ O O O O O _
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ u O
    O _ _ _ O O O
    O _ _ O _ _ O
    O _ _ O _ _ O
    O _ _ _ O O O
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    _ O O O O O O
)

GLYPH(A, 7,
    _ _ _ O _ _ _
    _ _ O _ O _ _
    _ O _ _ _ O _
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O O O O O O O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
)
GLYPH(B, 7,
    O O O O O O _
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O O O O O O _
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O O O O O O _
)
GLYPH(C, 7,
    _ O O O O O O
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    _ O O O O O O
)
GLYPH(D, 7,
    O O O O O · _
    O _ _ _ _ O _
    O _ _ _ _ · O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ · O
    O _ _ _ _ O _
    O O O O O · _
)
GLYPH(E, 7,
    O O O O O O O
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O O O O O O O
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O O O O O O O
)
GLYPH(F, 7,
    O O O O O O O
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O O O O O O O
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
)
GLYPH(G, 7,
    _ O O O O O O
    O · _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ O O O O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O · _ _ _ · O
    _ O O O O O _

)
GLYPH(H, 7,
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O O O O O O O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
)
GLYPH(I, 7,
    O O O O O O O
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    O O O O O O O
)
GLYPH(J, 7,
    O O O O O O O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    _ _ _ _ _ O _
    O O O O O _ _
)
GLYPH(K, 7,
    O _ _ _ _ _ O
    O _ _ _ _ O _
    O _ _ _ O _ _
    O _ _ O _ _ _
    O _ O _ _ _ _
    O O _ _ _ _ _
    O _ O _ _ _ _
    O _ _ O _ _ _
    O _ _ _ O _ _
    O _ _ _ _ O _
    O _ _ _ _ _ O
)
GLYPH(L, 7,
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O O O O O O O
)
GLYPH(M, 7,
    O _ _ _ _ _ O
    O O _ _ _ O O
    O O _ _ _ O O
    O _ O _ O _ O
    O _ O _ O _ O
    O _ _ O _ _ O
    O _ _ O _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
)
GLYPH(N, 7,
    O _ _ _ _ _ O
    O O _ _ _ _ O
    O O _ _ _ _ O
    O _ O _ _ _ O
    O _ O _ _ _ O
    O _ _ O _ _ O
    O _ _ O _ _ O
    O _ _ _ O _ O
    O _ _ _ O _ O
    O _ _ _ _ O O
    O _ _ _ _ O O
)
GLYPH(O_, 8,
    _ O O O O O O _
    O · _ _ _ _ · O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O · _ _ _ _ · O
    _ O O O O O O _
)
GLYPH(P, 8,
    O O O O O O O _
    O _ _ _ _ _ · O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ · O
    O O O O O O O _
    O _ _ _ _ _ _ _
    O _ _ _ _ _ _ _
    O _ _ _ _ _ _ _
    O _ _ _ _ _ _ _
    O _ _ _ _ _ _ _
)
GLYPH(Q, 8,
    _ O O O O O O _
    O · _ _ _ _ · O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ O _ O
    O _ _ _ _ _ O _
    _ O O O O O _ O
)
GLYPH(R, 8,
    O O O O O O O _
    O _ _ _ _ _ · O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ · O
    O O O O O O O _
    O _ · O _ _ _ _
    O _ _ _ O _ _ _
    O _ _ _ _ O _ _
    O _ _ _ _ _ O _
    O _ _ _ _ _ _ O
)
GLYPH(S, 8,
    _ O O O O O O ·
    O u _ _ _ · u O
    O _ _ _ _ _ · O
    O _ _ _ _ _ _ _
    O u · _ _ _ _ _
    _ O O O O O O _
    _ _ _ _ _ · u O
    _ _ _ _ _ _ _ O
    O · _ _ _ _ _ O
    O u · _ _ _ u O
    · O O O O O O _
)
GLYPH(T, 7,
    O O O O O O O // 1
    _ _ _ O _ _ _ // 2
    _ _ _ O _ _ _ // 3
    _ _ _ O _ _ _ // 4
    _ _ _ O _ _ _ // 5
    _ _ _ O _ _ _ // 6
    _ _ _ O _ _ _ // 7
    _ _ _ O _ _ _ // 8
    _ _ _ O _ _ _ // 9
    _ _ _ O _ _ _ // 10
    _ _ _ O _ _ _ // 11
)
GLYPH(U, 8,
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O · _ _ _ _ · O
    _ O O O O O O _
)
GLYPH(V, 8,
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    _ O _ _ _ _ O _
    _ O _ _ _ _ O _
    _ O _ _ _ _ O _
    _ _ O _ _ O _ _
    _ _ O _ _ O _ _
    _ _ O _ _ O _ _
    _ _ _ O O _ _ _
    _ _ _ O O _ _ _
)
GLYPH(W, 8,
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    _ O _ O O _ O _
    _ O _ O O _ O _
    _ O _ O O _ O _
    _ O _ O O _ O _
    _ _ O _ _ O _ _
    _ _ O _ _ O _ _
    _ _ O _ _ O _ _
)
GLYPH(X, 7,
    O _ _ _ _ _ O
    o O _ _ _ O o
    _ O _ _ _ O _
    _ o O _ O o _
    _ _ O _ O _ _
    _ _ _ O _ _ _
    _ _ O _ O _ _
    _ o O _ O o _
    _ O _ _ _ O _
    o O _ _ _ O o
    O _ _ _ _ _ O
)
GLYPH(Y, 7,
    O _ _ _ _ _ O
    o O _ _ _ O o
    _ O _ _ _ O _
    _ o O _ O o _
    _ _ O _ O _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
    _ _ _ O _ _ _
)
GLYPH(Z, 8,
    O O O O O O O O
    _ _ _ _ _ _ O _
    _ _ _ _ _ O _ _
    _ _ _ _ O _ _ _
    _ _ _ _ O _ _ _
    _ _ _ O _ _ _ _
    _ _ _ O _ _ _ _
    _ _ O _ _ _ _ _
    _ O _ _ _ _ _ _
    O _ _ _ _ _ _ _
    O O O O O O O O
)
GLYPH(opening_bracket, 5,
    O O O O O
    O _ _ _ _
    O _ _ _ _
    O _ _ _ _
    O _ _ _ _
    O _ _ _ _
    O _ _ _ _
    O _ _ _ _
    O _ _ _ _
    O _ _ _ _
    O O O O O
)
GLYPH(backslash, 5,
    u _ _ _ _
    O _ _ _ _
    O _ _ _ _
    _ O _ _ _
    _ O _ _ _
    _ _ O _ _
    _ _ O _ _
    _ _ _ O _
    _ _ _ O _
    _ _ _ _ O
    _ _ _ _ O
)
GLYPH(closing_bracket, 5,
    O O O O O
    _ _ _ _ O
    _ _ _ _ O
    _ _ _ _ O
    _ _ _ _ O
    _ _ _ _ O
    _ _ _ _ O
    _ _ _ _ O
    _ _ _ _ O
    _ _ _ _ O
    O O O O O
)
GLYPH(caret, 5,
    _ _ O _ _
    _ O _ O _
    O _ _ _ O
)

GLYPH_LOWER(underscore, 8,
    EMPTY_ROW_8
    EMPTY_ROW_8
    EMPTY_ROW_8
    EMPTY_ROW_8
    EMPTY_ROW_8
    EMPTY_ROW_8
    O O O O O O O O
)
GLYPH(backtick, 5,
    _ _ O _ _
    _ _ _ O _
    _ _ _ _ O
)

GLYPH_LOWER(a, 6,
    _ O O O O _
    O · _ _ · O
    _ _ _ _ u O
    _ O O O O O
    O _ _ _ _ O
    O _ _ _ o O
    u O O O u O
)
GLYPH(b, 7,
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O u O O O _ _
    O O _ _ _ O _
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O O _ _ _ O _
    O u O O O _ _
)
GLYPH_LOWER(c, 7,
    _ O O O O O _
    O u _ _ _ u O
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O u _ _ _ u O
    _ O O O O O _
)
GLYPH(d, 7,
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    _ _ O O O u O
    _ O _ _ _ O O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    _ O _ _ _ O O
    _ _ O O O u O
)
GLYPH_LOWER(e, 8,
    _ _ O O O O _ _   // 5
    _ O · _ _ · O _   // 6
    _ O _ _ _ _ O _   // 7
    _ O O O O O O _   // 8
    _ O _ _ _ _ _ _   // 9
    _ O · _ _ · O _   // 10
    _ _ O O O O _ _   // 11
)
GLYPH(f, 7,
    _ · O O O O ·
    _ O _ _ _ _ O
    _ O _ _ _ _ _
    _ O _ _ _ _ _
    O O O O O _ _
    _ O _ _ _ _ _
    _ O _ _ _ _ _
    _ O _ _ _ _ _
    _ O _ _ _ _ _
    _ O _ _ _ _ _
    _ O _ _ _ _ _
)
GLYPH_LOWER(g, 7,
    _ O O O O O ·
    O _ _ · O · _
    O _ _ _ O · _
    O _ _ _ O _ _
    O O O O _ _ _
    _ O _ _ _ _ _
    O O O O O O _
    O _ _ _ _ _ O
    O · _ _ _ · O
    _ O O O O O _
)
GLYPH(h, 7,
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O u O O O · _
    O O _ _ _ O ·
    O · _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
)
GLYPH(i, 5,
    _ _ _ _ _   // 1
    _ _ _ O O   // 2
    _ _ _ O O   // 3
    _ _ _ _ _   // 4
    O O O O O   // 5
    _ _ _ _ O   // 6
    _ _ _ _ O   // 7
    _ _ _ _ O   // 8
    _ _ _ _ O   // 9
    _ _ _ _ O   // 10
    _ _ _ _ O   // 11
)
GLYPH(j, 5,
    _ _ _ _ _   // 1
    _ _ _ O O   // 2
    _ _ _ O O   // 3
    _ _ _ _ _   // 4
    O O O O O   // 5
    _ _ _ _ O   // 6
    _ _ _ _ O   // 7
    _ _ _ _ O   // 8
    _ _ _ _ O   // 9
    _ _ _ _ O   // 10
    _ _ _ _ O   // 11
    _ _ _ _ O   // 12
    _ _ _ O _   // 13
    _ O O _ _   // 14
)
GLYPH(k, 6,
    O _ _ _ _ _
    O _ _ _ _ _
    O _ _ _ _ _
    O _ _ _ _ _
    O _ _ _ _ O
    O _ _ _ O _
    O _ _ O _ _
    O · O _ _ _
    O O _ O _ _
    O _ _ _ O _
    O _ _ _ _ O
)
GLYPH(l, 6,
    O O O _ _ _
    _ _ O _ _ _
    _ _ O _ _ _
    _ _ O _ _ _
    _ _ O _ _ _
    _ _ O _ _ _
    _ _ O _ _ _
    _ _ O _ _ _
    _ _ O _ _ _
    _ _ O · _ _
    _ _ · O O O
)
GLYPH_LOWER(m, 8,
    O · O O _ O O ·
    O O _ _ O _ _ O
    O _ _ _ O _ _ O
    O _ _ _ O _ _ O
    O _ _ _ O _ _ O
    O _ _ _ O _ _ O
    O _ _ _ O _ _ O
)
GLYPH_LOWER(n, 6,
    O · O O O ·
    O O _ _ _ O
    O _ _ _ _ O
    O _ _ _ _ O
    O _ _ _ _ O
    O _ _ _ _ O
    O _ _ _ _ O
)
GLYPH_LOWER(o_, 7,
    _ · O O O · _ // 5
    u O u _ u O u // 6
    O _ _ _ _ _ O // 7
    O _ _ _ _ _ O // 8
    O _ _ _ _ _ O // 9
    u O u _ u O u // 10
    _ · O O O · _ // 11
)
GLYPH_LOWER(p, 7,
    O · O O O _ _
    O O _ _ _ O _
    O · _ _ _ _ O
    O _ _ _ _ _ O
    O · _ _ _ _ O
    O O _ _ _ O _
    O _ O O O _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
    O _ _ _ _ _ _
)
GLYPH_LOWER(q, 7,
    _ _ O O O · O
    _ O _ _ _ O O
    O _ _ _ _ · O
    O _ _ _ _ _ O
    O _ _ _ _ · O
    _ O _ _ _ O O
    _ _ O O O _ O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
    _ _ _ _ _ _ O
)
GLYPH_LOWER(r, 6,
    O · O O O ·
    O O _ _ _ O
    O _ _ _ _ _
    O _ _ _ _ _
    O _ _ _ _ _
    O _ _ _ _ _
    O _ _ _ _ _
)
GLYPH_LOWER(s, 7,
    · O O O O O ·
    O _ _ _ _ _ O
    O _ _ _ _ _ _
    · O O O O O ·
    _ _ _ _ _ _ O
    O _ _ _ _ _ O
    · O O O O O ·
)
GLYPH(t_, 7,
    _ _ O _ _ _ _
    _ _ O _ _ _ _
    _ _ O _ _ _ _
    _ _ O _ _ _ _
    O O O O O O _
    _ _ O _ _ _ _
    _ _ O _ _ _ _
    _ _ O _ _ _ _
    _ _ O _ _ _ _
    _ _ O · _ _ _
    _ _ · O O O _
)
GLYPH_LOWER(u_, 7,
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O _ _ _ _ _ O
    O · _ _ _ O O
    _ O O O O · O
)
GLYPH_LOWER(v, 7,
    O _ _ _ _ _ O
    O · _ _ _ · O
    u O _ _ _ O u
    _ O · _ · O _
    _ u O _ O u _
    _ _ O · O _ _
    _ _ _ O _ _ _
)
GLYPH_LOWER(w, 8,
    O _ _ _ _ _ _ O
    O _ _ _ _ _ _ O
    O _ _ O O _ _ O
    u O _ O O _ O u
    _ O _ O O _ O _
    _ O _ O O _ O _
    _ · O · · O · _
)
GLYPH_LOWER(x, 7,
    O _ _ _ _ _ O
    _ O _ _ _ O _
    _ _ O _ O _ _
    _ _ _ O _ _ _
    _ _ O _ O _ _
    _ O _ _ _ O _
    O _ _ _ _ _ O
)
GLYPH_LOWER(y, 7,
    O _ _ _ _ _ O
    O · _ _ _ u O
    u O _ _ _ O ·
    _ O · _ u O _
    _ u O _ O · _
    _ · O · O _ _
    _ _ _ O _ _ _
    _ u O u _ _ _
    O O u _ _ _ _
    o · _ _ _ _ _
    · _ _ _ _ _ _
)
GLYPH_LOWER(z, 7,
    O O O O O O O
    _ _ _ _ _ O _
    _ _ _ _ O _ _
    _ _ _ O _ _ _
    _ _ O _ _ _ _
    _ O _ _ _ _ _
    O O O O O O O
)

GLYPH(opening_brace, 5,
    _ _ O O O
    _ O _ _ _
    _ O _ _ _
    _ O _ _ _
    _ O _ _ _
    O · _ _ _
    _ O _ _ _
    _ O _ _ _
    _ O _ _ _
    _ O _ _ _
    _ _ O O O
)
GLYPH(pipe, 1,
    O
    O
    O
    O
    O
    O
    O
    O
    O
    O
    O
    O
    O
    O
    O
)
GLYPH(closing_brace, 6,
    O O O _ _
    _ _ _ O _
    _ _ _ O _
    _ _ _ O _
    _ _ _ O _
    _ _ _ · O
    _ _ _ O _
    _ _ _ O _
    _ _ _ O _
    _ _ _ O _
    O O O _ _
)
GLYPH(tilde, 5,
    EMPTY_ROW_5
    EMPTY_ROW_5
    EMPTY_ROW_5
    EMPTY_ROW_5
    _ O O _ _ O
    O _ _ O O _
)

// Font

void pixel_font_default_init() {
    pixel_font_glyph glyphs[PIXEL_FONT_CHARACTER_COUNT] = {
        default_font_empty,             // NUL  0
        default_font_empty,             // SOH  1
        default_font_empty,             // STX  2
        default_font_empty,             // ETX  3
        default_font_empty,             // EOT  4

        default_font_empty,             // ENQ  5
        default_font_empty,             // ACK  6
        default_font_empty,             // BEL  7
        default_font_empty,             // BS   8
        default_font_empty,             // TAB  9

        default_font_empty,             // LF   10
        default_font_empty,             // VT   11
        default_font_empty,             // FF   12
        default_font_empty,             // CR   13
        default_font_empty,             // SO   14

        default_font_empty,             // SI   15
        default_font_empty,             // DLE  16
        default_font_empty,             // DC1  17
        default_font_empty,             // DC2  18
        default_font_empty,             // DC3  19

        default_font_empty,             // DC4  20
        default_font_empty,             // NAK  21
        default_font_empty,             // SYN  22
        default_font_empty,             // ETB  23
        default_font_empty,             // CAN  24

        default_font_empty,             // EM   25
        default_font_empty,             // SUB  26
        default_font_empty,             // ESC  27
        default_font_empty,             // FS   28
        default_font_empty,             // GS   29

        default_font_empty,             // RS   30
        default_font_empty,             // US   31
        default_font_space,             //      32
        default_font_exclamation_mark,  // !    33
        default_font_double_quotes,     // "    34

        default_font_pound,             // #    35
        default_font_dollar,            // $    36
        default_font_percent,           // %    37
        default_font_ampersand,         // &    38
        default_font_apostrophe,        // '    39

        default_font_opening_paran,     // (    40
        default_font_closing_paran,     // )    41
        default_font_asterisk,          // *    42
        default_font_plus,              // +    43
        default_font_comma,             // ,    44

        default_font_minus,             // -    45
        default_font_dot,               // .    46
        default_font_slash,             // /    47
        default_font_0,                 // 0    48
        default_font_1,                 // 1    49

        default_font_2,                 // 2    50
        default_font_3,                 // 3    51
        default_font_4,                 // 4    52
        default_font_5,                 // 5    53
        default_font_6,                 // 6    54

        default_font_7,                 // 7    55
        default_font_8,                 // 8    56
        default_font_9,                 // 9    57
        default_font_colon,             // :    58
        default_font_semicolon,         // ;    59

        default_font_lt,                // <    60
        default_font_equals,            // =    61
        default_font_gt,                // >    62
        default_font_question_mark,     // ?    63
        default_font_at,                // @    64

        default_font_A,                 // A    65
        default_font_B,                 // B    66
        default_font_C,                 // C    67
        default_font_D,                 // D    68
        default_font_E,                 // E    69

        default_font_F,                 // F    70
        default_font_G,                 // G    71
        default_font_H,                 // H    72
        default_font_I,                 // I    73
        default_font_J,                 // J    74

        default_font_K,                 // K    75
        default_font_L,                 // L    76
        default_font_M,                 // M    77
        default_font_N,                 // N    78
        default_font_O_,                // O    79

        default_font_P,                 // P    80
        default_font_Q,                 // Q    81
        default_font_R,                 // R    82
        default_font_S,                 // S    83
        default_font_T,                 // T    84

        default_font_U,                 // U    85
        default_font_V,                 // V    86
        default_font_W,                 // W    87
        default_font_X,                 // X    88
        default_font_Y,                 // Y    89

        default_font_Z,                 // Z    90
        default_font_opening_bracket,   // [    91
        default_font_backslash,         // \    92
        default_font_closing_bracket,   // ]    93
        default_font_caret,             // ^    94

        default_font_underscore,        // _    95
        default_font_backtick,          // `    96
        default_font_a,                 // a    97
        default_font_b,                 // b    98
        default_font_c,                 // c    99

        default_font_d,                 // d    100
        default_font_e,                 // e    101
        default_font_f,                 // f    102
        default_font_g,                 // g    103
        default_font_h,                 // h    104

        default_font_i,                 // i    105
        default_font_j,                 // j    106
        default_font_k,                 // k    107
        default_font_l,                 // l    108
        default_font_m,                 // m    109

        default_font_n,                 // n    110
        default_font_o_,                // o    111
        default_font_p,                 // p    112
        default_font_q,                 // q    113
        default_font_r,                 // r    114

        default_font_s,                 // s    115
        default_font_t_,                // t    116
        default_font_u_,                // u    117
        default_font_v,                 // v    118
        default_font_w,                 // w    119

        default_font_x,                 // x    120
        default_font_y,                 // y    121
        default_font_z,                 // z    122
        default_font_opening_brace,     // {    123
        default_font_pipe,              // |    124

        default_font_closing_brace,     // }    125
        default_font_tilde,             // ~    126
        default_font_empty,             // DEL  127
    };

    memcpy(pixel_font_default.glyphs, glyphs, sizeof(pixel_font_glyph) * PIXEL_FONT_CHARACTER_COUNT);
}

pixel_font pixel_font_default = {
    .init = pixel_font_default_init,
};