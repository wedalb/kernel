// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stdint.h>
#include <stdbool.h>

#define PIXEL_FONT_GLYPH_WIDTH 8
#define PIXEL_FONT_GLYPH_HEIGHT 15
#define PIXEL_FONT_LETTER_SPACING 1
#define PIXEL_FONT_LINE_HEIGHT 15
#define PIXEL_FONT_LINE_VERTICAL_PADDING_EACH 1
#define PIXEL_FONT_COL_WIDTH (PIXEL_FONT_GLYPH_WIDTH + PIXEL_FONT_LETTER_SPACING)
#define PIXEL_FONT_ROW_HEIGHT PIXEL_FONT_LINE_HEIGHT + PIXEL_FONT_LINE_VERTICAL_PADDING_EACH * 2
#define PIXEL_FONT_SCALE 1
#define PIXEL_FONT_GLYPH_BYTES (PIXEL_FONT_GLYPH_WIDTH * PIXEL_FONT_GLYPH_HEIGHT)
// We'll stick to ASCII (for now)
#define PIXEL_FONT_CHARACTER_COUNT 128
#define PIXEL_FONT_CHARSET_BITS sizeof(uint8_t)

#define PIXEL_FONT_ALIGN_LEFT 0
#define PIXEL_FONT_ALIGN_CENTER 1
#define PIXEL_FONT_ALIGN_RIGHT 2

#define EMPTY_ROW_0
#define EMPTY_ROW_1		0x00,
#define EMPTY_ROW_2		0x00, 0x00,
#define EMPTY_ROW_3		0x00, 0x00, 0x00,
#define EMPTY_ROW_4		EMPTY_ROW_2 EMPTY_ROW_2
#define EMPTY_ROW_5		EMPTY_ROW_3 EMPTY_ROW_2
#define EMPTY_ROW_6		EMPTY_ROW_3 EMPTY_ROW_3
#define EMPTY_ROW_7		EMPTY_ROW_4 EMPTY_ROW_3
#define EMPTY_ROW_8		EMPTY_ROW_4 EMPTY_ROW_4
#define EMPTY_ROW_9		EMPTY_ROW_5 EMPTY_ROW_4

typedef struct {
	uint8_t glyph_pixels[PIXEL_FONT_GLYPH_BYTES];
	uint8_t width;
	uint8_t alignment;
} pixel_font_glyph;

typedef struct {
	pixel_font_glyph glyphs[PIXEL_FONT_CHARACTER_COUNT];
	bool is_initialized;
	void (*init)();
} pixel_font;

int pixel_font_get_glyph(pixel_font_glyph* glyph, pixel_font* font, unsigned char c);
