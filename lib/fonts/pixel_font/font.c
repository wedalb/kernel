// SPDX-License-Identifier: GPL-3.0-or-later
#include "lib/tinystd/sync/spinlock.h"

#include "font.h"

#define PIXEL_FONT_GLYPH_OUT_OF_RANGE   1
#define PIXEL_FONT_SUCCESS              0

static spinlock init_lock;

static inline void ensure_initialized(pixel_font* font) {
    spinlock_guard(init_lock,
        if (!font->is_initialized) {
            font->init();
        }
    )
}

int pixel_font_get_glyph(pixel_font_glyph* glyph, pixel_font* font, unsigned char c) {
    ensure_initialized(font);
    if (c >= PIXEL_FONT_CHARACTER_COUNT) {
        return PIXEL_FONT_GLYPH_OUT_OF_RANGE;
    }
    *glyph = font->glyphs[c];
    return PIXEL_FONT_SUCCESS;
}
