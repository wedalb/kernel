// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#define PIXEL_FONT_GLYPH(fname, c, w, a, px...) \
pixel_font_glyph fname##_##c = { \
    .glyph_pixels = { \
		px \
	}, \
	.width = w, \
    .alignment = a \
};

#define ZERO_COLUMN \
	0x00, 0x00, 0x00, 0x00, \
	0x00, 0x00, 0x00, 0x00, \
	0x00, 0x00, 0x00, 0x00, \
	0x00, 0x00, 0x00,

#define _ 0x00,
#define O 0xFF,
#define o 0xA0,
#define u 0x5F,
#define · 0x30,



