// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>

typedef __INTMAX_TYPE__ ssize_t;
