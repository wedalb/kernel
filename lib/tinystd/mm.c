// SPDX-License-Identifier: GPL-3.0-or-later
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "mm.h"

void memset(void* ptr, const char c, size_t len) {
    for (size_t i = 0; i < len; i++) {
        ((char*)ptr)[i] = c;
    }
}

void memmove(void* dest, const void* src, size_t len) {
    if (dest == src || len == 0) {
        return;
    }

    if (dest < src) {
        for (size_t i = 0; i < len; i++) {
            ((char*) dest)[i] = ((char*) src)[i];
        }
    } else if (dest > src) {
        for (size_t i = len - 1; i != -1; i--) {
            ((char*) dest)[i] = ((char*) src)[i];
        }
    }
}

void memcpy(void* dest, const void* src, size_t len) {
    if (dest == src || len == 0) {
        return;
    }

    for (size_t i = 0; i < len; i++) {
        ((char*) dest)[i] = ((char*) src)[i];
    }
}

bool memncmp(const void* a, const void* b, size_t len) {
    for (size_t i = 0; i < len; i++) {
        if (((char*)a)[i] != ((char*)b)[i]) {
            return false;
        }
    }
    return true;
}
