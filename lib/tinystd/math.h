// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stdint.h>

int math_abs(int n);
int64_t math_abs_64(int64_t n);
int32_t math_abs_32(int32_t n);
int16_t math_abs_16(int16_t n);
int8_t math_abs_8(int8_t n);

#define min(a, b) ((a) > (b) ? (b) : (a))
#define max(a, b) ((a) > (b) ? (a) : (b))

#define align(unaligned, border) \
    (((uintptr_t) (unaligned)) % (border) == 0 ? \
        ((uintptr_t) (unaligned)) : \
        ((uintptr_t) (unaligned)) + ((border) - (((uintptr_t) (unaligned)) % (border))))

#define align_floor(unaligned, border) \
       (((uintptr_t) (unaligned)) % (border) == 0 ? \
        ((uintptr_t) (unaligned)) : \
        ((uintptr_t) (unaligned)) - ((uintptr_t) (unaligned)) % (border))

