// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "lib/tinystd/stddef.h"

size_t strlen(const char* str);
size_t strchoccur(const char* str, const char c, size_t len);

void u64tos(char* result, uint64_t value);
size_t u64tosn(char* result, uint64_t value);
size_t u64tosn_len(uint64_t value);

void s64tos(char* result, int64_t value);
size_t s64tosn(char* result, int64_t value);
size_t s64tosn_len(int64_t value);

void u32tos(char* result, uint32_t value);
size_t u32tosn(char* result, uint32_t value);
size_t u32tosn_len(uint32_t value);

void s32tos(char* result, int32_t value);
size_t s32tosn(char* result, int32_t value);
size_t s32tosn_len(int32_t value);

void u64toshex(char* result, uint64_t value);
size_t u64toshexn(char* result, uint64_t value);
size_t u64toshexn_len(uint64_t value);

void u32toshex(char* result, uint32_t value);
size_t u32toshexn(char* result, uint32_t value);
size_t u32toshexn_len(uint32_t value);

void inttoshex4(char *result, unsigned int value);
size_t inttoshexn4(char *result, unsigned int value);
size_t inttoshexn4_len(unsigned int value);

void ptrtoshex(char* result, uintptr_t value);
size_t ptrtoshexn(char* result, uintptr_t value);
size_t ptrtoshexn_len(uintptr_t value);

void sztos(char* result, size_t value);
size_t sztosn(char* result, size_t value);
size_t sztosn_len(size_t value);

void ssztos(char* result, ssize_t value);
size_t ssztosn(char* result, ssize_t value);
size_t ssztosn_len(ssize_t value);

void uinttobin(char* result, unsigned int value);
size_t uinttobinn(char* result, unsigned int value);
size_t uinttobin_len();

void ulongtobin(char* result, unsigned long value);
size_t ulongtobinn(char* result, unsigned long value);
size_t ulongtobin_len();

const char b10_to_ascii(uint8_t n);
const char b16_to_ascii(uint8_t n, bool caps);

size_t hexdump_len(size_t data_len);
void hexdump(char* dest, const void* data, size_t data_len);

#define hexdump_var(name, data, data_len) \
    char name[hexdump_len(data_len) + 1]; \
    hexdump(name, data, data_len);
