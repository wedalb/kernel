// SPDX-License-Identifier: GPL-3.0-or-later
#include <stdarg.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdint.h>

#include "include/spec/units/byte.h"

#include "kernel/boot_iface.h"
#include "lib/tinystd/stddef.h"
#include "lib/tinystd/str.h"
#include "lib/tinystd/mm.h"

#include "printf.h"
#include "streams.h"

size_t vsprintf_destlen(const char* fmt, va_list vargs) {
    char fmt_c = *fmt;
    char fmt_c_next;
    bool pct = false;

    size_t dest_i = 0;
    for (size_t src_i = 0; fmt_c; src_i++) {
        fmt_c = fmt[src_i];
        fmt_c_next = fmt[src_i + 1];
        if (pct) {
            switch (fmt_c) {
                case 'b':
                    dest_i += uinttobin_len() + 2 /* for 0b */;
                    break;
                case '?':
                    dest_i += ((bool) va_arg(vargs, int)) ? 4 : 5;
                    break;
                case 'c':
                    dest_i += sizeof(char);
                    break;
                case 's':
                    dest_i += strlen(va_arg(vargs, char*));
                    break;
                case 'i':
                case 'd':
                    dest_i += u32tosn_len(va_arg(vargs, int32_t));
                    break;
                case 'u':
                    dest_i += u32tosn_len(va_arg(vargs, uint32_t));
                    break;
                case 'l': {
                    switch (fmt_c_next) {
                        case 'b':
                            dest_i += ulongtobin_len() + 2 /* for 0b */;
                            break;
                        case 'u':
                            dest_i += u64tosn_len(va_arg(vargs, uint64_t));
                            break;
                        case 'x':
                            dest_i += u64toshexn_len(va_arg(vargs, unsigned long)) + 2 /* for 0x */;
                            break;
                        case 'i':
                        case 'd':
                        default:
                            dest_i += s64tosn_len(va_arg(vargs, int64_t));
                            break;
                    }
                    src_i++;
                    break;
                }
                case 'p':
                    dest_i += ptrtoshexn_len(va_arg(vargs, uintptr_t)) + 2 /* for 0x */;
                    break;
                case 'x':
                    dest_i += u32toshexn_len(va_arg(vargs, uint32_t)) + 2 /* for 0x */;
                    break;
                case 'z':
                    switch (fmt_c_next) {
                        case 's':
                            dest_i += ssztosn_len(va_arg(vargs, ssize_t));
                            break;
                        case 'u':
                        default:
                            dest_i += sztosn_len(va_arg(vargs, size_t));
                            break;
                    }
                    break;
                default:
                    va_arg(vargs, uint32_t);
                    dest_i += 2;
                    break;
            }
            pct = false;
        } else {
            switch (fmt_c) {
                case '%':
                    pct = true;
                    break;
                default:
                    dest_i++;
                    break;
            }
        }
    }

    return dest_i;
}

void vsnprintf(char* result, size_t len, const char* fmt, va_list vargs) {
    char fmt_c = *fmt;
    char fmt_c_next;
    bool pct = false;
    char* cur_pos = result;

    size_t dest_i = 0;
    for (size_t src_i = 0; dest_i < len && fmt_c; src_i++) {
        fmt_c = fmt[src_i];
        fmt_c_next = fmt[src_i + 1];
        cur_pos = &result[dest_i];
        if (pct) {
            switch (fmt_c) {
                case 'b': {
                    const int n = va_arg(vargs, int);
                    cur_pos[0] = '0';
                    cur_pos[1] = 'b';
                    cur_pos = &cur_pos[2];

                    dest_i += 2;
                    size_t sz = uinttobinn(cur_pos, n);
                    dest_i += sz;
                    break;
                }
                case '?': {
                    const bool b = (bool) va_arg(vargs, int);
                    if (b) {
                        cur_pos[0] = 't';
                        cur_pos[1] = 'r';
                        cur_pos[2] = 'u';
                        cur_pos[3] = 'e';
                        dest_i += 4;
                    } else {
                        cur_pos[0] = 'f';
                        cur_pos[1] = 'a';
                        cur_pos[2] = 'l';
                        cur_pos[3] = 's';
                        cur_pos[4] = 'e';
                        dest_i += 5;
                    }
                    break;
                }
                case 'c': {
                    const char c = va_arg(vargs, int);
                    cur_pos[0] = c;
                    dest_i += sizeof(char);
                    break;
                }
                case 's': {
                    const char* string = va_arg(vargs, char*);
                    size_t len = strlen(string);
                    memcpy(cur_pos, string, len);
                    dest_i += len;
                    break;
                }
                case 'i':
                case 'd': {
                    const int32_t n = va_arg(vargs, int32_t);
                    const size_t sz = s32tosn(cur_pos, n);
                    dest_i += sz;
                    break;
                }
                case 'u': {
                    const uint32_t n = va_arg(vargs, uint32_t);
                    const size_t sz = u32tosn(cur_pos, n);
                    dest_i += sz;
                    break;
                }
                case 'l': {
                    switch (fmt_c_next) {
                        case 'b': {
                            const long n = va_arg(vargs, long);
                            cur_pos[0] = '0';
                            cur_pos[1] = 'b';
                            cur_pos = &cur_pos[2];

                            dest_i += 2;
                            size_t sz = ulongtobinn(cur_pos, n);
                            dest_i += sz;
                            break;
                        }
                        case 'u': {
                            uint64_t n = va_arg(vargs, uint64_t);
                            size_t sz = u64tosn(cur_pos, n);
                            dest_i += sz;
                            break;
                        }
                        case 'x': {
                            cur_pos[0] = '0';
                            cur_pos[1] = 'x';
                            cur_pos = &cur_pos[2];
                            dest_i += 2;

                            unsigned long n = va_arg(vargs, unsigned long);
                            size_t sz = u64toshexn(cur_pos, n);
                            dest_i += sz;
                            break;
                        }
                        case 'i':
                        case 'd':
                        default: {
                            int64_t n = va_arg(vargs, int64_t);
                            size_t sz = s64tosn(cur_pos, n);
                            dest_i += sz;
                            break;
                        }
                    }
                    src_i++;
                    break;
                }
                case 'p': {
                    cur_pos[0] = '0';
                    cur_pos[1] = 'x';
                    cur_pos = &cur_pos[2];
                    dest_i += 2;

                    uintptr_t ptr = va_arg(vargs, uintptr_t);
                    size_t sz = ptrtoshexn(cur_pos, ptr);
                    dest_i += sz;
                    break;
                }
                case 'x': {
                    cur_pos[0] = '0';
                    cur_pos[1] = 'x';
                    cur_pos = &cur_pos[2];
                    dest_i += 2;
                    switch (fmt_c_next) {
                        case '4': {
                            unsigned int n = va_arg(vargs, unsigned int);
                            size_t sz = inttoshexn4(cur_pos, n);
                            dest_i += sz;
                            src_i++;
                            break;
                        }
                        case '8':
                            src_i++;
                        default: {
                            uint32_t n = va_arg(vargs, uint32_t);
                            size_t sz = u32toshexn(cur_pos, n);
                            dest_i += sz;
                            break;
                        }
                    }
                    break;
                }
                case 'z':
                    switch (fmt_c_next) {
                        case 's': {
                            ssize_t n = va_arg(vargs, ssize_t);
                            size_t sz = ssztosn(cur_pos, n);
                            dest_i += sz;
                            break;
                        }
                        case 'u':
                        default: {
                            size_t n = va_arg(vargs, size_t);
                            size_t sz = sztosn(cur_pos, n);
                            dest_i += sz;
                            break;
                        }
                    }
                    src_i++;
                    break;
                default:
                    *cur_pos = '%';
                    cur_pos[1] = fmt_c;
                    dest_i += 2;
                    va_arg(vargs, size_t);
                    break;
                }
            pct = false;
        } else {
            switch (fmt_c) {
                case '%':
                    pct = true;
                    break;
                default:
                    *cur_pos = fmt_c;
                    dest_i++;
                    break;
            }
        }
    }

    result[dest_i] = '\0';
}

void snprintf(char* result, size_t len, const char* fmt, ...) {
    va_list vargs;
    va_start(vargs, fmt);
    vsnprintf(result, len, fmt, vargs);
    va_end(vargs);
}

void vprintf(const char* fmt, va_list vargs) {
    va_list my_vargs;
    va_copy(my_vargs, vargs);
    size_t len = vsprintf_destlen(fmt, my_vargs);
    char result[len + 1];
    va_end(my_vargs);

    vsnprintf(result, len, fmt, vargs);

    pipe_stdout_write(result);
}

void printf(const char* fmt, ...) {
    va_list vargs;
    va_start(vargs, fmt);
    vprintf(fmt, vargs);
    va_end(vargs);
}

__attribute__ ((no_caller_saved_registers))
void printf_callee_saved(const char* fmt, ...) {
    va_list vargs;
    va_start(vargs, fmt);
    vprintf(fmt, vargs);
    va_end(vargs);
}
