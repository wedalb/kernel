// SPDX-License-Identifier: GPL-3.0-or-later
#include "streams.h"

void pipe_stdout_write(const char *data) {
    pipe_stream_write(stdout, data);
}

void pipe_stderr_write(const char *data) {
    pipe_stream_write(stderr, data);
}
