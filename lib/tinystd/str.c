// SPDX-License-Identifier: GPL-3.0-or-later
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "include/spec/units/byte.h"
#include "lib/tinystd/math.h"
#include "lib/tinystd/stddef.h"

#include "str.h"

size_t strlen(const char* str) {
    size_t len = 0;
    while (str[len]) {
        len++;
    }
    return len;
}

size_t strchoccur(const char* str, const char c, size_t len) {
    size_t occur = 0;
    for (size_t i = 0; i < len; i++) {
        if (str[i] == c) {
            occur++;
        }
    }
    return occur;
}

// 18446744073709551615
// 12345678901234567890
// 20 characters max.
/**
 * Converts a 64 bit unsigned integer to a string.
 * The result should be at least of size 21 to accomodate
 * a maximum of 20 characters for a 64 bit int + the NULL terminator to fit.
 *
 * This function terminates the string after the last character.
 */
void u64tos(char* result, uint64_t value) {
    size_t len = u64tosn(result, value);
    result[len] = '\0';
}

/**
 * Converts a 64 bit unsigned integer to a string.
 * The result should be at least of size 20 to accomodate
 * a maximum of 20 characters for a 64 bit to fit.
 *
 * This function DOES NOT terminate the string after the last character.
 * If you want it to be null-terminated, use u64tos() instead.
 */
size_t u64tosn(char* result, uint64_t value) {
    if (value == 0) {
        result[0] = '0';
        return 1;
    }

    uint64_t quotient = value;
    uint64_t remainder;
    size_t len = u64tosn_len(value);

    size_t i = len;
    while (quotient > 0) {
        remainder = quotient % 10;
        result[--i] = b10_to_ascii(remainder);
        quotient /= 10;
    }

    return len;
}

size_t u64tosn_len(uint64_t value) {
    if (value < 10) {
        return 1;
    }

    uint64_t quotient = value;
    size_t len = 0;
    for (; quotient > 0; len++, quotient /= 10);
    return len;
}

void s64tos(char* result, int64_t value) {
    size_t len = s64tosn(result, value);
    result[len] = '\0';
}

size_t s64tosn(char* result, int64_t value) {
    char* final_result = result;
    if (value < 0) {
        result[0] = '-';
        final_result = &result[1];
    }
    return u64tosn(final_result, (uint64_t) math_abs_64(value)) + (value < 0);
}

size_t s64tosn_len(int64_t value) {
    return u64tosn_len((uint64_t) math_abs_64(value)) + (value < 0);
}

// 4294967295
// 1234567890
// Max characters: 10
/**
 * Converts a 32 bit unsigned integer to a string.
 * The result should be at least of size 11 to accomodate
 * a maximum of 10 characters for a 32 bit int + the NULL terminator to fit.
 *
 * This function terminates the string after the last character.
 */
void u32tos(char* result, uint32_t value) {
    size_t len = u32tosn(result, value);
    result[len] = '\0';
}

/**
 * Converts a 32 bit unsigned integer to a string.
 * The result should be at least of size 10 to accomodate
 * a maximum of 10 characters for a 32 bit int to fit.
 *
 * This function DOES NOT terminate the string after the last character.
 * If you want it to be null-terminated, use u64tos() instead.
 */
size_t u32tosn(char* result, uint32_t value) {
    if (value == 0) {
        result[0] = '0';
        return 1;
    }

    uint32_t quotient = value;
    uint32_t remainder;
    size_t len = u32tosn_len(value);

    size_t i = len;
    while (quotient > 0) {
        remainder = quotient % 10;
        result[--i] = b10_to_ascii(remainder);
        quotient /= 10;
    }

    return len;
}

size_t u32tosn_len(uint32_t value) {
    if (value < 10) {
        return 1;
    }

    uint32_t quotient = value;
    size_t len = 0;
    for (; quotient > 0; len++, quotient /= 10);
    return len;
}


void s32tos(char* result, int32_t value) {
    size_t len = s32tosn(result, value);
    result[len] = '\0';
}

size_t s32tosn(char* result, int32_t value) {
    char* final_result = result;
    if (value < 0) {
        result[0] = '-';
        final_result = &result[1];
    }
    return u32tosn(final_result, (uint32_t) math_abs_32(value)) + (value < 0);
}

size_t s32tosn_len(int32_t value) {
    return u64tosn_len((uint32_t) math_abs_32(value)) + (value < 0);
}

const char b10_to_ascii(uint8_t n) {
    if (n >= 10) {
        return '?';
    }

    return '0' + n;
}

void u64toshex(char* result, uint64_t value) {
    size_t len = u64toshexn(result, value);
    result[len] = '\0';
}

size_t u64toshexn(char* result, uint64_t value) {
    if (value == 0) {
        result[0] = '0';
        return 1;
    }

    uint64_t quotient = value;
    uint64_t remainder;
    size_t len = u64toshexn_len(value);

    size_t i = len;
    while (quotient > 0) {
        remainder = quotient % 16;
        result[--i] = b16_to_ascii(remainder, false);
        quotient /= 16;
    }

    return len;
}


size_t u64toshexn_len(uint64_t value) {
    if (value <= 0xF) {
        return 1;
    }

    uint64_t quotient = value;
    size_t len = 0;
    for (; quotient > 0; len++, quotient /= 16);
    return len;
}

void u32toshex(char* result, uint32_t value) {
    size_t len = u64toshexn(result, value);
    result[len] = '\0';
}

size_t u32toshexn(char* result, uint32_t value) {
    if (value == 0) {
        result[0] = '0';
        return 1;
    }

    uint32_t quotient = value;
    uint32_t remainder;
    size_t len = u64toshexn_len(value);

    size_t i = len;
    while (quotient > 0) {
        remainder = quotient % 16;
        result[--i] = b16_to_ascii(remainder, false);
        quotient /= 16;
    }

    return len;
}

size_t u32toshexn_len(uint32_t value) {
    if (value <= 0xF) {
        return 1;
    }

    uint32_t quotient = value;
    size_t len = 0;
    for (; quotient > 0; len++, quotient /= 16);
    return len;
}

void inttoshex4(char *result, unsigned int value) {
    size_t len = u64toshexn(result, value);
    result[len] = '\0';
}

size_t inttohexn4_len_dyn(unsigned int value) {
    unsigned int quotient = value;
    size_t len = 0;
    while (quotient > 0 && len < 4) {
        len++;
        quotient /= 16;
    }
    return len;
}

size_t inttoshexn4(char *result, unsigned int value) {
    unsigned int quotient = value;
    unsigned int remainder;
    size_t len = 4;
    size_t fill_zeroes_count = len - inttohexn4_len_dyn(value);
    for (uint8_t i = 0; i < fill_zeroes_count; i++) {
        result[i] = '0';
    }

    size_t i = len;
    while (quotient > 0 && i > 0) {
        remainder = quotient % 16;
        result[--i] = b16_to_ascii(remainder, false);
        quotient /= 16;
    }

    return len;
}

void ptrtoshex(char* result, uintptr_t value) {
    size_t len = u64toshexn(result, value);
    result[len] = '\0';
}

size_t ptrtohexn_len_dyn(uintptr_t value) {
    uintptr_t quotient = value;
    size_t len = 0;
    for (; quotient > 0; len++, quotient /= 16);
    return len;
}

size_t ptrtoshexn(char* result, uintptr_t value) {
    uintptr_t quotient = value;
    uintptr_t remainder;
    size_t len = ptrtoshexn_len(value);
    size_t fill_zeroes_count = len - ptrtohexn_len_dyn(value);
    for (uint8_t i = 0; i < fill_zeroes_count; i++) {
        result[i] = '0';
    }

    size_t i = len;
    while (quotient > 0) {
        remainder = quotient % 16;
        result[--i] = b16_to_ascii(remainder, false);
        quotient /= 16;
    }

    return len;
}

void sztos(char* result, size_t value) {
    size_t len = sztosn(result, value);
    result[len] = '\0';
}

size_t sztosn(char* result, size_t value) {
    if (value == 0) {
        result[0] = '0';
        return 1;
    }

    size_t quotient = value;
    size_t remainder;
    size_t len = sztosn_len(value);

    size_t i = len;
    while (quotient > 0) {
        remainder = quotient % 10;
        result[--i] = b10_to_ascii(remainder);
        quotient /= 10;
    }

    return len;
}

size_t sztosn_len(size_t value) {
    if (value < 10) {
        return 1;
    }

    size_t quotient = value;
    size_t len = 0;
    for (; quotient > 0; len++, quotient /= 10);
    return len;
}

void ssztos(char* result, ssize_t value) {
    size_t len = ssztosn(result, value);
    result[len] = '\0';
}

size_t ssztosn(char* result, ssize_t value) {
    if (value == 0) {
        result[0] = '0';
        return 1;
    }

    size_t quotient = value;
    size_t remainder;
    size_t len = ssztosn_len(value);

    size_t i = len;
    while (quotient > 0) {
        remainder = quotient % 10;
        result[--i] = b10_to_ascii(remainder);
        quotient /= 10;
    }

    return len;
}

size_t ssztosn_len(ssize_t value) {
    if (value < 10) {
        return 1;
    }

    size_t quotient = value;
    size_t len = 0;
    for (; quotient > 0; len++, quotient /= 10);
    return len;
}

#define PTR_SIZE (sizeof(uintptr_t))

size_t ptrtoshexn_len(uintptr_t value) {
    return PTR_SIZE * 2; // 1 byte = 2 hex chars
}

const char b16_to_ascii(uint8_t n, bool caps) {
    if (n >= 0x10) {
        return '?';
    }

    if (n < 0xA) {
        return '0' + n;
    } else {
        return (caps ? 'A' : 'a') + (n - 0xA);
    }
}

static size_t uinttobinn_len_dyn(unsigned int value) {
    unsigned int quotient = value;
    size_t len = 0;
    for (; quotient > 0; len++, quotient /= 2);
    return len;
}

void uinttobin(char* result, unsigned int value) {
    size_t len = uinttobinn(result, value);
    result[len] = '\0';
}

size_t uinttobinn(char* result, unsigned int value) {
    unsigned int quotient = value;
    unsigned int remainder;
    size_t len = uinttobin_len(value);
    size_t fill_zeroes_count = len - uinttobinn_len_dyn(value);
    for (int i = 0; i < fill_zeroes_count; i++) {
        result[i] = '0';
    }

    size_t i = len;
    while (quotient > 0) {
        remainder = quotient % 2;
        result[--i] = remainder ? '1' : '0';
        quotient /= 2;
    }

    return len;
}

size_t uinttobin_len() {
    return BITS_PER_BYTE * sizeof(unsigned int);
}


static size_t ulongtobinn_len_dyn(unsigned long value) {
    unsigned long quotient = value;
    size_t len = 0;
    for (; quotient > 0; len++, quotient /= 2);
    return len;
}

void ulongtobin(char* result, unsigned long value) {
    size_t len = ulongtobinn(result, value);
    result[len] = '\0';
}

size_t ulongtobinn(char* result, unsigned long value) {
    unsigned long quotient = value;
    unsigned long remainder;
    size_t len = ulongtobin_len(value);
    size_t fill_zeroes_count = len - ulongtobinn_len_dyn(value);
    for (int i = 0; i < fill_zeroes_count; i++) {
        result[i] = '0';
    }

    size_t i = len;
    while (quotient > 0) {
        remainder = quotient % 2;
        result[--i] = remainder ? '1' : '0';
        quotient /= 2;
    }

    return len;
}

size_t ulongtobin_len() {
    return BITS_PER_BYTE * sizeof(unsigned long);
}

#define HEXDUMP_BREAK_AT_BYTES 16
#define HEXDUMP_SPACING_AT_BYTES (HEXDUMP_BREAK_AT_BYTES / 2)
size_t hexdump_len(size_t data_len) {
    return data_len * 3 + (data_len / HEXDUMP_BREAK_AT_BYTES) + (data_len / HEXDUMP_SPACING_AT_BYTES) + 1;
}

void hexdump(char* dest, const void* data, size_t data_len) {
    uint8_t val;
    int cursor = 0;
    for (int i = 0; i < data_len; i++) {
        val = ((const uint8_t*) data)[i];
        dest[cursor++] = b16_to_ascii((val & 0xF0) >> 4, true);
        dest[cursor++] = b16_to_ascii(val & 0x0F, true);
        dest[cursor++] = ' ';
        if ((i + 1) % HEXDUMP_BREAK_AT_BYTES == 0) {
            dest[cursor++] = '\n';
        } else if ((i + 1) % HEXDUMP_SPACING_AT_BYTES == 0) {
            dest[cursor++] = ' ';
        }
    }
    dest[cursor++] = '\0';
}