// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stdint.h>

static inline uint16_t little16_to_big(uint16_t little) {
    return (uint16_t) 0U |
        little >> 8 |
        little << 8;
}

static inline uint32_t little32_to_big(uint32_t little) {
    return (uint32_t) 0U |
        little >> 24 |
        little << 8 >> 16 |
        little << 16 >> 8 |
        little << 24;
}

static inline uint64_t little64_to_big(uint64_t little) {
    return (uint64_t) 0U |
        little >> 56 |
        little << 8 >> 48 |
        little << 16 >> 40 |
        little << 24 >> 32 |
        little << 32 >> 24 |
        little << 40 >> 16 |
        little << 48 >> 8 |
        little << 56;
}

// Since both ways do essentially the same thing, we'll redefine them here

#define big16_to_little little16_to_big
#define big32_to_little little32_to_big
#define big64_to_little little64_to_big
