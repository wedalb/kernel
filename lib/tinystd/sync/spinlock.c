// SPDX-License-Identifier: GPL-3.0-or-later
#include "spinlock.h"

void spinlock_acquire(spinlock* lock) {
    while (!__sync_bool_compare_and_swap(lock, 0, 1)) {
        __sync_synchronize();
    }
}

void spinlock_release(spinlock* lock) {
    __sync_synchronize();
    *lock = 0;
}
