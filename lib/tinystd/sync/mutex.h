// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

typedef volatile int mutex;

void mutex_lock(mutex* mut);
void mutex_unlock(mutex* mut);
void mutex_wait(mutex* mut);
