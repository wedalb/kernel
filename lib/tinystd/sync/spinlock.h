// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stdint.h>

typedef volatile uint32_t spinlock;

void spinlock_acquire(spinlock* lock);
void spinlock_release(spinlock* lock);

#define spinlock_guard(lock, scope) \
    spinlock_acquire(&lock); \
    scope; \
    spinlock_release(&lock);

#define spinlock_wait(lock) \
    spinlock_acquire(&lock); \
    spinlock_release(&lock)
