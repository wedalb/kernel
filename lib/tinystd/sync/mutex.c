// SPDX-License-Identifier: GPL-3.0-or-later
#include "mutex.h"

void mutex_lock(mutex* mut) {

}

void mutex_unlock(mutex* mut) {

}

void mutex_wait(mutex* mut) {
    mutex_lock(mut);
    mutex_unlock(mut);
}
