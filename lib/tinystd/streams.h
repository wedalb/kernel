// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stdint.h>

#define STD_INTERFACE __attribute__((sysv_abi))

#define stdin   0
#define stdout  1
#define stderr  2

STD_INTERFACE
void pipe_stream_write(uint8_t stream, const char* data);

void pipe_stdout_write(const char* data);
void pipe_stderr_write(const char* data);

