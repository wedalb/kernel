// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stdarg.h>
#include <stddef.h>

void vsnprintf(char* result, size_t len, const char* fmt, va_list vargs);
void snprintf(char* result, size_t len, const char* fmt, ...);
void vprintf(const char* fmt, va_list vargs);
void printf(const char* fmt, ...);

__attribute__((no_caller_saved_registers))
void printf_callee_saved(const char* fmt, ...);
