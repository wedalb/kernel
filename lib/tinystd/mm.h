// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

void memset(void* ptr, const char c, size_t len);
void memmove(void* dest, const void* src, size_t len);
void memcpy(void* dest, const void* src, size_t len);
bool memncmp(const void* a, const void* b, size_t len);

static inline void memzero(void* dest, size_t len) {
    memset(dest, 0, len);
}