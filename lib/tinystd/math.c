// SPDX-License-Identifier: GPL-3.0-or-later
#include <stdint.h>

int64_t math_abs_64(int64_t n) {
    if (n < 0) {
        return n ^ 0xFFFFFFFFFFFFFFFF - 1;
    } else {
        return n;
    }
}

int32_t math_abs_32(int32_t n) {
    if (n < 0) {
        return n ^ 0xFFFFFFFF - 1;
    } else {
        return n;
    }
}

int16_t math_abs_16(int16_t n) {
    if (n < 0) {
        return n ^ 0xFFFF - 1;
    } else {
        return n;
    }
}

int8_t math_abs_8(int8_t n) {
    if (n < 0) {
        return n ^ 0xFF - 1;
    } else {
        return n;
    }
}

int math_abs(int n) {
    uint64_t n64 = n;
    if (n64 < 0) {
        return (int) (n ^ 0xFFFFFFFFFFFFFFFF - 1);
    } else {
        return n;
    }
}

#define max(a, b) ((a) > (b) ? (a) : (b))
#define min(a, b) ((a) < (b) ? (a) : (b))
#define max3(a, b, c) max(a, (max(b, c)))
#define min3(a, b, c) min(a, (min(b, c)))
