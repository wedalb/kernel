// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stddef.h>
#include <stdint.h>

#include "include/internal/gfx/colors/format.h"

#include "kernel/drivers.h"

typedef uint32_t fbcon_px_size;
typedef uint8_t fbcon_subpixel;

typedef struct {
    fbcon_subpixel r;
    fbcon_subpixel g;
    fbcon_subpixel b;
    fbcon_subpixel a;
} fbcon_pixel_color;

typedef struct {
    char* fb;
    size_t size;
    uint32_t width;
    uint32_t height;
    uint32_t pps;
    gfx_pixel_color_format format;

    char* dfb;
} fbcon_framebuffer;

void fbcon_init();
void fbcon_init_mem();
void fbcon_write_text(const char* string);
void fbcon_backspace();
void fbcon_draw_pixel(fbcon_px_size x, fbcon_px_size y, fbcon_pixel_color color);
void fbcon_draw_pixel_on(uintptr_t fb_offs, fbcon_pixel_color color, char* fb);
void fbcon_use_framebuffer(fbcon_framebuffer* fb);
void fbcon_draw_rect_filled_on(fbcon_px_size x, fbcon_px_size y, fbcon_px_size width, fbcon_px_size height, fbcon_pixel_color color, char* fb);
void fbcon_draw_rect_filled(fbcon_px_size x, fbcon_px_size y, fbcon_px_size width, fbcon_px_size height, fbcon_pixel_color color);
void fbcon_draw_rect_inverted_on(fbcon_px_size x, fbcon_px_size y, fbcon_px_size width, fbcon_px_size height, char* fb);
void fbcon_erase_screen();
void fbcon_flip();
