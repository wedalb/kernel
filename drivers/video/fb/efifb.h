// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stddef.h>
#include <stdint.h>

#include "include/internal/gfx/colors/format.h"

#include "kernel/drivers.h"

#include "console.h"


typedef struct {
    fbcon_framebuffer framebuffer;
} driver_efifb_handoff;

typedef struct {
    void (*init_driver)(kernel_driver* driver, void* extras);
    void (*init)(driver_efifb_handoff* handoff);
} driver_efifb_driver_iface;

extern driver_efifb_driver_iface driver_efifb_iface;