// SPDX-License-Identifier: GPL-3.0-or-later
#include "drivers/video/fb/console.h"
#include "include/internal/gfx/colors/format.h"
#include "include/internal/mm/bases.h"
#include "kernel/boot_iface.h"
#include "kernel/mm/alloc.h"
#include "lib/tinystd/mm.h"
#include "lib/tinystd/printf.h"
#include "lib/fonts/pixel_font/default_font.h"
#include "lib/fonts/pixel_font/font.h"

#include "kernel/mm/paging.h"

#include "drivers/video/fb/efifb.h"
#include <stdint.h>

void driver_efifb_init_driver(kernel_driver* driver, void* extras) {
    ((driver_efifb_driver_iface*) driver->driver)->init((driver_efifb_handoff*) extras);
    driver->is_ready = true;
}

void driver_efifb_init(driver_efifb_handoff* handoff) {
    void *framebuffer_addr = kernel_mm_alloc_mapping(
        handoff->framebuffer.fb, handoff->framebuffer.size);
    fbcon_init(&((fbcon_framebuffer) {
        .fb = framebuffer_addr,
        .size = handoff->framebuffer.size,
        .width = handoff->framebuffer.width,
        .height = handoff->framebuffer.height,
        .pps = handoff->framebuffer.pps,
        .format = handoff->framebuffer.format,
    }));
}

driver_efifb_driver_iface driver_efifb_iface = {
    .init = driver_efifb_init,
    .init_driver = driver_efifb_init_driver,
};

