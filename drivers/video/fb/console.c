// SPDX-License-Identifier: GPL-3.0-or-later
#include <stdint.h>

#include "lib/tinystd/mm.h"
#include "lib/tinystd/printf.h"
#include "lib/tinystd/sync/spinlock.h"
#include "lib/tinystd/math.h"

#include "kernel/mm/alloc.h"
#include "kernel/mm/paging.h"
#include "kernel/boot_iface.h"
#include "include/internal/gfx/colors/format.h"
#include "lib/fonts/pixel_font/default_font.h"
#include "lib/fonts/pixel_font/font.h"

#include "console.h"

#define TAB_SIZE 4

typedef struct {
    uint32_t col;
    uint32_t row;
    fbcon_pixel_color color;
    fbcon_pixel_color back_color;
    fbcon_pixel_color cursor_color;
    fbcon_framebuffer framebuffer;
} fbcon_console;

/**
 * Text color
 **/
static fbcon_pixel_color default_foreground_color = {
    .r = 0xE8,
    .g = 0xE8,
    .b = 0xE8,
    .a = 0xFF,
};

/**
 * Text background color
 **/
static fbcon_pixel_color default_background_color = {
    .r = 0x00,
    .g = 0x00,
    .b = 0x00,
    .a = 0xFF,
};

/**
 * Cursor color
 **/
static fbcon_pixel_color default_cursor_color = {
    .r = 0xFF,
    .g = 0xFF,
    .b = 0xFF,
    .a = 0x9F,
};

static fbcon_console console = {
    .col = 0,
    .row = 0,
};

static spinlock copy_lock;
static spinlock text_lock;

static int fb_start_index = 0;

void fbcon_init(fbcon_framebuffer* framebuffer) {
    memcpy(&console.framebuffer, framebuffer, sizeof(fbcon_framebuffer));
    console.framebuffer.dfb = kernel_mm_alloc(console.framebuffer.size);
    console.back_color = default_background_color;
    console.color = default_foreground_color;
    console.cursor_color = default_cursor_color;
    fbcon_erase_screen();
    fbcon_flip();
}

static inline uint32_t fbcon_row_height() {
    return PIXEL_FONT_ROW_HEIGHT * PIXEL_FONT_SCALE;
}

static inline uint8_t fbcon_color_mult() {
    switch (console.framebuffer.format) {
    case RGBA_32:
    case BGRA_32:
    case ABGR_32:
    case ARGB_32:
    case RGBR_32:
    case BGRR_32:
    case VALUE_32:
        return 32 / 8;
    case RGB_24:
    case BGR_24:
    case VALUE_24:
        return 24 / 8;
    case VALUE_8:
        return 8 / 8;
    }
}

static inline fbcon_px_size fbcon_pitch() {
    return console.framebuffer.pps * fbcon_color_mult();
}

static inline uint32_t fbcon_row_bytes() {
    return fbcon_row_height() * fbcon_pitch();
}

static inline uint32_t fbcon_column_width() {
    return PIXEL_FONT_COL_WIDTH * PIXEL_FONT_SCALE;
}

static inline uint32_t fbcon_max_col() {
    return console.framebuffer.width / fbcon_column_width();
}

static inline uint32_t fbcon_max_row() {
    return console.framebuffer.height / fbcon_row_height();
}

static inline uintptr_t fbcon_calculate_index(fbcon_px_size x, fbcon_px_size y) {
    return fbcon_pitch()* y + x * fbcon_color_mult();
}

static inline uintptr_t fbcon_translate_index(uintptr_t index) {
    // Since we are using a rotated double framebuffer, we need to translate the index
    // so that we can access the correct pixel
    return (index + fb_start_index) % console.framebuffer.size;
}

static inline fbcon_subpixel fbcon_color_multiply_blend_alpha(fbcon_subpixel color, fbcon_subpixel original, fbcon_subpixel alpha) {
    return (color * alpha + original * (0xFF - alpha)) / 0xFF;
}

static void fbcon_draw_cursor() {
    // We draw directly onto the fb because the dfb should not include it
    fbcon_draw_rect_inverted_on(
        console.col * PIXEL_FONT_COL_WIDTH * PIXEL_FONT_SCALE,
        console.row * fbcon_row_height(),
        PIXEL_FONT_COL_WIDTH * PIXEL_FONT_SCALE,
        PIXEL_FONT_ROW_HEIGHT * PIXEL_FONT_SCALE,
        console.framebuffer.fb
    );
}

static void fbcon_add_overlays() {
    // Add cursor
    fbcon_draw_cursor();
}

void fbcon_flip() {
    // Copy the double framebuffer to the actual framebuffer
    // The double framebuffer is a rotating buffer
    // We start copying at fb_start_index until the end of the buffer
    // and then we copy from the beginning of the buffer until fb_start_index
    memcpy(
        console.framebuffer.fb,
        console.framebuffer.dfb + fb_start_index,
        console.framebuffer.size - fb_start_index
    );
    memcpy(
        console.framebuffer.fb + console.framebuffer.size - fb_start_index,
        console.framebuffer.dfb,
        fb_start_index
    );
    fbcon_add_overlays();
}

void fbcon_draw_pixel(fbcon_px_size x, fbcon_px_size y, fbcon_pixel_color color) {
    if (!console.framebuffer.dfb) {
        serial_output("Attempt to use fbcon without a double framebuffer\n");
        return;
    }
    uintptr_t fb_offs = fbcon_calculate_index(x, y);
    fbcon_draw_pixel_on(fbcon_translate_index(fb_offs), color, console.framebuffer.dfb);
}

void fbcon_draw_pixel_on(uintptr_t fb_index, fbcon_pixel_color color, char* fb) {
    fbcon_subpixel* subpixels = (fbcon_subpixel*) &fb[fb_index];
    switch (console.framebuffer.format) {
        case RGBA_32:
        case RGBR_32:
            subpixels[3] = 0;
        case RGB_24:
            subpixels[0] = fbcon_color_multiply_blend_alpha(color.r, subpixels[0], color.a);
            subpixels[1] = fbcon_color_multiply_blend_alpha(color.g, subpixels[1], color.a);
            subpixels[2] = fbcon_color_multiply_blend_alpha(color.b, subpixels[2], color.a);
            break;
        case BGRA_32:
        case BGRR_32:
            subpixels[3] = 0;
        case BGR_24:
            subpixels[0] = fbcon_color_multiply_blend_alpha(color.b, subpixels[0], color.a);
            subpixels[1] = fbcon_color_multiply_blend_alpha(color.g, subpixels[1], color.a);
            subpixels[2] = fbcon_color_multiply_blend_alpha(color.r, subpixels[2], color.a);
            break;
        default: {
            const char format_string[] = "fixme: format %d not implemented\n";
            char buf[sizeof(format_string) + 32] = {0};
            snprintf(buf, sizeof(format_string) + 32, format_string, console.framebuffer.format);
            serial_output(buf);
            // TODO: not implemented, don't bother with it now.
            break;
        }
    }
}

void fbcon_invert_pixel_with_color_on(uintptr_t fb_index, fbcon_pixel_color color, char* fb) {
    fbcon_subpixel* subpixels = (fbcon_subpixel*) &fb[fb_index];
    switch (console.framebuffer.format) {
        case RGBA_32:
        case RGBR_32:
            subpixels[3] = 0;
        case RGB_24:
            // invert pixel and blend with color
            subpixels[0] = fbcon_color_multiply_blend_alpha(0xFF - subpixels[0], color.r, color.a);
            subpixels[1] = fbcon_color_multiply_blend_alpha(0xFF - subpixels[1], color.g, color.a);
            subpixels[2] = fbcon_color_multiply_blend_alpha(0xFF - subpixels[2], color.b, color.a);
            break;
        case BGRA_32:
        case BGRR_32:
            subpixels[3] = 0;
        case BGR_24:
            subpixels[0] = fbcon_color_multiply_blend_alpha(0xFF - subpixels[0], color.r, color.a);
            subpixels[1] = fbcon_color_multiply_blend_alpha(0xFF - subpixels[1], color.g, color.a);
            subpixels[2] = fbcon_color_multiply_blend_alpha(0xFF - subpixels[2], color.b, color.a);
            break;
        default: {
            const char format_string[] = "fixme: format %d not implemented\n";
            char buf[sizeof(format_string) + 32] = {0};
            snprintf(buf, sizeof(format_string) + 32, format_string, console.framebuffer.format);
            serial_output(buf);
            break;
        }
    }
}

void fbcon_draw_pixel_scaled(fbcon_px_size x, fbcon_px_size y, fbcon_pixel_color color, uint8_t scale) {
    if (scale == 1) {
        fbcon_draw_pixel(x, y, color);
        return;
    }
    for (uint8_t xi = 0; xi < scale; xi++) {
        for (uint8_t yi = 0; yi < scale; yi++) {
            fbcon_draw_pixel(x * scale + xi, y * scale + yi, color);
        }
    }
}

void fbcon_draw_rect_inverted_on(fbcon_px_size x, fbcon_px_size y, fbcon_px_size width, fbcon_px_size height, char* fb) {
    for (fbcon_px_size y_cur = y; y_cur < y + height; y_cur++) {
        for (fbcon_px_size x_cur = x; x_cur < x + width; x_cur++) {
            uintptr_t fb_offs = fbcon_calculate_index(x_cur, y_cur);
            fbcon_invert_pixel_with_color_on(fb_offs, console.cursor_color, fb);
        }
    }
}

void fbcon_draw_rect_filled_on(fbcon_px_size x, fbcon_px_size y, fbcon_px_size width, fbcon_px_size height, fbcon_pixel_color color, char* fb) {
    for (fbcon_px_size y_cur = y; y_cur < y + height; y_cur++) {
        for (fbcon_px_size x_cur = x; x_cur < x + width; x_cur++) {
            uintptr_t fb_offs = fbcon_calculate_index(x_cur, y_cur);
            fbcon_draw_pixel_on(fb_offs, color, fb);
        }
    }
}

void fbcon_draw_rect_filled(fbcon_px_size x, fbcon_px_size y, fbcon_px_size width, fbcon_px_size height, fbcon_pixel_color color) {
    for (fbcon_px_size y_cur = y; y_cur < y + height; y_cur++) {
        for (fbcon_px_size x_cur = x; x_cur < x + width; x_cur++) {
            fbcon_draw_pixel(x_cur, y_cur, color);
        }
    }
}

void fbcon_draw_rect_outline(fbcon_px_size x, fbcon_px_size y, fbcon_px_size width, fbcon_px_size height, fbcon_pixel_color color) {
    for (fbcon_px_size y_cur = y; y_cur < y + height; y_cur++) {
        for (fbcon_px_size x_cur = x; x_cur < x + width; x_cur++) {
            if (y_cur == y || x_cur == x || y_cur == y + height - 1 || x_cur == x + width - 1) {
                fbcon_draw_pixel(x_cur, y_cur, color);
            }
        }
    }
}

static fbcon_pixel_color fbcon_interpolate(fbcon_pixel_color color, uint8_t value) {
    int16_t delta_r = (uint16_t) console.color.r - console.back_color.r;
    int16_t delta_g = (uint16_t) console.color.g - console.back_color.g;
    int16_t delta_b = (uint16_t) console.color.b - console.back_color.b;
    int16_t delta_a = (uint16_t) console.color.a - console.back_color.a;
    int16_t delta_val_r = delta_r * ((float) value / UINT8_MAX);
    int16_t delta_val_g = delta_g * ((float) value / UINT8_MAX);
    int16_t delta_val_b = delta_b * ((float) value / UINT8_MAX);
    int16_t delta_val_a = delta_a;
    return (fbcon_pixel_color){
        .r = console.back_color.r + delta_val_r,
        .g = console.back_color.g + delta_val_g,
        .b = console.back_color.b + delta_val_b,
        .a = console.back_color.a + delta_val_a,
    };
}

void fbcon_draw_glyph(fbcon_px_size x_pos, fbcon_px_size y_pos, fbcon_pixel_color color, pixel_font_glyph* glyph) {
    int x_offs = 0;
    uint32_t col_width = fbcon_column_width();
    if (glyph->alignment == PIXEL_FONT_ALIGN_CENTER && glyph->width != col_width) {
        x_offs = ((float) col_width - (float) glyph->width) / 2.0f;
    } else if (glyph->alignment == PIXEL_FONT_ALIGN_RIGHT) {
        x_offs = PIXEL_FONT_GLYPH_WIDTH - glyph->width;
    }
    int y_offs = PIXEL_FONT_LINE_VERTICAL_PADDING_EACH;

    for (int y = 0; y < y_offs; y++) {
        for (int x = 0; x < PIXEL_FONT_COL_WIDTH; x++) {
            fbcon_draw_pixel_scaled(x_pos + x, y_pos + y, console.back_color, PIXEL_FONT_SCALE);
        }
    }
    for (int y = y_offs; y < y_offs + PIXEL_FONT_GLYPH_HEIGHT; y++) {
        for (int x = 0; x < x_offs; x++) {
            fbcon_draw_pixel_scaled(x_pos + x, y_pos + y, console.back_color, PIXEL_FONT_SCALE);
        }
        for (int x = x_offs; x < x_offs + glyph->width; x++) {
            uint8_t pixel_value = glyph->glyph_pixels[(y - y_offs) * glyph->width + x - x_offs];
            fbcon_draw_pixel_scaled(
                x_pos + x, y_pos + y, fbcon_interpolate(color, pixel_value), PIXEL_FONT_SCALE
            );
        }
        for (int x = x_offs + glyph->width; x < PIXEL_FONT_COL_WIDTH; x++) {
            fbcon_draw_pixel_scaled(x_pos + x, y_pos + y, console.back_color, PIXEL_FONT_SCALE);
        }
    }
    for (int y = y_offs + PIXEL_FONT_GLYPH_HEIGHT; y < PIXEL_FONT_ROW_HEIGHT; y++) {
        for (int x = 0; x < PIXEL_FONT_COL_WIDTH; x++) {
            fbcon_draw_pixel_scaled(x_pos + x, y_pos + y, console.back_color, PIXEL_FONT_SCALE);
        }
    }
}

static inline void fbcon_carriage_return() {
    console.col = 0;
}

static inline void fbcon_line_feed() {
    console.row++;
}

static inline void fbcon_new_line() {
    fbcon_carriage_return();
    fbcon_line_feed();
}

void fbcon_scroll() {
    uint32_t row_bytes = fbcon_row_bytes();

    spinlock_acquire(&copy_lock);
    fb_start_index += row_bytes;
    if (fb_start_index > console.framebuffer.size) {
        fb_start_index = 0;
    }

    // Find the index of the last row, taking fb_start_index into account
    int last_row_index;
    if (fb_start_index == 0) {
        last_row_index = console.framebuffer.size - row_bytes;
    } else {
        last_row_index = math_abs((fb_start_index - row_bytes) % console.framebuffer.size);
    }
    memset(
        &console.framebuffer.dfb[last_row_index],
        0,
        row_bytes
    );

    fbcon_flip();
    spinlock_release(&copy_lock);
}

static void fbcon_erase_to_end() {
    fbcon_px_size y_pos = console.row * PIXEL_FONT_LINE_HEIGHT * PIXEL_FONT_SCALE;
    fbcon_px_size y_end = y_pos + PIXEL_FONT_LINE_HEIGHT * PIXEL_FONT_SCALE;
    fbcon_px_size x_start_pos = console.col * PIXEL_FONT_COL_WIDTH * PIXEL_FONT_SCALE;
    fbcon_px_size x_end_pos = console.framebuffer.width;
    for (int y = y_pos; y < y_end; y++) {
        for (int x = x_start_pos; x < x_end_pos; x++) {
            fbcon_draw_pixel(x, y, console.back_color);
        }
    }
}

static inline void fbcon_draw_glyph_at_cursor(pixel_font_glyph* glyph) {
    fbcon_draw_glyph(
        console.col * PIXEL_FONT_COL_WIDTH,
        console.row * fbcon_row_height(),
        console.color,
        glyph
    );
}

static void fbcon_cursor_up() {
    if (console.row > 0) {
        console.row--;
    }
}

static void fbcon_cursor_down() {
    if (console.row < fbcon_max_row() - 1) {
        console.row++;
    }
}

static void fbcon_cursor_left() {
    if (console.col > 0) {
        console.col--;
    }
}

static void fbcon_cursor_right() {
    if (console.col < fbcon_max_col() - 1) {
        console.col++;
    }
}

void fbcon_erase_screen() {
    for (int y = 0; y < console.framebuffer.height; y++) {
        for (int x = 0; x < console.framebuffer.width; x++) {
            fbcon_draw_pixel(x, y, console.back_color);
        }
    }
}

void fbcon_backspace() {
    if (console.col > 0) {
        console.col--;
        fbcon_draw_rect_filled(
            console.col * PIXEL_FONT_COL_WIDTH * PIXEL_FONT_SCALE,
            console.row * PIXEL_FONT_ROW_HEIGHT * PIXEL_FONT_SCALE,
            PIXEL_FONT_COL_WIDTH * PIXEL_FONT_SCALE,
            PIXEL_FONT_ROW_HEIGHT * PIXEL_FONT_SCALE,
            console.back_color
        );
    }
}

void fbcon_write_text(const char* string) {
    // If we don't have a framebuffer, we should just return here
    if (console.framebuffer.dfb == NULL) {
        return;
    }

    spinlock_acquire(&text_lock);

    for (int i = 0; string[i] != 0; i++) {
        const char c = string[i];
        pixel_font_glyph glyph;
        switch (c) {
            case '\r':
                fbcon_carriage_return();
                goto end;
            case '\n':
                fbcon_new_line();
                goto end;
            case '\t': {
                uint8_t spaces_to_add = TAB_SIZE - (console.col % TAB_SIZE);
                if (spaces_to_add == 0) {
                    spaces_to_add = TAB_SIZE;
                }
                pixel_font_get_glyph(&glyph, &pixel_font_default, ' ');
                int target = console.col + spaces_to_add;
                for (; console.col < target; console.col++) {
                    fbcon_draw_glyph_at_cursor(&glyph);
                }
                goto end;
            }
            case '\b': {
                fbcon_backspace();
                goto end;
            }
            case '\033': {
                i++;
                if (string[i] == '[') {
                    i++;
                    switch (string[i]) {
                        case 'A':
                            fbcon_cursor_up();
                            goto end;
                        case 'B':
                            fbcon_cursor_down();
                            goto end;
                        case 'C':
                            fbcon_cursor_right();
                            goto end;
                        case 'D':
                            fbcon_cursor_left();
                            goto end;
                        case 'J':
                            fbcon_erase_screen();
                            goto end;
                        case 'K':
                            fbcon_erase_to_end();
                            goto end;
                    }
                }
            }
            default:
                break;
        }

        int error = pixel_font_get_glyph(&glyph, &pixel_font_default, c);
        if (error) {
            // TODO: handle error
            continue;
        }

        fbcon_draw_glyph_at_cursor(&glyph);

        console.col++;

        end:
        if (console.col >= fbcon_max_col()) {
            fbcon_new_line();
        }
        if (console.row >= fbcon_max_row()) {
            console.row = fbcon_max_row() - 1;
            fbcon_scroll();
        }
    }

    fbcon_flip();

    spinlock_release(&text_lock);
}
