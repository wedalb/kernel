// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <kernel/drivers.h>

#define PCIE_CONFIG_SPACE_SIZE 0x1000

typedef struct {
    void (*init_driver)(kernel_driver *driver, void *extras);
} driver_pcie_driver_iface;

extern driver_pcie_driver_iface driver_pcie_iface;
