// SPDX-License-Identifier: GPL-3.0-or-later
#include <lib/tinystd/mm.h>
#include <lib/tinystd/printf.h>
#include <lib/tinystd/str.h>
#include <lib/tinystd/math.h>

#include <kernel/entry.h>
#include <kernel/drivers.h>
#include <kernel/mm/alloc.h>

#include <drivers/acpi/acpi.h>
#include <drivers/acpi/tables/mcfg.h>

#include <include/spec/hw/acpi/tables/mcfg.h>

#include "pcie.h"
#include "pci.h"

#define MAX_BUSES 1
#define MAX_DEVICES 10
#define MAX_FUNCTIONS 2

static void driver_pcie_init_mcfg_entry(acpi_table_mcfg_entry* mcfg_entry) {
    const size_t bus_count = min(mcfg_entry->end_bus_number - mcfg_entry->start_bus_number, MAX_BUSES);
    const size_t device_count = min(32, MAX_DEVICES);
    const size_t function_count = min(8, MAX_FUNCTIONS);

    // Loop through bus numbers
    for (uint8_t bus = mcfg_entry->start_bus_number; bus <= mcfg_entry->start_bus_number + bus_count; bus++) {
        // Loop through device numbers
        for (uint8_t device = 0; device < device_count; device++) {
            // Loop through function numbers
            for (uint8_t function = 0; function < function_count; function++) {
                // Determine the physical address of the PCIe config space
                // https://wiki.osdev.org/PCI_Express
                // MMIO_Starting_Physical_Address + ((Bus - MMIO_Starting_Bus) << 20 | Device << 15 |Function << 12)
                void *pcie_phys_addr = (void*) (mcfg_entry->base_address +
                    ((bus - mcfg_entry->start_bus_number) << 20 | device << 15 | function << 12));
                // Map the PCIe config space
                pci_device* pci_device = kernel_mm_alloc_mapping(pcie_phys_addr, PCIE_CONFIG_SPACE_SIZE);
                if (!pci_device) {
                    printf("pcie: failed to map PCIe config space (bus = %d, device = %d, function = %d)\n", bus, device, function);
                    continue;
                }

                if (pci_device->vendor_id == 0xFFFF) {
                    // No device found
                    continue;
                }

                printf("pcie: found device with vendor id %x4 and device id %x4 at bus = %d, device = %d, function = %d\n",
                    pci_device->vendor_id, pci_device->device_id, bus, device, function);

                kernel_mm_free(pci_device);
            }
        }
    }
}

void driver_pcie_init(kernel_driver *driver, void *extras) {
    // PCIe depends on ACPI so check if it is ready
    if (!kernel_driver_is_ready(KERNEL_DRIVER_ACPI)) {
        printf("ACPI not available, not initializing PCIe\n");
        return;
    }

    acpi_supported_table* mcfg = acpi_get_supported_table(ACPI_MCFG_SIGNATURE);
    if (!mcfg->is_usable) {
        printf("ACPI: MCFG not available, not initializing PCIe\n");
        return;
    }

    printf("pcie: initializing\n");
    acpi_table_mcfg_interface* mcfg_iface = (acpi_table_mcfg_interface*) mcfg->interface;
    if (!mcfg_iface) {
        printf("pcie: mcfg interface not available, not initializing PCIe\n");
        return;
    }

    mcfg_iface->for_each_mcfg_entry(driver_pcie_init_mcfg_entry);

    driver->is_ready = true;
}

driver_pcie_driver_iface driver_pcie_iface = {
    .init_driver = driver_pcie_init,
};