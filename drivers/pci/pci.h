// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <lib/tinystd/stddef.h>

typedef struct {
    uint16_t vendor_id;
    uint16_t device_id;
    uint16_t command;
    uint16_t status;
    uint8_t revision_id;
    uint8_t prog_if;
    uint8_t subclass;
    uint8_t class_code;
    uint8_t cache_line_size;
    uint8_t latency_timer;
    uint8_t header_type;
    uint8_t bist;
}
__attribute__((packed))
pci_device;
