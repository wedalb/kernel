// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <lib/tinystd/stddef.h>
#include <lib/tinystd/mm.h>

#include "kernel/drivers.h"

typedef struct {
    void (*init_driver)(kernel_driver* driver, void* extras);
    void (*tick)(kernel_driver* driver);
} driver_keyboard_driver_iface;

extern driver_keyboard_driver_iface driver_keyboard_iface;

typedef struct {
    uint8_t keycode;
    uint8_t is_down;
} driver_keyboard_event;

extern driver_keyboard_event last_event;

__attribute__((no_caller_saved_registers))
inline void driver_keyboard_on_event(driver_keyboard_event* event) {
    memcpy(&last_event, event, sizeof(driver_keyboard_event));
}
