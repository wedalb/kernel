// SPDX-License-Identifier: GPL-3.0-or-later

#include <lib/tinystd/printf.h>
#include <lib/tinystd/stddef.h>

#include "kernel/drivers.h"

#include "keyboard.h"

#define DEBUG_PRINT_KEYCODE 0

#define KBD_CODE_BACKSPACE 14
#define KBD_CODE_LSHIFT 42
#define KBD_CODE_RSHIFT 54
#define KBD_CODE_LSHIFT_RELEASE 170
#define KBD_CODE_RSHIFT_RELEASE 182
#define KBD_CODE_CAPSLOCK 58
#define KBD_CODE_CAPSLOCK_RELEASE 186
#define KBD_CODE_ENTER 28
#define KBD_CODE_SPACE 57
#define KBD_CODE_TAB 15
#define KBD_CODE_ARROW_UP 72
#define KBD_CODE_ARROW_DOWN 80
#define KBD_CODE_ARROW_LEFT 75
#define KBD_CODE_ARROW_RIGHT 77

static uint8_t keycodes[] = {
    2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 16, 17, 18, 19, 20, 21, 22,
    23, 24, 25, 26, 27, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41,
    43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 86
};

#define KBD_TABLE_UNSHIFTED 0
#define KBD_TABLE_SHIFTED 1
#define KBD_TABLE_CAPSLOCK 2

// QWERTY layout
char* kbd_qwerty_table[] = {
    "1234567890-=qwertyuiop[]asdfghjkl;'`\\zxcvbnm,./", // Unshifted
    "!@#$%^&*()_+QWERTYUIOP{}ASDFGHJKL:\"~|ZXCVBNM<>?", // Shifted
    "1234567890-=QWERTYUIOP[]ASDFGHJKL;'`\\ZXCVBNM,./", // Caps lock
};

// German layout. Umlaute not supported yet.
char* kbd_german_table[] = {
    "1234567890  qwertzuiop +asdfghjkl   #yxcvbnm,.-<", // Unshifted
    "!\" $%&/()=? QWERTZUIOP *ASDFGHJKL   'YXCVBNM;:_>", // Shifted
    "1234567890  QWERTZUIOP +ASDFGHJKL   #YXCVBNM,.-<", // Caps lock
};

#define KBD_DEFAULT_LAYOUT kbd_german_table

driver_keyboard_event last_event = {0, 0};
static bool is_shift_down = false;
static bool is_caps_enabled = false;

static bool handle_special_keycode(uint8_t keycode, bool is_down) {
    switch (keycode) {
        case KBD_CODE_BACKSPACE: // Backspace
            printf("\b");
            return true;
        case KBD_CODE_ENTER: // Enter
            printf("\n");
            return true;
        case KBD_CODE_SPACE: // Space
            printf(" ");
            return true;
        case KBD_CODE_TAB: // Tab
            printf("\t");
            return true;
        case KBD_CODE_LSHIFT: // Left shift
        case KBD_CODE_RSHIFT: // Right shift
            is_shift_down = true;
            return true;
        case KBD_CODE_LSHIFT_RELEASE: // Left shift release
        case KBD_CODE_RSHIFT_RELEASE: // Right shift release
            is_shift_down = false;
            return true;
        case KBD_CODE_CAPSLOCK: // Caps lock
            if (is_down) {
                is_caps_enabled = !is_caps_enabled;
            }
            return true;
        case KBD_CODE_ARROW_UP: // Arrow up
            printf("\033[A");
            return true;
        case KBD_CODE_ARROW_DOWN: // Arrow down
            printf("\033[B");
            return true;
        case KBD_CODE_ARROW_LEFT: // Arrow left
            printf("\033[D");
            return true;
        case KBD_CODE_ARROW_RIGHT: // Arrow right
            printf("\033[C");
            return true;
        default:
            return false;
    }
}

static char keycode_to_character(uint8_t keycode) {
    // iterate over the keycodes and return the character if the keycode matches
    for (size_t i = 0; i < sizeof(keycodes); i++) {
        if (keycodes[i] == keycode) {
            if (is_shift_down) {
                return KBD_DEFAULT_LAYOUT[KBD_TABLE_SHIFTED][i];
            } else if (is_caps_enabled) {
                return KBD_DEFAULT_LAYOUT[KBD_TABLE_CAPSLOCK][i];
            } else {
                return KBD_DEFAULT_LAYOUT[KBD_TABLE_UNSHIFTED][i];
            }
        }
    }
    return 0;
}

// Driver keyboard init function
void driver_keyboard_init(kernel_driver* driver, void* extras) {
    driver->is_ready = true;
}

void driver_keyboard_handle_event(driver_keyboard_event* event) {
    #if DEBUG_PRINT_KEYCODE
    printf("Keycode: %d\n", event->keycode);
    #endif
    if (!handle_special_keycode(event->keycode, event->is_down) && event->is_down) {
        char character = keycode_to_character(event->keycode);
        if (character != 0) {
            printf("%c", character);
        }
    }
}

void driver_keyboard_tick(kernel_driver* driver) {
    if (last_event.keycode != 0) {
        driver_keyboard_handle_event(&last_event);
        last_event.keycode = 0;
    }
}

driver_keyboard_driver_iface driver_keyboard_iface = {
    .init_driver = driver_keyboard_init,
    .tick = driver_keyboard_tick,
};
