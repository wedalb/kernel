// SPDX-License-Identifier: GPL-3.0-or-later
#include "kernel/drivers.h"
#include "kernel/entry.h"

#include "kernel/mm/alloc.h"
#include "lib/tinystd/mm.h"
#include "lib/tinystd/printf.h"
#include "lib/tinystd/str.h"

#include "include/spec/hw/acpi/acpi.h"

#include "acpi.h"
#include "tables/mcfg.h"
#include "tables/fadt.h"

static acpi_rsdp_descriptor_20 rsdp_descriptor;
static acpi_xsdt* xsdt = NULL;
static acpi_supported_table supported_tables[] = {
    {
        .signature = ACPI_MCFG_SIGNATURE,
        .init = acpi_table_mcfg_init,
    },
    {
        .signature = ACPI_FADT_SIGNATURE,
        .init = acpi_table_fadt_init,
    },
    {
        .init = NULL,
    },
};

#define new_signature(name) char name[ACPI_SDT_SIGNATURE_LEN + 1]
#define new_oem_id(name) char name[ACPI_OEM_ID_LEN + 1]

static bool rsdp_verify_checksum() {
    uint8_t checksum = 0;
    for (int i = 0; i < ACPI_RSDP_CHECKSUM_DATA_LEN; i++) {
        checksum += ((uint8_t*) &rsdp_descriptor)[i];
    }

    return checksum == 0;
}

bool acpi_table_verify_checksum(acpi_table* table) {
    uint8_t checksum = 0;
    for (int i = 0; i < table->header.length; i++) {
        checksum += ((uint8_t*) table)[i];
    }

    return checksum == 0;
}

static bool rsdp_validate() {
    if (!memncmp(rsdp_descriptor.v1.signature, ACPI_RSDP_SIGNATURE, ACPI_RSDP_SIGNATURE_LEN)) {
        printf("ACPI: RSDP signature mismatch!\n");
        return false;
    }

    if (!rsdp_verify_checksum()) {
        printf("ACPI: RSDP checksum failed verification\n");
        return false;
    }

    return true;
}

void acpi_get_oem(char oem[ACPI_OEM_ID_LEN + 1]) {
    oem[ACPI_OEM_ID_LEN] = '\0';
    memcpy(oem, rsdp_descriptor.v1.oem_id, ACPI_OEM_ID_LEN);
}

void acpi_get_sdt_signature(acpi_sdt_header* header, char sig[ACPI_SDT_SIGNATURE_LEN + 1]) {
    sig[ACPI_SDT_SIGNATURE_LEN] = '\0';
    memcpy(sig, header->signature, ACPI_SDT_SIGNATURE_LEN);
}

uint32_t acpi_get_entry_count() {
    return (xsdt->header.length - sizeof(acpi_sdt_header)) / sizeof(uint64_t);
}

acpi_table* acpi_get_table_at(int index) {
    acpi_table* table_phys = *(acpi_table**) ((void*)(xsdt) + sizeof(acpi_sdt_header) + index * sizeof(uint64_t));
    acpi_table* table = kernel_mm_alloc_mapping(table_phys, sizeof(acpi_table));
    size_t table_size = table->header.length;
    kernel_mm_free(table);
    return kernel_mm_alloc_mapping(table_phys, table_size);
}

acpi_table* acpi_get_table(const char signature[ACPI_SDT_SIGNATURE_LEN]) {
    uint32_t entries = acpi_get_entry_count();
    for (int i = 0; i < entries; i++) {
        acpi_table* table = acpi_get_table_at(i);
        if (memncmp(table->header.signature, signature, ACPI_SDT_SIGNATURE_LEN)) {
            return table;
        }
        kernel_mm_free(table);
    }

    return NULL;
}

static bool acpi_init() {
    xsdt = kernel_mm_alloc_mapping((void*) rsdp_descriptor.xsdt_address, sizeof(acpi_xsdt));
    size_t len = xsdt->header.length;
    kernel_mm_free(xsdt);
    xsdt = kernel_mm_alloc_mapping((void*) rsdp_descriptor.xsdt_address, len);

    if (!acpi_table_verify_checksum((acpi_table*) xsdt)) {
        printf("ACPI: XSDT checksum failed verification\n");
        return false;
    }

    uint32_t entries = acpi_get_entry_count();
    for (int i = 0; i < entries; i++) {
        acpi_table* table = acpi_get_table_at(i);

        new_signature(signature);
        acpi_get_sdt_signature(&table->header, signature);
        printf("ACPI table: %s\n", signature);

        // Loop through supported_tables until init is NULL
        for (int table_index = 0; supported_tables[table_index].init; table_index++) {
            if (memncmp(signature, supported_tables[table_index].signature, ACPI_SDT_SIGNATURE_LEN)) {
                printf("ACPI: Initializing table %s\n", signature);
                acpi_supported_table* supported_table = &supported_tables[table_index];
                memcpy(&supported_table->header, &table->header, sizeof(acpi_sdt_header));
                supported_table->table_index = i;
                supported_table->is_usable = supported_table->init(supported_table, &table->header);
                break;
            }
        }

        kernel_mm_free(table);
    }

    return true;
}

void driver_acpi_init(kernel_driver *driver, void *extras) {
    if (!boot_iface->acpi_rsdp) {
        // ACPI not supported
        return;
    }
    acpi_rsdp_descriptor_20 *rsdp_descriptor_ptr =
        (acpi_rsdp_descriptor_20 *)boot_iface->acpi_rsdp;
    if (!rsdp_descriptor_ptr) {
        printf("ACPI: Invalid RSDP descriptor, not initializing ACPI\n");
        return;
    }
    rsdp_descriptor = *rsdp_descriptor_ptr;

    if (!rsdp_validate()) {
        printf("ACPI: Validation failed\n");
        return;
    }
    printf("Initializing ACPI\n");

    new_oem_id(oem);
    acpi_get_oem(oem);
    printf("OEM: %s\n", oem);

    if (!acpi_init()) {
        printf("ACPI initialization failed\n");
        return;
    }

    driver->is_ready = true;

    // Usually when we have ACPI we also have PCIe.
    // Require the PCIe driver now even if we have no devices.
    kernel_driver_require(KERNEL_DRIVER_PCIE, NULL);
}

acpi_supported_table* acpi_get_supported_table(const char signature[ACPI_SDT_SIGNATURE_LEN]) {
    for (int i = 0; supported_tables[i].init; i++) {
        if (memncmp(signature, supported_tables[i].signature, ACPI_SDT_SIGNATURE_LEN)) {
            return &supported_tables[i];
        }
    }

    return NULL;
}

driver_acpi_driver_iface driver_acpi_iface = {
    .init_driver = driver_acpi_init,
};
