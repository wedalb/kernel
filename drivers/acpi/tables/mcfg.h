// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <include/spec/hw/acpi/acpi.h>
#include <include/spec/hw/acpi/tables/mcfg.h>

#include <drivers/acpi/acpi.h>

typedef struct {
    // A function that can be used to iterate over the MCFG entries
    void (*for_each_mcfg_entry)(void (*callback)(acpi_table_mcfg_entry* entry));
} acpi_table_mcfg_interface;

// Initializes the PCIe configuration using the MCFG table.
// This function is called from acpi.c during initialization.
// It returns bool upon success and is passed the acpi_sdt_header pointer.
bool acpi_table_mcfg_init(acpi_supported_table* this_table, acpi_sdt_header* sdt_header);

