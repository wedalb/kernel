// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <include/spec/hw/acpi/acpi.h>
#include <include/spec/hw/acpi/tables/fadt.h>

#include <drivers/acpi/acpi.h>

bool acpi_table_fadt_init(acpi_supported_table* _, acpi_sdt_header* sdt_header);

