// SPDX-License-Identifier: GPL-3.0-or-later
#include <lib/tinystd/stddef.h>
#include <lib/tinystd/printf.h>

#include <drivers/acpi/acpi.h>

#include <include/spec/hw/acpi/acpi.h>
#include <include/spec/hw/acpi/tables/fadt.h>

#include "fadt.h"

// FADT = Fixed ACPI Description Table

const char* acpi_fadt_pm_profile_descriptions[ACPI_FADT_PM_PROFILE_END] = {
    "Unspecified",
    "Desktop",
    "Mobile",
    "Workstation",
    "Enterprise Server",
    "SOHO Server",
    "Appliance PC",
    "Performance Server",
};

static bool acpi_table_fadt_validate(acpi_table_fadt* fadt) {
    // Check if power management profile is valid
    if (fadt->preferred_pm_profile >= ACPI_FADT_PM_PROFILE_END) {
        printf("ACPI: FADT: invalid power management profile: %d\n", fadt->preferred_pm_profile);
        return false;
    }

    // Check if the reset register is valid
    if (fadt->reset_reg.access_size >= ACPI_GAS_ACCESS_SIZE_END) {
        printf("ACPI: FADT: invalid reset register access size: %d\n", fadt->reset_reg.access_size);
        return false;
    }

    return true;
}

bool acpi_table_fadt_init(acpi_supported_table* _, acpi_sdt_header* sdt_header) {
    acpi_table_fadt* fadt = (acpi_table_fadt*) sdt_header;
    if (fadt->header.length < sizeof(acpi_table_fadt)) {
        printf("ACPI: FADT: table too short, maybe ACPI 2.0 is not available\n");
        return false;
    }

    // Verify checksum of the table
    if (!acpi_table_verify_checksum((acpi_table*) fadt)) {
        printf("ACPI: FADT: table checksum invalid\n");
        return false;
    }

    // Validate the table
    if (!acpi_table_fadt_validate(fadt)) {
        printf("ACPI: FADT: table validation failed\n");
        return false;
    }

    printf("ACPI: FADT: Preferred Power Management Profile: %s\n",
        acpi_fadt_pm_profile_descriptions[fadt->preferred_pm_profile]);
    return true;
}
