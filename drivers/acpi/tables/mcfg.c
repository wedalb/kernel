// SPDX-License-Identifier: GPL-3.0-or-later
#include <lib/tinystd/stddef.h>
#include <lib/tinystd/printf.h>
#include <lib/tinystd/mm.h>

#include <include/spec/hw/acpi/tables/mcfg.h>

#include <drivers/acpi/acpi.h>

#include <kernel/mm/alloc.h>

#include "mcfg.h"

// MCFG = PCI Express Memory Mapped Configuration Space Base Address Description Table

static void acpi_table_mcfg_iterate_entries(void (*callback)(acpi_table_mcfg_entry* entry)) {
    acpi_supported_table* supported_mcfg = acpi_get_supported_table(ACPI_MCFG_SIGNATURE);
    if (!supported_mcfg->is_usable) {
        return;
    }

    acpi_table_mcfg* mcfg = (acpi_table_mcfg*) acpi_get_table_at(supported_mcfg->table_index);

    // Determine the number of entries
    size_t num_entries = (mcfg->header.length - sizeof(acpi_table_mcfg)) /
                         sizeof(acpi_table_mcfg_entry);
    printf("ACPI: MCFG: number of entries: %d\n", num_entries);

    // Iterate over the entries
    for (size_t i = 0; i < num_entries; i++) {
        // Determine the address of the entry
        acpi_table_mcfg_entry *cfg =
            (acpi_table_mcfg_entry *)((void *)mcfg + sizeof(acpi_table_mcfg) +
                                      i * sizeof(acpi_table_mcfg_entry));
        printf("ACPI: MCFG: entry %d: base address: %p, segment group: %x, "
               "start bus: %x, end bus: %x\n",
               i, cfg->base_address, cfg->pci_segment_group,
               cfg->start_bus_number, cfg->end_bus_number);
        callback(cfg);
    }

    kernel_mm_free(mcfg);
}

bool acpi_table_mcfg_init(acpi_supported_table* this_table, acpi_sdt_header* sdt_header) {
    acpi_table_mcfg* mcfg = (acpi_table_mcfg*) sdt_header;
    if (mcfg->header.length < sizeof(acpi_table_mcfg)) {
        printf("ACPI: MCFG: table too short\n");
        return false;
    }

    // Verify checksum of the table
    if (!acpi_table_verify_checksum((acpi_table*) mcfg)) {
        printf("ACPI: MCFG: table checksum invalid\n");
        return false;
    }

    // Check if the MCFG has any entries
    if (mcfg->header.length == sizeof(acpi_table_mcfg)) {
        printf("ACPI: MCFG: table has no entries\n");
        return false;
    }

    this_table->interface = kernel_mm_alloc(sizeof(acpi_table_mcfg_interface));
    if (!this_table->interface) {
        printf("ACPI: MCFG: failed to allocate interface\n");
        return false;
    }

    acpi_table_mcfg_interface interface = {
        .for_each_mcfg_entry = acpi_table_mcfg_iterate_entries,
    };
    memcpy(this_table->interface, &interface, sizeof(acpi_table_mcfg_interface));

    return true;
}
