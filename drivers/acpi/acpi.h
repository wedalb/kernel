// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <include/spec/hw/acpi/acpi.h>

#include <kernel/drivers.h>

#include "include/spec/hw/acpi/tables/mcfg.h"

typedef struct {
    void (*init_driver)(kernel_driver* driver, void* extras);
} driver_acpi_driver_iface;

extern driver_acpi_driver_iface driver_acpi_iface;

typedef struct acpi_supported_table_t {
    char* signature;
    bool is_usable;
    void* interface;
    int table_index;
    acpi_sdt_header header;
    // Function that uses the acpi_sdt_header to parse the table
    // and returns true if initialization was successful
    bool (*init)(struct acpi_supported_table_t* this_table, acpi_sdt_header *header);
} acpi_supported_table;

bool acpi_table_verify_checksum(acpi_table* header);
acpi_supported_table* acpi_get_supported_table(const char* signature);

// Returns a pointer to the ACPI table. Must be freed after use.
acpi_table* acpi_get_table_at(int index);
