// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <lib/tinystd/stddef.h>

#include <include/spec/hw/acpi/acpi.h>

#define ACPI_MCFG_SIGNATURE "MCFG"

typedef struct {
    acpi_sdt_header header;
    uint64_t _;
    // Configuration is located here and there can be multiple of them.
    // It will be iterated over in the code.
}
__attribute__ ((packed))
acpi_table_mcfg;

typedef struct {
    uint64_t base_address;
    uint16_t pci_segment_group;
    uint8_t start_bus_number;
    uint8_t end_bus_number;
    uint32_t _;
}
__attribute__((packed))
acpi_table_mcfg_entry;
