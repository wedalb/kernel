// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <lib/tinystd/stddef.h>

#include <include/spec/hw/acpi/acpi.h>

#define ACPI_FADT_SIGNATURE "FACP"

/*
 * Preferred power management profile
 */
#define ACPI_FADT_PM_PROFILE_UNSPECIFIED 0
#define ACPI_FADT_PM_PROFILE_DESKTOP 1
#define ACPI_FADT_PM_PROFILE_MOBILE 2
#define ACPI_FADT_PM_PROFILE_WORKSTATION 3
#define ACPI_FADT_PM_PROFILE_ENTERPRISE_SERVER 4
#define ACPI_FADT_PM_PROFILE_SOHO_SERVER 5
#define ACPI_FADT_PM_PROFILE_APPLIANCE_PC 6
#define ACPI_FADT_PM_PROFILE_PERFORMANCE_SERVER 7
#define ACPI_FADT_PM_PROFILE_END 8
// >7 reserved


typedef struct {
    acpi_sdt_header header;

    uint32_t firmware_ctrl;
    uint32_t dsdt;

    // field was used in ACPI 1.0 and is no longer in use
    uint8_t reserved;

    // Preferred power management profile
    uint8_t preferred_pm_profile;
    uint16_t sci_interrupt;
    uint32_t smi_command_port;
    uint8_t acpi_enable;
    uint8_t acpi_disable;
    uint8_t s4bios_req;
    uint8_t pstate_control;
    uint32_t pm1a_event_blk;
    uint32_t pm1b_event_blk;
    uint32_t pm1a_control_blk;
    uint32_t pm1b_control_blk;
    uint32_t pm2_control_blk;
    uint32_t pm_timer_blk;
    uint32_t gpe0_blk;
    uint32_t gpe1_blk;
    uint8_t pm1_event_len;
    uint8_t pm1_control_len;
    uint8_t pm2_control_len;
    uint8_t pm_timer_len;
    uint8_t gpe0_len;
    uint8_t gpe1_len;
    uint8_t gpe1_base;
    uint8_t cstate_control;
    // worst case hardware latency to enter/exit C2 state
    uint16_t worst_c2_latency;
    // worst case hardware latency to enter/exit C3 state
    uint16_t worts_c3_latency;
    // flush size of processor's memory cache
    uint16_t flush_size;
    // flush stride of processor's memory cache
    uint16_t flush_stride;
    // duty cycle offset in processor's P_CNT register
    uint8_t duty_offset;
    // duty cycle width in processor's P_CNT register
    uint8_t duty_width;
    // day-alarm index in RTC CMOS
    uint8_t day_alarm;
    // month-alarm index in RTC CMOS
    uint8_t month_alarm;
    // century index in RTC CMOS
    uint8_t century;

    uint16_t boot_architecture_flags;

    uint8_t reserved2;
    uint32_t flags;

    acpi_gas reset_reg;

    uint8_t reset_value;
    uint8_t reserved3[3];

    // 64-bit pointers, only valid if ACPI 2.0+ is supported
    uint64_t x_firmware_ctrl;
    uint64_t x_dsdt;

    acpi_gas x_pm1a_event_blk;
    acpi_gas x_pm1b_event_blk;
    acpi_gas x_pm1a_control_blk;
    acpi_gas x_pm1b_control_blk;
    acpi_gas x_pm2_control_blk;
    acpi_gas x_pm_timer_blk;
    acpi_gas x_gpe0_blk;
    acpi_gas x_gpe1_blk;
}
__attribute__ ((packed))
acpi_table_fadt;

