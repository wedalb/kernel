// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stdint.h>

#define ACPI_VERSION 20
#define ACPI_OEM_ID_LEN 6
#define ACPI_RSDP_SIGNATURE_LEN 8
#define ACPI_SDT_SIGNATURE_LEN 4
#define ACPI_OEM_TABLE_ID_LEN 8

// https://www.daimongu.com/2018/03/13/how-to-calculate-the-checksum-of-the-rsdp-table/
#define ACPI_RSDP_CHECKSUM_DATA_LEN 20

/*
 * ACPI Generic Address Structure (GAS) AddressSpace
 */

#define ACPI_GAS_ADDRESS_SPACE_SYSTEM_MEMORY 0
#define ACPI_GAS_ADDRESS_SPACE_SYSTEM_IO 1
#define ACPI_GAS_ADDRESS_SPACE_PCI_CONFIG_SPACE 2
#define ACPI_GAS_ADDRESS_SPACE_EMBEDDED_CONTROLLER 3
#define ACPI_GAS_ADDRESS_SPACE_SMBUS 4
#define ACPI_GAS_ADDRESS_SPACE_SYSTEM_CMOS 5
#define ACPI_GAS_ADDRESS_SPACE_PCI_BAR_TARGET 6
#define ACPI_GAS_ADDRESS_SPACE_IPMI 7
#define ACPI_GAS_ADDRESS_SPACE_GENERAL_PURPOSE_IO 8
#define ACPI_GAS_ADDRESS_SPACE_GENERIC_SERIAL_BUS 9
#define ACPI_GAS_ADDRESS_SPACE_PLATFORM_COMMUNICATION_CHANNEL 0x0A
// 0x0B-0x7F reserved
// 0x80-0xFF OEM defined

/*
 * ACPI Generic Address Structure (GAS) AccessSize
 */
#define ACPI_GAS_ACCESS_SIZE_UNDEFINED 0 // legacy reasons
#define ACPI_GAS_ACCESS_SIZE_BYTE 1
#define ACPI_GAS_ACCESS_SIZE_16BIT 2
#define ACPI_GAS_ACCESS_SIZE_32BIT 3
#define ACPI_GAS_ACCESS_SIZE_64BIT 4
#define ACPI_GAS_ACCESS_SIZE_WORD ACPI_GAS_ACCESS_SIZE_16BIT
#define ACPI_GAS_ACCESS_SIZE_DWORD ACPI_GAS_ACCESS_SIZE_32BIT
#define ACPI_GAS_ACCESS_SIZE_QWORD ACPI_GAS_ACCESS_SIZE_64BIT
#define ACPI_GAS_ACCESS_SIZE_END 5

extern const char ACPI_RSDP_SIGNATURE[];

typedef struct {
    char signature[ACPI_RSDP_SIGNATURE_LEN];
    uint8_t checksum;
    char oem_id[ACPI_OEM_ID_LEN];
    uint8_t revision;
    uint32_t rsdt_addr;
}
__attribute__((packed))
acpi_rsdp_descriptor_10;

typedef struct {
    acpi_rsdp_descriptor_10 v1;

    uint32_t length;
    uint64_t xsdt_address;
    uint8_t extended_checksum;
    uint8_t reserved[3];
}
__attribute__((packed))
acpi_rsdp_descriptor_20;

typedef struct {
    char signature[ACPI_SDT_SIGNATURE_LEN];
    uint32_t length;
    uint8_t revision;
    uint8_t checksum;
    char oem_id[ACPI_OEM_ID_LEN];
    char oem_table_id[ACPI_OEM_TABLE_ID_LEN];
    uint32_t oem_revision;
    uint32_t creator_id;
    uint32_t creator_revision;
}
__attribute__((packed))
acpi_sdt_header;

typedef struct {
    acpi_sdt_header header;
}
__attribute__((packed))
acpi_table;

/**
 * XSDT - Extended System Description Table
 */
typedef struct {
    acpi_sdt_header header;
    // After the header are a variable number of entries
    acpi_table* tables;
}
__attribute__((packed))
acpi_xsdt;

/**
 * Generic Address Structure used throughout ACPI
 */
typedef struct {
    uint8_t space_id;
    uint8_t bit_width;
    uint8_t bit_offset;
    uint8_t access_size;
    uint64_t address;
}
__attribute__((packed))
acpi_gas;

