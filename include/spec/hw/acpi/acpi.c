// SPDX-License-Identifier: GPL-3.0-or-later
#include "acpi.h"

const char ACPI_RSDP_SIGNATURE[ACPI_RSDP_SIGNATURE_LEN] = "RSD PTR ";
