// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stdbool.h>

static const int endianness_n = 1;

static inline bool endianness_is_big() {
    return *(char *)&endianness_n == 0;
}

static inline bool endianness_is_little() {
    return *(char *)&endianness_n == 1;
}

