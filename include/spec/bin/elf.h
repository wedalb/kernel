// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <stdint.h>
#include <stdbool.h>
#include "include/internal/bits/wordsize.h"

// https://en.wikipedia.org/wiki/Executable_and_Linkable_Format

#if __WORDSIZE == 64
#define IS_64_BIT 1
#else
#define IS_64_BIT 0
#endif

//    1 2 3 4
// 0x7f E L F
#define ELF_HEADER_MAGIC_LEN 4

#define ELF_CLASS_32 1
#define ELF_CLASS_64 2

#define ELF_ENDIANNESS_LITTLE 1
#define ELF_ENDIANNESS_BIG 2

#define ELF_VERSION 1

// We're skipping irrelevant ones
#define ELF_ABI_SYSTEM_V 0x00
#define ELF_ABI_LINUX 0x03
#define ELF_ABI_GNU_HURD 0x04
#define ELF_ABI_FREEBSD 0x09
#define ELF_ABI_OPENBSD 0x0C

#define ELF_HEADER_INNER_PADDING 7

#define ELF_TYPE_NONE 0x00
#define ELF_TYPE_REL 0x01
#define ELF_TYPE_EXEC 0x02
#define ELF_TYPE_DYN 0x03
#define ELF_TYPE_CORE 0x04
#define ELF_TYPE_LOOS 0xFE00
#define ELF_TYPE_HIOS 0x0FEFF;
#define ELF_TYPE_LOPROC 0xFF00;
#define ELF_TYPE_HIPROC 0xFFFF;

// Again, we're skipping irrelevant ones
#define ELF_MACHINE_NO_SPECIFIC 0x00
#define ELF_MACHINE_X86 0x03
#define ELF_MACHINE_ARM 0x28
#define ELF_MACHINE_AMD64 0x3E
#define ELF_MACHINE_RISCV 0xF3

typedef uint8_t elf_class;
typedef uint8_t elf_data_endianness;
typedef uint8_t elf_version;
typedef uint8_t elf_abi;
typedef uint8_t elf_abi_version;
typedef uint16_t elf_type;
typedef uint16_t elf_machine;
typedef uint32_t elf_version_2;
#if IS_64_BIT
typedef uint64_t elf_entry;
typedef uint64_t elf_program_header_offset;
typedef uint64_t elf_section_header_offset;
#else
typedef uint32_t elf_entry;
typedef uint32_t elf_program_header_offset;
typedef uint32_t elf_section_header_offset;
#endif
typedef uint32_t elf_flags;
typedef uint16_t elf_header_size;
typedef uint16_t elf_program_header_table_entry_size;
typedef uint16_t elf_program_header_table_num_entries;
typedef uint16_t elf_section_header_table_entry_size;
typedef uint16_t elf_section_header_table_num_entries;
typedef uint16_t elf_section_header_table_entry_index_section_names;

typedef struct {
    char magic[ELF_HEADER_MAGIC_LEN];
    elf_class class;
    elf_data_endianness endianness;
    elf_version version;
    elf_abi abi;
    elf_abi_version abi_version;
    char reserved[ELF_HEADER_INNER_PADDING];
    elf_type type;
    elf_machine machine;
    elf_version_2 version2;

    elf_entry entry;
    elf_program_header_offset phoff;
    elf_section_header_offset shoff;

    elf_flags flags;
    elf_header_size header_size;
    elf_program_header_table_entry_size phentsize;
    elf_program_header_table_num_entries phnum;
    elf_section_header_table_entry_size shentsize;
    elf_section_header_table_num_entries shnum;
    elf_section_header_table_entry_index_section_names shstrndx;
}
__attribute__((packed))
elf_header;

typedef uint32_t elf_program_header__type;
typedef uint32_t elf_program_header__flags_int;
typedef struct {
    bool x : 1;
    bool w : 1;
    bool r : 1;
}
__attribute__((packed))
elf_program_header__flags_struct;
typedef union {
    elf_program_header__flags_struct flags;
    elf_program_header__flags_int n;
}
__attribute__((packed))
elf_program_header__flags;
#if IS_64_BIT
typedef uint64_t elf_program_header__offset;
typedef uint64_t elf_program_header__virtual_addr;
typedef uint64_t elf_program_header__physical_addr;
typedef uint64_t elf_program_header__filesz;
typedef uint64_t elf_program_header__memsz;
typedef uint64_t elf_program_header__align;
#else
typedef uint32_t elf_program_header__offset;
typedef uint32_t elf_program_header__virtual_addr;
typedef uint32_t elf_program_header__physical_addr;
typedef uint32_t elf_program_header__filesz;
typedef uint32_t elf_program_header__memsz;
typedef uint32_t elf_program_header__align;
#endif

#define ELF_PH_TYPE_NULL          0x00
#define ELF_PH_TYPE_LOAD          0x01
#define ELF_PH_TYPE_DYNAMIC       0x02
#define ELF_PH_TYPE_INTERP        0x03
#define ELF_PH_TYPE_NOTE          0x04
#define ELF_PH_TYPE_SHLIB         0x05
#define ELF_PH_TYPE_PHDR          0x06
#define ELF_PH_TYPE_TLS           0x07
#define ELF_PH_TYPE_LOOS    0x60000000
#define ELF_PH_TYPE_HIOS    0x6FFFFFFF
#define ELF_PH_TYPE_LOPROC  0x70000000
#define ELF_PH_TYPE_HIPROC  0x7FFFFFFF

typedef struct {
    elf_program_header__type type;
#if IS_64_BIT
    elf_program_header__flags flags;
#endif
    elf_program_header__offset offset;
    elf_program_header__virtual_addr vaddr;
    elf_program_header__physical_addr paddr;
    elf_program_header__filesz filesz;
    elf_program_header__memsz memsz;
#if !IS_64_BIT
    elf_program_header__flags flags;
#endif
    elf_program_header__align align;
}
__attribute__((packed))
elf_program_header;

#define elf_header_magic_matches(mag) ( \
    mag[0] == 0x7F && \
    mag[1] == 'E' && \
    mag[2] == 'L' && \
    mag[3] == 'F' \
)

