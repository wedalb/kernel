// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

typedef struct {
    void* virt;
    void* phys;
} mem_address_pair;