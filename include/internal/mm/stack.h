// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stddef.h>

void* stack_pointer();
void stack_set_pointer(void* addr);