// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stddef.h>

void paging_map_page_direct(void* phys_addr, void* virt_addr);
void paging_map_identity_page_direct(void *phys_addr);
void paging_map_range_direct(void* phys_start, void* virt_start, size_t len);