// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stdint.h>

void* memory_higher_half_base();
