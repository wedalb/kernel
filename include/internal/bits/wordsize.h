// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#if defined __x86_64__ && !defined __ILP32__
#define __WORDSIZE 64
#else
#define __WORDSIZE 32
#endif
