// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#define RING_LEVEL_KERNEL 0
// These two could be used for drivers but
// no one really wants or uses them so we'll just
// keep them here as level 1 and 2
#define RING_LEVEL_1 1
#define RING_LEVEL_2 2
#define RING_LEVEL_USERSPACE 3
