// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

typedef enum {
    // Red Green Blue, 8 bpp, total 24
    RGB_24,
    // Red Green Blue Reserved, 8 bpp, total 32
    RGBR_32,
    // Red Green Blue Alpha, 8 bpp, total 32
    RGBA_32,
    // Blue Green Red, 8 bpp, total 24
    BGR_24,
    // Blue Green Red Reserved, 8 bpp, total 32
    BGRR_32,
    // Blue Green Red Alpha, 8 bpp, total 32
    BGRA_32,
    // Alpha Blue Green Red, 8 bpp, total 32
    ABGR_32,
    // Alpha Red Green Blue, 8 bpp, total 32
    ARGB_32,
    // Brightness/Value only, 8 bpp, total 8
    VALUE_8,
    // Brightness/Value only x3, 8 bpp, total 24
    VALUE_24,
    // Brightness/Value only x4, 8 bpp, total 24
    VALUE_32,
} gfx_pixel_color_format;

