// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stdbool.h>

#include "lib/tinystd/sync/spinlock.h"

typedef struct kernel_driver_st {
    void* driver;
    int driver_id;
    bool is_ready;
    bool needs_extras;
    spinlock lock;

    void (*init)(struct kernel_driver_st* driver, void* extras);
    void (*tick)(struct kernel_driver_st* driver);
} kernel_driver;

#define KERNEL_DRIVER_EFIFB 1
#define KERNEL_DRIVER_ACPI 2
#define KERNEL_DRIVER_KEYBOARD 3
#define KERNEL_DRIVER_PCIE 4

#define KERNEL_DRIVER_SUCCESS 0
#define KERNEL_DRIVER_NO_EXTRAS 0x100

extern kernel_driver kernel_drivers[];
extern int kernel_driver_last_ix;

int kernel_driver_require(int driver_id, void* extras);
kernel_driver* kernel_driver_get(int driver_id);
bool kernel_driver_is_ready(int driver_id);
void kernel_define_drivers();

