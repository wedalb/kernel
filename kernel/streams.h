// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

typedef void (*pipe_stream_default_out_write_text_func)(const char* data);

void pipe_stream_set_default_out_write_text(pipe_stream_default_out_write_text_func fn);
