// SPDX-License-Identifier: GPL-3.0-or-later
#include <stddef.h>

#include "image.h"

size_t kernel_image_size() {
    return &kernel_image_end - &kernel_image_start;
}