// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stdint.h>
#include <stddef.h>

extern uintptr_t kernel_image_start;
extern uintptr_t kernel_image_end;

size_t kernel_image_size();
