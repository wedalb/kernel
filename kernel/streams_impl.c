// SPDX-License-Identifier: GPL-3.0-or-later
#include "drivers/video/fb/console.h"
#include "lib/tinystd/streams.h"

#include "boot_iface.h"
#include "boot_vars.h"
#include "streams.h"

static pipe_stream_default_out_write_text_func pipe_stream_default_out_write_text = NULL;

STD_INTERFACE
void pipe_stream_write(uint8_t stream __attribute__((unused)), const char *data) {
    if (pipe_stream_default_out_write_text) {
        pipe_stream_default_out_write_text(data);
    }
}

void pipe_stream_set_default_out_write_text(pipe_stream_default_out_write_text_func fn) {
    pipe_stream_default_out_write_text = fn;
}