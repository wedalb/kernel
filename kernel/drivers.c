// SPDX-License-Identifier: GPL-3.0-or-later
#include "drivers.h"

#include "drivers/acpi/acpi.h"
#include "drivers/keyboard/keyboard.h"
#include "drivers/pci/pcie.h"
#include "drivers/video/fb/efifb.h"
#include "kernel/boot_iface.h"
#include "lib/tinystd/printf.h"

// For now we'll add the drivers here but later we'll
// need to do it dynamically
#define KERNEL_DRIVER_COUNT 16
kernel_driver kernel_drivers[KERNEL_DRIVER_COUNT];
int kernel_driver_last_ix = 0;

int kernel_driver_require(int driver_id, void* extras) {
    for (int i = 0; i < KERNEL_DRIVER_COUNT; i++) {
        if (kernel_drivers[i].driver_id == driver_id) {
            spinlock_acquire(&kernel_drivers[i].lock);
            if (!kernel_drivers[i].is_ready) {
                if (kernel_drivers[i].needs_extras && extras == NULL) {
                    printf("Cannot initialize driver %d: requires extras but none were given\n", driver_id);
                    spinlock_release(&kernel_drivers[i].lock);
                    return KERNEL_DRIVER_NO_EXTRAS;
                }
                printf("Initializing driver %d\n", driver_id);
                kernel_drivers[i].init(&kernel_drivers[i], extras);
            }
            spinlock_release(&kernel_drivers[i].lock);
        }
    }
    return KERNEL_DRIVER_SUCCESS;
}

kernel_driver* kernel_driver_get(int driver_id) {
    for (int i = 0; i < KERNEL_DRIVER_COUNT; i++) {
        if (kernel_drivers[i].driver_id == driver_id) {
            spinlock_wait(kernel_drivers[i].lock);
            return &kernel_drivers[i];
        }
    }
    return NULL;
}

bool kernel_driver_is_ready(int driver_id) {
    for (int i = 0; i < KERNEL_DRIVER_COUNT; i++) {
        if (kernel_drivers[i].driver_id == driver_id) {
            return kernel_drivers[i].is_ready;
        }
    }
    return false;
}

void kernel_define_drivers() {
    kernel_drivers[kernel_driver_last_ix++] = (kernel_driver){
        .driver = &driver_efifb_iface,
        .driver_id = KERNEL_DRIVER_EFIFB,
        .init = driver_efifb_iface.init_driver,
        .needs_extras = true,
    };

    kernel_drivers[kernel_driver_last_ix++] = (kernel_driver){
        .driver = &driver_acpi_iface,
        .driver_id = KERNEL_DRIVER_ACPI,
        .init = driver_acpi_iface.init_driver,
    };

    kernel_drivers[kernel_driver_last_ix++] = (kernel_driver){
        .driver = &driver_keyboard_iface,
        .driver_id = KERNEL_DRIVER_KEYBOARD,
        .init = driver_keyboard_iface.init_driver,
        .tick = driver_keyboard_iface.tick,
    };

    kernel_drivers[kernel_driver_last_ix++] = (kernel_driver){
        .driver = &driver_pcie_iface,
        .driver_id = KERNEL_DRIVER_PCIE,
        .init = driver_pcie_iface.init_driver,
    };
}