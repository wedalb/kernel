// SPDX-License-Identifier: GPL-3.0-or-later

#include "boot_iface.h"
#include "drivers.h"

static void kernel_event_tick() {
    // Loop through the drivers and call their tick functions if they have them
    for (int i = 0; i <= kernel_driver_last_ix; i++) {
        kernel_driver* driver = &kernel_drivers[i];
        if (driver->tick != NULL) {
            driver->tick(driver);
        }
    }
}

void kernel_run_event_loop() {
    for (;;) {
        // Wait for an interrupt
        boot_arch_asm_hlt();
        // Run a tick
        kernel_event_tick();
    }
}

