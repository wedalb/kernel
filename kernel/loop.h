// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

void kernel_run_event_loop();
