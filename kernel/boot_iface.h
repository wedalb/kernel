// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include <drivers/video/fb/efifb.h>

#include <kernel/mm/mem_iface.h>

#ifndef BOOT_INTERFACE
#define BOOT_INTERFACE __attribute__((sysv_abi))
#endif

/**
 * This is used to call into implementation-specific
 * functions. The underlying loader passes an interface
 * to us with functions we can call.
 */
typedef struct {
    // Stack pointer before the kernel was booted
    void* stack_pointer_before_kernel;
    // ACPI RSDP pointer, set to NULL if not available
    void* acpi_rsdp;
    // For now, we will pass the efifb handoff structure here
    // but this will be replaced with a more generic display
    // interface in the future
    driver_efifb_handoff* efifb_handoff;
    // Memory info
    kernel_meminfo* meminfo;
} boot_interface;

void boot_arch_memory_management_fill_info(kernel_meminfo* info);
void boot_arch_prepare_display();
void boot_arch_prepare_memory();
void boot_arch_prepare_interrupts();
void boot_arch_remap_stack();
void boot_arch_full_halt();
void boot_arch_asm_hlt();
void serial_output();
void serial_init();
