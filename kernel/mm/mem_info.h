// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stddef.h>

/*
 * All this information is provided to us via the memory interface.
 * We do not have control over what is put into these values as it is
 * implementation-specific and can be different on every architecture
 * (and sometimes even just between BIOSes)
 */

typedef struct {
    void* start;
    size_t size_bytes;
} kernel_meminfo_area;

typedef struct {
    // Usable memory areas
    kernel_meminfo_area* mem_areas;
    int mem_area_count;
    // All memory areas
    kernel_meminfo_area* all_mem_areas;
    int all_mem_area_count;

    size_t page_size;
} kernel_meminfo;

extern kernel_meminfo kernel_mem_info;

size_t kernel_meminfo_phys_total_usable();
