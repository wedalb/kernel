// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stddef.h>
#include <stdbool.h>

#ifndef KERNEL_MEMORY_PAGE_SIZE
#define KERNEL_MEMORY_PAGE_SIZE 0x2000
#endif

typedef struct kernel_mem_allocation_st {
    size_t size;
    void* start;
    void* phys;
    struct kernel_mem_allocation_st* next;
    struct kernel_mem_allocation_st* prev;
    bool free : 1;
    bool reusable : 1;
} kernel_mem_allocation;

void kernel_mm_alloc_init();
void kernel_mm_alloc_pre_init();

void* kernel_mm_alloc(size_t size);
void* kernel_mm_alloc_at(size_t size, void* virt_addr);
void* kernel_mm_alloc_mapping(void* phys_addr, size_t size);
void* kernel_mm_alloc_page_phys();
void* kernel_mm_alloc_last_page();
size_t kernel_mm_free_memory_bytes();
size_t kernel_mm_used_memory_bytes();
void kernel_mm_free(void* ptr);
