// SPDX-License-Identifier: GPL-3.0-or-later
#include <stddef.h>

#include "lib/tinystd/printf.h"

#include "mem_info.h"

kernel_meminfo kernel_mem_info = {0};

size_t kernel_meminfo_phys_total_usable() {
    size_t total_usable = 0;
    for (int i = 0; i < kernel_mem_info.mem_area_count; i++) {
        total_usable += kernel_mem_info.mem_areas[i].size_bytes;
    }
    return total_usable;
}