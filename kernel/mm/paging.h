// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include <stdint.h>
#include <stddef.h>

int paging_add_skip_range(void* start, void* exclusive_end);
void paging_remove_skip_range(int skip_range_id);
void paging_map_range(void* phys_start, void* virt_start, size_t len);
void paging_map_addr_range(void* phys_start, void* phys_exclusive_end, void* virt_start);
void paging_map_identity_page(void *phys_addr);
void paging_map_identity_range(void *phys_addr, size_t len);
void paging_map_page(void* phys_addr, void* virt_addr);
void paging_unmap_page(void* virt_addr);
void paging_unmap_range(void* virt_start, size_t len);
void paging_unmap_addr_range(void* virt_start, void* virt_exclusive_end);
void paging_set_page_allocation_border(void* border);
uintptr_t paging_physical_addr_from_virt(void* addr);
