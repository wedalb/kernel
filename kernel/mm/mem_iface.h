// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once
#include "mem_info.h"

#define MEMORY_INTERFACE __attribute__((sysv_abi))

typedef struct {
    MEMORY_INTERFACE void (*memory_management_initialize)(kernel_meminfo* meminfo);

    void* stack_pointer_before_kernel;
} boot_memory_interface;

void arch_memory_management_initialize(kernel_meminfo* meminfo);
void* arch_memory_alloc_base();
void arch_memory_freeze_setup_paging();
