// SPDX-License-Identifier: GPL-3.0-or-later

#include "kernel/boot_iface.h"
#include "kernel/image.h"
#include "kernel/mm/mem_iface.h"
#include "kernel/mm/mem_info.h"
#include "kernel/mm/paging.h"
#include "lib/tinystd/mm.h"
#include "lib/tinystd/printf.h"
#include "lib/tinystd/math.h"

#include "include/internal/mm/addr.h"
#include "include/internal/mm/bases.h"
#include "include/internal/mm/paging.h"

#include "alloc.h"
#include "lib/tinystd/sync/spinlock.h"

static void* pages_base = NULL;
static kernel_meminfo_area* pages_base_area = NULL;
static int pages_base_area_index = -1;
#define page_size (kernel_mem_info.page_size)

static void* last_page = NULL;
static void* next_virt_page = NULL;

static kernel_mem_allocation* first = NULL;
static kernel_mem_allocation* last = NULL;

static kernel_mem_allocation* first_free = NULL;
static kernel_mem_allocation* last_free = NULL;

static size_t total_usable_bytes = 0;

static spinlock fresh_page_lock;
static spinlock alloc_fresh_page_lock;
static spinlock alloc_node_lock;
static spinlock next_virt_addr_lock;

void kernel_mm_alloc_init() {
    printf("End of kernel image: %p\n", &kernel_image_end);
}

static size_t total_usable() {
    size_t usable = pages_base - pages_base_area->start;
    for (int i = pages_base_area_index + 1; i < kernel_mem_info.mem_area_count; i++) {
        kernel_meminfo_area area = kernel_mem_info.mem_areas[i];
        usable += area.size_bytes;
    }
    return usable;
}

static kernel_meminfo_area* find_mem_area_containing(void* addr) {
    kernel_meminfo_area* last_area = NULL;
    for (int i = 0; kernel_mem_info.mem_area_count; i++) {
        kernel_meminfo_area* area = &kernel_mem_info.mem_areas[i];
        if (addr < area->start) {
            return last_area;
        }
        last_area = area;
    }
    return NULL;
}

static int find_mem_area_index_containing(void* addr) {
    for (int i = 0; kernel_mem_info.mem_area_count; i++) {
        kernel_meminfo_area* area = &kernel_mem_info.mem_areas[i];
        if (addr < area->start) {
            return i;
        }
    }
    return -1;
}

void kernel_mm_alloc_pre_init() {
    pages_base = arch_memory_alloc_base();
    last_page = pages_base - page_size;
    pages_base_area = find_mem_area_containing(pages_base);
    pages_base_area_index = find_mem_area_index_containing(pages_base);
    next_virt_page = memory_higher_half_base();
    total_usable_bytes = total_usable();
}

static void* find_closest_available_page_to(void* target_page) {
    void* closest_page = 0;
    void* last_area_start = 0;
    size_t last_area_size = 0;

    for (int i = 0; kernel_mem_info.mem_area_count; i++) {
        kernel_meminfo_area area = kernel_mem_info.mem_areas[i];
        if (target_page < area.start) {
            // So now we've surpassed the area that COULD hold our
            // target page. Let's check whether it's actually in bounds.
            // If not, we'll use this one.
            if (last_area_start == 0) {
                // There isn't any last area. So we have to use this one.
                // We'll take up the very beginning of this area.
                // The next alloc won't run into this.
                closest_page = area.start;
                break;
            }
            if (target_page >= last_area_start && target_page + page_size < last_area_start + last_area_size) {
                // Yes! We can use this as-is.
                closest_page = target_page;
            } else {
                // Oh no! It doesn't fit. Let's use the new one then.
                closest_page = area.start;
            }

            break;
        }

        last_area_start = area.start;
        last_area_size = area.size_bytes;
    }

    return (void*) closest_page;
}

static void* next_fresh_page() {
    spinlock_guard(fresh_page_lock,
        void* closest_page = find_closest_available_page_to(last_page + page_size);
        last_page = closest_page;
    )
    return closest_page;
}

static mem_address_pair alloc_page_at(void* virt_addr, void* phys_addr) {
    if (kernel_mm_free_memory_bytes() < page_size) {
        printf("Out of memory (free: %zu, in use: %zu)\n", kernel_mm_free_memory_bytes(), kernel_mm_used_memory_bytes());
        boot_arch_full_halt();
    }

    paging_map_page_direct(phys_addr, virt_addr);
    return (mem_address_pair){
        .virt = virt_addr,
        .phys = phys_addr,
    };
}

static void* allocate_next_virt_addr() {
    spinlock_guard(next_virt_addr_lock,
        void* virt_addr = next_virt_page;
        next_virt_page += page_size;
    )
    return virt_addr;
}

static mem_address_pair alloc_next_fresh_page() {
    if (!next_virt_page) {
        printf("OOPS! next_virt_page == 0\n");
        boot_arch_full_halt();
    }
    spinlock_acquire(&alloc_fresh_page_lock);

    mem_address_pair page;

    kernel_mem_allocation* next = first_free;
    while (next && !next->reusable) {
        next = next->next;
    }

    if (next) {
        first_free->size -= page_size;
        if (first_free->size < page_size) {
            kernel_mem_allocation* next_first_free = first_free->next;
            while (!next_first_free->free) {
                next_first_free = next_first_free->next;
                if (!next_first_free) {
                    break;
                }
            }
            first_free = next_first_free;
            if (!first_free) {
                last_free = NULL;
            }
        }
        page = alloc_page_at(next->start, next->phys);
        if (first_free && first_free->size >= page_size) {
            first_free->phys += page_size;
            first_free->start += page_size;
        }
    } else {
        page = alloc_page_at(next_virt_page, next_fresh_page());
        next_virt_page += page_size;
    }

    spinlock_release(&alloc_fresh_page_lock);
    return page;
}

static kernel_mem_allocation* create_alloc_node() {
    spinlock_acquire(&alloc_node_lock);
    if (!first) {
        first = alloc_next_fresh_page().virt;
        last = first;
        memzero(first, sizeof(kernel_mem_allocation));
        spinlock_release(&alloc_node_lock);
        return first;
    } else {
        kernel_mem_allocation* node = NULL;
        if (align_floor(last, page_size) + page_size < (uintptr_t) last + sizeof(kernel_mem_allocation)) {
            // Doesn't fit, need new page
            node = alloc_next_fresh_page().virt;
        } else {
            node = (void*) last + sizeof(kernel_mem_allocation);
        }
        memzero(node, sizeof(kernel_mem_allocation));
        last->next = node;
        node->prev = last;
        last = node;
        spinlock_release(&alloc_node_lock);
        return node;
    }
}

void* kernel_mm_alloc(size_t size) {
    size_t aligned_size = align(size, page_size);
    kernel_mem_allocation* alloc = create_alloc_node();
    mem_address_pair addr = alloc_next_fresh_page();
    alloc->size = aligned_size;
    alloc->start = addr.virt;
    alloc->phys = addr.phys;
    alloc->free = false;
    alloc->reusable = true;
    for (size_t s = 0; s < aligned_size; s += page_size) {
        alloc_next_fresh_page();
    }

    return alloc->start;
}

void* kernel_mm_alloc_at(size_t size, void* virt_addr_unaligned) {
    void* virt_addr = virt_addr_unaligned - (uintptr_t) virt_addr_unaligned % page_size;
    size_t aligned_size = align(size, page_size);

    kernel_mem_allocation* alloc = create_alloc_node();
    mem_address_pair addr = alloc_page_at(virt_addr, next_fresh_page());
    alloc->size = aligned_size;
    alloc->start = addr.virt;
    alloc->phys = addr.phys;
    alloc->free = false;
    alloc->reusable = false;
    for (size_t s = page_size; s < aligned_size; s += page_size) {
        alloc_page_at(virt_addr + s, next_fresh_page());
    }

    return alloc->start;
}

void* kernel_mm_alloc_mapping(void* phys_addr, size_t size) {
    size_t aligned_size = align(size, page_size);
    void* aligned_phys_addr = (void*) align_floor(phys_addr, page_size);

    kernel_mem_allocation* alloc = create_alloc_node();
    void* virt = allocate_next_virt_addr();
    alloc->size = aligned_size;
    alloc->start = virt;
    alloc->phys = aligned_phys_addr;
    alloc->free = false;
    alloc->reusable = false;
    for (size_t s = 0; s < aligned_size; s += page_size) {
        allocate_next_virt_addr();
    }

    paging_map_range_direct(aligned_phys_addr, virt, aligned_size);

    return alloc->start + (phys_addr - aligned_phys_addr);
}

void* kernel_mm_alloc_page_phys() {
    void* page = next_fresh_page();
    paging_map_identity_page_direct(page);
    return page;
}

void kernel_mm_free(void* ptr) {
    void* aligned_virt_addr = (void*) align_floor(ptr, page_size);
    kernel_mem_allocation* alloc = first;
    while (alloc) {
        if (alloc->start == aligned_virt_addr && !alloc->free) {
            break;
        }
        alloc = alloc->next;
    }
    if (!alloc) {
        printf("Invalid allocation pointer or double-free detected: %p\n", ptr);
        boot_arch_full_halt();
    }

    if (!first_free) {
        first_free = alloc;
    }
    last_free = alloc;
    alloc->free = true;
    paging_unmap_range(alloc->start, alloc->size);
}

void* kernel_mm_alloc_last_page() {
    return last_page;
}

static size_t used_pages_size() {
    size_t size = 0;
    for (kernel_mem_allocation* alloc = first_free; alloc; alloc = alloc->next) {
        if (!alloc->free && alloc->reusable) {
            size += alloc->size;
        }
    }
    return size;
}

size_t kernel_mm_free_memory_bytes() {
    return total_usable_bytes - used_pages_size();
}

size_t kernel_mm_used_memory_bytes() {
    return used_pages_size();
}
