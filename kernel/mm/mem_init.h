// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include "kernel/mm/mem_info.h"

void kernel_mm_initialize();
void kernel_mm_late_init();
void kernel_mm_prepare();
