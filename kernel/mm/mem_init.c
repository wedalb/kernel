// SPDX-License-Identifier: GPL-3.0-or-later
#include <lib/tinystd/mm.h>
#include <lib/tinystd/printf.h>

#include <include/spec/units/byte.h>

#include <kernel/entry.h>

#include "alloc.h"
#include "mem_iface.h"
#include "mem_info.h"
#include "paging.h"

static bool initialized = false;

void kernel_mm_prepare() {
    memset(&kernel_mem_info, 0, sizeof(kernel_mem_info));
    boot_arch_memory_management_fill_info(&kernel_mem_info);
    boot_arch_prepare_memory();
}

void kernel_mm_initialize() {
    if (kernel_mem_info.mem_area_count == 0) {
        printf("Memory info not populated!\n");
        return;
    }

    printf("Usable memory areas: %zu\n", kernel_mem_info.mem_area_count);
    printf("Total usable physical memory: %zu MiB (%zu GiB, %zu B)\n",
        FLOORED_MiB(kernel_meminfo_phys_total_usable()),
        FLOORED_GiB(kernel_meminfo_phys_total_usable()),
        kernel_meminfo_phys_total_usable());

    arch_memory_management_initialize(&kernel_mem_info);

    kernel_mm_alloc_pre_init();
    arch_memory_freeze_setup_paging();
    kernel_mm_alloc_init();

    initialized = true;
}

void kernel_mm_relocate_remap() {
    if (!initialized) {
        return;
    }

    printf("Relocating memory map info\n");
    size_t all_areas_size = kernel_mem_info.all_mem_area_count * sizeof(kernel_meminfo_area);
    size_t areas_size = kernel_mem_info.mem_area_count * sizeof(kernel_meminfo_area);
    kernel_meminfo_area* areas_all = kernel_mm_alloc(all_areas_size);
    kernel_meminfo_area* areas = kernel_mm_alloc(areas_size);
    memcpy(areas_all, kernel_mem_info.all_mem_areas, all_areas_size);
    memcpy(areas, kernel_mem_info.mem_areas, areas_size);
    kernel_mem_info.all_mem_areas = areas_all;
    kernel_mem_info.mem_areas = areas;

    paging_set_page_allocation_border(kernel_mm_alloc_last_page());

    printf("Unmapping identity map\n");
    for (int i = 0; i < kernel_mem_info.all_mem_area_count; i++) {
        kernel_meminfo_area area = kernel_mem_info.all_mem_areas[i];
        paging_unmap_range(area.start, area.size_bytes);
    }

    printf("Kernel memory management initialization complete.\n");
}