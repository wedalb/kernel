// SPDX-License-Identifier: GPL-3.0-or-later
#include <lib/tinystd/mm.h>
#include <lib/tinystd/printf.h>
#include <lib/tinystd/str.h>
#include <lib/tinystd/stddef.h>

#include <drivers/video/fb/console.h>
#include <drivers/video/fb/efifb.h>

#include <include/internal/mm/stack.h>
#include <include/spec/units/byte.h>

#include "mm/mem_info.h"
#include "streams.h"
#include "mm/mem_init.h"
#include "mm/alloc.h"
#include "boot_iface.h"
#include "drivers.h"
#include "entry.h"
#include "loop.h"

boot_interface* boot_iface;
void* kernel_entry_stack_pointer;

void kernel_prepare_memory() {
    printf("Preparing memory...\n");
    kernel_mm_prepare();
    kernel_mm_initialize();
    boot_arch_remap_stack();
}

void kernel_prepare_interrupts() {
    printf("Preparing interrupts...\n");
    boot_arch_prepare_interrupts();
}

void kernel_prepare_display() {
    boot_arch_prepare_display();
}

void kernel_start_drivers() {
    if (boot_iface->acpi_rsdp) {
        kernel_driver_require(KERNEL_DRIVER_ACPI, NULL);
    }
}

void kernel_main(boot_interface* bi) {
    kernel_entry_stack_pointer = stack_pointer();

    boot_iface = bi;

    serial_init();
    pipe_stream_set_default_out_write_text(serial_output);

    kernel_define_drivers();
    kernel_prepare_memory();
    kernel_prepare_interrupts();
    kernel_prepare_display();
    kernel_start_drivers();

    printf("Free memory: %zu MiB\n", FLOORED_MiB(kernel_mm_free_memory_bytes()));
    printf("\nIdle.\n");

    kernel_run_event_loop();
}
