// SPDX-License-Identifier: GPL-3.0-or-later
#pragma once

#include "kernel/boot_iface.h"

extern boot_interface* boot_iface;

extern void* kernel_entry_stack_pointer;

void kernel_main(boot_interface* bi);
